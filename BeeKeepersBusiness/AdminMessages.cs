﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersDataBase;
namespace BeeKeepersBusiness
{
    public class AdminMessages
    {
        public Int64? SendAdminMessage(int usertype, string message,string messagefr)
        {
            using (BeeKeepersEntities entity = new BeeKeepersEntities())
            {
                return entity.sp_SendAdminMessages(usertype, message, messagefr).FirstOrDefault().result;
            }
        }
    }
}
