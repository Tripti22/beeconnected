﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersDataBase;

namespace BeeKeepersBusiness
{
    public class Admin:tbladmin
    {

        public int ChangePassword(Int64 userid, string oldpassword, string newpassword)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var user = DB.tbladmins.Where(s => s.id == userid && s.password == oldpassword).FirstOrDefault();
                if (user != null)
                {
                    user.password = newpassword;
                    DB.tbladmins.ApplyCurrentValues(user);
                    DB.SaveChanges();
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

    }
}
