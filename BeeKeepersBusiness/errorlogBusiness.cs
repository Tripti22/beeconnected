﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BeeKeepersBusiness.Business;
using BeeKeepersDataBase;
using BeeKeepersDataBase;

namespace BeeKeepersBusiness
{
    public class errorlogBusiness : BusinessBase
    {

        public void InsertErrorLog(int id, string source, string description, string occuredcomponent)
        {
            bool isNew = (id == 0);

            try
            {
                errorlog errorlog;
                if (!isNew)
                    errorlog = errorlogRepository.Find(id);
                else
                    errorlog = new errorlog();
                if (errorlog != null)
                {
                    errorlog.source = source;
                    errorlog.description = description;
                    errorlog.occuredcomponent = occuredcomponent;
                    if (isNew)
                    {
                        errorlog.AddedOn = DateTime.Now;
                        errorlogRepository.Add(errorlog);
                    }
                    else
                        errorlogRepository.Update(errorlog);
                }
                errorlog = null;

            }
            catch (Exception ex)
            {

            }
        }
    }
}
