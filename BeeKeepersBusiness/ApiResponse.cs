﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeeKeepersBusiness
{
    public class ApiResponse<T>
    {
        public string status { get; set; }
        public string message { get; set; }


        public T data { get; set; }
        public void AddResponsePacket(T data, string message, string status)
        {
            this.status = status;
            this.data = data;
            this.message = message;
        }

        
    }
}
