﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersBusiness
{
    public class MsgCounter
    {
        public int? UnreadCounter { get; set; }
        public long UserId { get; set; }
    }

    [DataContract]
    public class CommonResponse
    {
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public DataResponse data { get; set; }

    }
    [MessageContract]
    public class DataResponse
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int userType { get; set; }
        [DataMember]
        public bool isactive { get; set; }
    }
    [DataContract]
    public class SignUpRequest
    {
        [DataMember]
        public string userName { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public int userType { get; set; }//0 for Iphone ,1 for android
        [DataMember]
        public string nickName { get; set; }
        [DataMember]
        public string firstname { get; set; }
        [DataMember]
        public string lastname { get; set; }
        [DataMember]
        public string Lang { get; set; }
        [DataMember]
        public int tokenId { get; set; }
    }

    [DataContract]
    public class LoginRequest
    {
        [DataMember]
        public string userName { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string Lang { get; set; }
        [DataMember]
        public int tokenId { get; set; }
    }
    [MessageContract]
    public class ForgotPassword
    {
        [DataMember]
        public string email;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class ChangePassword
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public string oldPassword;
        [DataMember]
        public string newPassword;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class EditProfile
    {
        [DataMember]
        public string userName;
        [DataMember]
        public string email;
        [DataMember]
        public int userType;
        [DataMember]
        public string nickName;
        [DataMember]
        public string firstname;
        [DataMember]
        public string lastname;
        [DataMember]
        public int Status;
        [DataMember]
        public string UpdatedOn;
    }
    
    [MessageContract]
    public class ScalarResponse
    {
        [DataMember]
        public Int64 result;
    }

    [MessageContract]
    public class MessageResponse
    {
        [DataMember]
        public Int64 result;
        [DataMember]
        public string message;
        [DataMember]
        public string addedon;
    }
    [MessageContract]
    public class ScalarRequest
    {
        [DataMember]
        public Int64 requestValue;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class GetSynchDataRequest
    {
        [DataMember]
        public Int64 UserId;
        [DataMember]
        public string LastSynchDateTime; //dd-mmm-yyyy format
        [DataMember]
        public string Latitude;
        [DataMember]
        public string Longitude;
        [DataMember]
        public Int64 InstallationId;
        [DataMember]
        public bool IsDistanceGT50;
        [DataMember]
        public string Lang { get; set; }
    }

    [MessageContract]
    public class InstallationRequest
    {
        [DataMember]
        public string DeviceId;
        [DataMember]
        public int deviceType;
        [DataMember]
        public string deviceToken;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class SetUserInstallationRequest
    {
        [DataMember]
        public Int64 tokenId;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public string Lang;
    }
    

    
    [MessageContract]
    public class UpdateProfile
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public string userName;
        [DataMember]
        public string email;
        [DataMember]
        public int userType;
        [DataMember]
        public string nickName;
        [DataMember]
        public string firstname;
        [DataMember]
        public string lastname;
        [DataMember]
        public string Lang { get; set; }

    }

    [MessageContract]
    public class AddProperty
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string propertyName;
        [DataMember]
        public string Lang { get; set; }
    }

    [MessageContract]
    public class PropertyResponse
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 propertyId;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string propertyName;
        [DataMember]
        public string Lang { get; set; }
    }

    [MessageContract]
    public class ActivityRequest
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int32 userType;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public string Lang { get; set; }
    }

    [MessageContract]
    public class MyActivityRequest
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public string dateFrom;
        [DataMember]
        public string dateTo;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class ActivityResponse
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public string Lang { get; set; }

    }

    [MessageContract]
    public class ActivityResponse1
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public string distance;
        [DataMember]
        public string userName;
    }

    [MessageContract]
    public class ActivityListRequest
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public int radius;
        [DataMember]
        public string Lang { get; set; }
        
    }
    [MessageContract]
    public class MyActivitiesAroundRequest
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 activityId;
        [DataMember]
        public int radius;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
     public class MyActivitiesAroundResponse
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public string distance;
    }
    [MessageContract]
    public class MessageSendRequest
    {
        [DataMember]
        public Int64 fromUserId;
        [DataMember]
        public Int64 toUserId;
        [DataMember]
        public string message;
        [DataMember]
        public string Lang { get; set; }

    }
    [MessageContract]
    public class BroadcastMessageRequest
    {
        [DataMember]
        public Int64 activityId;
        [DataMember]
        public string message;
        [DataMember]
        public string Lang { get; set; }
    }

    [MessageContract]
    public class MessageGetRequest
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 otherUserId;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class MessageGetRequestNew
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 otherUserId;
        [DataMember]
        public int pageNumber;
        [DataMember]
        public int pageSize;
        [DataMember]
        public string Lang { get; set; }
    }


    [MessageContract]
    public class GetMessagesResponse
    {
        [DataMember]
        public Int64 messageId;
        [DataMember]
        public Int64 fromUserId;
        [DataMember]
        public Int64 toUserId;
        [DataMember]
        public string fromUsername;
        [DataMember]
        public Int64 fromUserType;
        [DataMember]
        public string message;
        [DataMember]
        public string date;
        [DataMember]
        public int status; //1 for unread & 2 for read 
    }
    [MessageContract]
    public class GetMessagesResponseNew
    {
        [DataMember]
        public int flag_Records;
        [DataMember]
        public List<GetMessagesResponse> listMessage;
    }


    [MessageContract]
    public class GetInboxUserResponse
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public string userName;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string Lang { get; set; }
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public long? lastMsgDateTime { get; set; }
    }

    [MessageContract]
    public class UnreadCountResponse
    {
        [DataMember]
        public Int64 toUserId;
        [DataMember]
        public Int64 fromUserId;
        [DataMember]
        public string unReadCount;
    }

    [MessageContract]
    public class NearbyPropertyRequest
    {
        [DataMember]
        public int distance;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class NearbyPropertyResponse
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public string propertyName;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public int userType;
        [DataMember]
        public string userName;
    }


    [MessageContract]
    public class GetAllSynchData
    {
        [DataMember]
        public EditProfile ProfileDetails;
        [DataMember]
        public List<MyActivity> MyActivityList;
        [DataMember]
        public List<MyProperty> MyPropertyList;
        [DataMember]
        public NearByActivities NearByList;
        [DataMember]
        public List<InboxUsers> InboxUsers;
    }
    [MessageContract]
    public class MyProperty
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 propertyId;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string propertyName;
        [DataMember]
        public List<GetActivities> GetActivities;
    }
    [MessageContract]
    public class MyActivity
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public List<GetActivities> GetActivities;
    }
    [MessageContract]
    public class GetActivities
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public string distance;
        [DataMember]
        public string userName;
        [DataMember]
        public MyActivityByTime MyActivityByTime;
    }
    [MessageContract]
    public class MyActivityByTime
    {
        [DataMember]
        public List<Myactivities2> PresentActivity;
        [DataMember]
        public List<Myactivities2> FutureActivity;
        [DataMember]
        public List<Myactivities2> PastActivity;
    }

    [MessageContract]
    public class Myactivities2
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public List<MyActivityArround> MyActivityArround;
    }

    [MessageContract]
    public class MyActivityArround
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public string note;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public string distance;
    }
    [MessageContract]
    public class NearByActivities
    {
        [DataMember]
        public List<GetActivities> GetActivities;
        [DataMember]
        public List<NearbyProperty> NearbyProperty;
    }
    [MessageContract]
    public class NearbyProperty
    {
        [DataMember]
        public Int64 id;
        [DataMember]
        public string propertyName;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public Int64 userId;
        [DataMember]
        public int userType;
        [DataMember]
        public string userName;
        [DataMember]
        public MyActivityByTime MyActivityByTime;
    }

    [MessageContract]
    public class InboxUsers
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public string userName;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public List<InboxMessages> Messages;

    }
    [MessageContract]
    public class InboxMessages
    {
        [DataMember]
        public Int64 fromUserId;
        [DataMember]
        public Int64 toUserId;
        [DataMember]
        public string fromUsername;
        [DataMember]
        public Int64 fromUserType;
        [DataMember]
        public string message;
        [DataMember]
        public string date;
    }

    [MessageContract]
    public class SynchData
    {
        [DataMember]
        public string CurrentSynchDateTime; //dd-mmm-yyyy format
        [DataMember]
        public ProfileDetails ProfileDetails;
        [DataMember]
        public List<SynchActivities> SynchActivities;
        [DataMember]
        public List<SynchProperties> SynchProperties;
        [DataMember]
        public List<SynchMessages> SynchMessages;
        [DataMember]
        public List<SynchUsers> SynchUsers;
        [DataMember]
        public DeletedData DeletedData;
    }
    [MessageContract]
    public class ProfileDetails
    {
        [DataMember]
        public string userName;
        [DataMember]
        public string email;
        [DataMember]
        public int userType;
        [DataMember]
        public string nickName;
        [DataMember]
        public string firstname;
        [DataMember]
        public string lastname;
        [DataMember]
        public int Status;
    }
    [MessageContract]
    public class SynchUsers
    {
        [DataMember]
        public string UserName;
        [DataMember]
        public int UserType;
        [DataMember]
        public Int64 UserId;
        //[DataMember]
        //public DateTime AddedOn;
        [DataMember]
        public int Status;

    }
    [MessageContract]
    public class DeletedData
    {
        [DataMember]
        public List<DeletedActivity> DeletedActivity;
        [DataMember]
        public List<DeletedProperty> DeletedProperty;
        [DataMember]
        public List<DeletedUser> DeletedUser;
    }
    [MessageContract]
    public class DeletedActivity
    {
        [DataMember]
        public Int64 PrimaryId;
    }
    [MessageContract]
    public class DeletedProperty
    {
        [DataMember]
        public Int64 PrimaryId;
    }
    [MessageContract]
    public class DeletedUser
    {
        [DataMember]
        public Int64 PrimaryId;
    }
    [MessageContract]
    public class SynchActivities
    {
        [DataMember]
        public Int64 ActivityId;
        [DataMember]
        public string Note;
        [DataMember]
        public string Latitude;
        [DataMember]
        public string Longitude;
        [DataMember]
        public string StartDate;
        [DataMember]
        public string EndDate;
        [DataMember]
        public string ExpiryDate;
        //[DataMember]
        //public DateTime AddedOn;
        //[DataMember]
        //public DateTime? UpdatedOn;
        [DataMember]
        public Int64 UserId;
        [DataMember]
        public int Status;
    }
    [MessageContract]
    public class SynchProperties
    {
        [DataMember]
        public Int64 PropertyId;
        [DataMember]
        public string PropertyName;
        [DataMember]
        public string Latitude;
        [DataMember]
        public string Longitude;
        //[DataMember]
        //public DateTime AddedOn;
        //[DataMember]
        //public DateTime? UpdatedOn;
        [DataMember]
        public Int64 UserId;
        [DataMember]
        public int Status;
    }
    [MessageContract]
    public class SynchMessages
    {
        [DataMember]
        public Int64 FromUserId;
        [DataMember]
        public Int64 ToUserId;
        [DataMember]
        public string Body;
        [DataMember]
        public string AddedOn;
        [DataMember]
        public Int64 MessageId;
        [DataMember]
        public int Status;
        [DataMember]
        public bool IsAdmin;
        [DataMember]
        public int ReadUnreadStatus;
    }
    
    [MessageContract]
    public class LanguageRequest
    {
        [DataMember]
        public int tokenId;
        [DataMember]
        public string Language;
    }
    [MessageContract]
    public class LocalData
    {
        [DataMember]
        public List<LocalActivity> LocalActivity;
        [DataMember]
        public List<LocalProperty> LocalProperty;
        [DataMember]
        public List<LocalMessage> LocalMessage;
        [DataMember]
        public LocalDeletedData LocalDeleted;
        [DataMember]
        public List<BroadcastMessageRequest> BroadCastMessages;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class LocalActivity
    {
        [DataMember]
        public Int64 ActivityId;
        [DataMember]
        public Int64 UserId;
        [DataMember]
        public Int32 UserType;
        [DataMember]
        public string Note;
        [DataMember]
        public string Latitude;
        [DataMember]
        public string Longitude;
        [DataMember]
        public string StartDate;
        [DataMember]
        public string EndDate;
        [DataMember]
        public int Status;
    }
    [MessageContract]
    public class LocalProperty
    {
        [DataMember]
        public Int64 PropertyId;
        [DataMember]
        public Int64 UserId;
        [DataMember]
        public string Latitude;
        [DataMember]
        public string Longitude;
        [DataMember]
        public string PropertyName;
        [DataMember]
        public int Status;
    }

    [MessageContract]
    public class LocalMessage
    {
        [DataMember]
        public Int64 MessageId;
        [DataMember]
        public Int64 FromUserId;
        [DataMember]
        public Int64 ToUserId;
        [DataMember]
        public string Message;
        [DataMember]
        public string AddedOn;
        [DataMember]
        public int status; // Read Unread status
    }
    [MessageContract]
    public class LocalDeletedData
    {
        [DataMember]
        public List<DeletedActivity> DeletedActivity;
        [DataMember]
        public List<DeletedProperty> DeletedProperty;
    }

    [MessageContract]
    public class ActivityListFilteredRequest
    {
        [DataMember]
        public Int64 userId;
        [DataMember]
        public Int64 userType;
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string startDate;
        [DataMember]
        public string endDate;
        [DataMember]
        public string Lang { get; set; }
        [DataMember]
        public string ActivityIds { get; set; }
    }
    [MessageContract]
    public class NearbyPropertyFilteredRequest
    {
        [DataMember]
        public string latitude;
        [DataMember]
        public string longitude;
        [DataMember]
        public string Lang { get; set; }
        [DataMember]
        public string PropertyIds { get; set; }
    }
    [MessageContract]
    public class GetUserRequest
    {
        [DataMember]
        public string UserIds;
        [DataMember]
        public string Lang { get; set; }
    }
    [MessageContract]
    public class GetUserResponse
    {
        [DataMember]
        public string UserName;
        [DataMember]
        public int UserType;
        [DataMember]
        public Int64 UserId;
    }

    [MessageContract]

    public class DeleteUser
    {
        [MessageBodyMember]
        public string UserName;
        [MessageBodyMember]
        public string Email;
        [MessageBodyMember]
        public string Lang { get; set; }
    }

    [DataContract]
    public class DeleteUserResponse
    {
        [DataMember]
        public string UserName;
        [DataMember]
        public int UserType;
        [DataMember]
        public Int64 UserId;

    }
}
