﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersDataBase;
namespace BeeKeepersBusiness
{
    public class PageBusiness : Business.BusinessBase
    {
        public List<tblPage> SelectAllPages()
        {
            return PageRepository.SelectAllPages();
        }

        public Int64? InsertUpdatePage(string pagename,string pagecontent,string pageformatedname,string frenchcontent)
        {
            return PageRepository.InsertUpdatePage(pagename,pagecontent,pageformatedname,frenchcontent);
        }
        public Int64 DeletePage(string pagename)
        {
            return PageRepository.DeletePage(pagename);
        }
        public tblPage SelectPageText(string pagename)
        {
            return PageRepository.SelectAllPages().Where(s => s.pagename == pagename).FirstOrDefault();
        }



    }
}
