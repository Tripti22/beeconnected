﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BeeKeepersBusiness
{
    public class Common
    {
        public static String BasePath()
        {
            if (HttpContext.Current.Request.Url.ToString().Contains("https") == false)
                return String.Format("http://{0}{1}", HttpContext.Current.Request.ServerVariables["HTTP_HOST"], (HttpContext.Current.Request.ApplicationPath.Equals("/") ? String.Empty : HttpContext.Current.Request.ApplicationPath)) + "/";
            else
                return String.Format("https://{0}{1}", HttpContext.Current.Request.ServerVariables["HTTP_HOST"], (HttpContext.Current.Request.ApplicationPath.Equals("/") ? String.Empty : HttpContext.Current.Request.ApplicationPath)) + "/";
        }

        public static bool IsValidFile(System.Web.UI.WebControls.FileUpload FileName)
        {
            bool functionReturnValue = false;
            string strExtension = null;
            strExtension = System.IO.Path.GetExtension(FileName.PostedFile.FileName);
            strExtension = strExtension.ToLower();
            if ((strExtension == ".jpg" | strExtension == ".jpeg" | strExtension == ".gif" | strExtension == ".bmp" | strExtension == ".png"))
            {
                functionReturnValue = true;
            }
            else
            {
                functionReturnValue = false;
            }
            return functionReturnValue;
        }
        public static bool IsValidFileVideo(System.Web.UI.WebControls.FileUpload FileName)
        {
            bool functionReturnValue = false;
            string strExtension = null;
            strExtension = System.IO.Path.GetExtension(FileName.PostedFile.FileName);
            strExtension = strExtension.ToLower();
            if ((strExtension == ".flv" | strExtension == ".avi" | strExtension == ".mov" | strExtension == ".mp4" | strExtension == ".mpg" | strExtension == ".wmv" | strExtension == ".3gp" | strExtension == ".asf" | strExtension == ".rm" | strExtension == ".swf" | strExtension == ".mkv"))
            {
                functionReturnValue = true;
            }
            else
            {
                functionReturnValue = false;
            }
            return functionReturnValue;
        }

        public static bool IsValidFile(string FileName, int type)
        {
            bool functionReturnValue = false;

            string strExtension = null;
            strExtension = System.IO.Path.GetExtension(FileName);

            if (type == 0)
            {
                if ((strExtension == ".mp3" | strExtension == ".wav" | strExtension == ".wma" | strExtension == ".mp4"))
                {
                    functionReturnValue = true;
                }
                else
                {
                    functionReturnValue = false;
                }
            }
            else
            {
                if ((strExtension == ".flv" | strExtension == ".mp4" | strExtension == ".dat" | strExtension == ".wmv"))
                {
                    functionReturnValue = true;
                }
                else
                {
                    functionReturnValue = false;
                }
            }
            return functionReturnValue;
        }

        public static void BindDropDown<T>(DropDownList ddlDropdown, List<T> lstData, string DataDisplayField, string DataValueField, bool isDefaultItem, string DefaultText)
        {
            ddlDropdown.DataSource = lstData;
            ddlDropdown.DataValueField = DataValueField;
            ddlDropdown.DataTextField = DataDisplayField;
            ddlDropdown.DataBind();
            if (isDefaultItem)
                ddlDropdown.Items.Insert(0, new ListItem(DefaultText, ""));

        }
        public void UpdateLanguageSetting4Web(long UserId, string lang, int sourcetype)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities entity = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var objLanguage = entity.tblLanguages.Where(s => s.userid == UserId && s.sorcetype == sourcetype).FirstOrDefault();
                if (objLanguage != null)
                {
                    objLanguage.lastupdateon = DateTime.UtcNow;
                    objLanguage.language = lang.ToLower();
                    entity.tblLanguages.ApplyCurrentValues(objLanguage);
                    entity.SaveChanges();
                }
            }
        }
        public int UpdateLanguageSetting(int TokenId, string Language, int sourcetype)
        {
            try
            {
                if (TokenId != 0)
                {
                    using (BeeKeepersDataBase.BeeKeepersEntities entity = new BeeKeepersDataBase.BeeKeepersEntities())
                    {
                        var objLanguage = entity.tblLanguages.Where(s => s.tokenid == TokenId && s.sorcetype == sourcetype).FirstOrDefault();
                        if (objLanguage != null)
                        {
                            objLanguage.language = Language.ToLower();
                            objLanguage.lastupdateon = DateTime.UtcNow;
                            entity.tblLanguages.ApplyCurrentValues(objLanguage);
                            entity.SaveChanges();
                            return 1;
                        }
                        else
                        {
                            return -2;
                        }
                    }
                }
                else
                {
                    return -2;
                }
                
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public string GetDeviceLanguageByToken(int TokenId)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities entity = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var ObjLanguages = entity.tblLanguages.Where(s => s.tokenid == TokenId).FirstOrDefault();
                if (ObjLanguages != null)
                {
                    return Convert.ToString(ObjLanguages.language).ToLower();
                }
                else
                {
                    return "en";
                }
            }
        }

        public string GetLanguage4Notification(long? UserId)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities entity = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var ObjLanguage = entity.sp_SelectLang4Notifications(UserId).FirstOrDefault();
                if (ObjLanguage != null)
                {
                    return Convert.ToString(ObjLanguage.language).ToLower();
                }
                else
                {
                    return "en";
                }
            }
        }
        public string GetLanguage4Emails(long? UserId)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities entity = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var ObjLanguage = entity.tblLanguages.Where(s => s.userid == UserId).OrderByDescending(s=>s.lastupdateon).FirstOrDefault();
                if (ObjLanguage != null)
                {
                    return Convert.ToString(ObjLanguage.language).ToLower();
                }
                else
                {
                    return "en";
                }
            }
        }
        public ApiResponse<ScalarResponse> UpdateLanguageSettingJson(int TokenId, string Language)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                Int64 result = UpdateLanguageSetting(TokenId, Language, 2);
                if (result > 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Language, "Installationsuccessfully"), "1");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Language, "ThereisaproblemPleasetryagainlater"), "1");
                }

            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Language, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public int ResetUnreadNotifications(Int64 UserId, string Language)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities entity = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var installation = entity.tblusers.Where(s => s.id == UserId).FirstOrDefault();
                if (installation != null)
                {
                    installation.NotiCounter = null;
                    entity.tblusers.ApplyCurrentValues(installation);
                    entity.SaveChanges();
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }
        public ApiResponse<ScalarResponse> ResetUnreadNotificationsJson(Int64 UserId, string Language)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                Int64 result = ResetUnreadNotifications(UserId, Language);
                if (result > 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Language, "Success"), "1");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Language, "ThereisaproblemPleasetryagainlater"), "1");
                }

            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Language, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public int? GetNotificationCounter(Int64? userid) 
        {
            using (BeeKeepersDataBase.BeeKeepersEntities entity = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var installation = entity.tblusers.Where(s => s.id == userid).FirstOrDefault();
                if (installation != null)
                {
                    installation.NotiCounter = installation.NotiCounter!=null?installation.NotiCounter+1:1;
                    entity.tblusers.ApplyCurrentValues(installation);
                    entity.SaveChanges();
                    return installation.NotiCounter;
                }
            }
            return null;

        }
        
    }
}
