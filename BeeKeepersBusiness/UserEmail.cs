﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersBusiness
{
    public class UserEmail
    {
        public string toEmailId { get; set; }
        public string toUserName { get; set; }
        public string fromUserName { get; set; }
        public string message { get; set; }
        public string UserLanguage { get; set; }

        
        public static void SendNotificationMails4Message(List<UserEmail> lstuseremail)
        {
            try
            {
                //SmtpClient smtp = new SmtpClient();
                //smtp.UseDefaultCredentials = false;
                //smtp.Host = "mail.dotsquares.com";
                //smtp.Credentials = new System.Net.NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
                //smtp.Port = 25;

                SmtpClient smtp = new SmtpClient();
                smtp.UseDefaultCredentials = true;
                smtp.Host = "localhost";
                smtp.Port = 25;
                if (lstuseremail != null && lstuseremail.Count() > 0)
                {
                    foreach (var item in lstuseremail)
                    {
                        MailMessage msg = new MailMessage("donotreply@beeconnected.ca", item.toEmailId, new SiteLanguages().GetResxNameByValue(item.UserLanguage, "NewmessageonBeeConnected"), new SiteLanguages().GetResxNameByValue(item.UserLanguage, "Dear") + " " + item.toUserName + ",<br />" + item.message + "<br /><br />" + new SiteLanguages().GetResxNameByValue(item.UserLanguage, "Toreadthemessage") + "<br /><br />" + new SiteLanguages().GetResxNameByValue(item.UserLanguage, "Thisisanautomatednotification"));
                        msg.IsBodyHtml = true;
                        smtp.Send(msg);
                    }
                }
            }
            catch (Exception ex)
            { 
            }
            
        }
        public static void SendNotificationMails4Activity(List<UserEmail> lstuseremail)
        {
            try
            {
                //SmtpClient smtp = new SmtpClient();
                //smtp.UseDefaultCredentials = false;
                //smtp.Host = "mail.dotsquares.com";
                //smtp.Credentials = new System.Net.NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
                //smtp.Port = 25;

                SmtpClient smtp = new SmtpClient();
                smtp.UseDefaultCredentials = true;
                smtp.Host = "localhost";
                smtp.Port = 25;
                if (lstuseremail != null && lstuseremail.Count() > 0)
                {
                    foreach (var item in lstuseremail)
                    {
                        MailMessage msg = new MailMessage("donotreply@beeconnected.ca", item.toEmailId, new SiteLanguages().GetResxNameByValue(item.UserLanguage, "NewactivityaddedonBeeConnected"), new SiteLanguages().GetResxNameByValue(item.UserLanguage, "Dear") + " " + item.toUserName + ",<br />" + item.message + "<br /><br />" + new SiteLanguages().GetResxNameByValue(item.UserLanguage, "Toviewtheactivitydetails") + "<br /><br />" + new SiteLanguages().GetResxNameByValue(item.UserLanguage, "Thisisanautomatednotification"));
                        msg.IsBodyHtml = true;
                        smtp.Send(msg);
                    }
                }
            }
            catch (Exception ex)
            { 
            }
            
        }
    }
}
