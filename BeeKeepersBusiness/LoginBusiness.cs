﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersDataBase;
namespace BeeKeepersBusiness
{
    public class LoginBusiness
    {
        public tbladmin DoLogin(string username, string password)
        {
            return LoginRepository.DoLogin(username, password);
        }
    }
}
