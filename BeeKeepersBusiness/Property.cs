﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersDataBase;
namespace BeeKeepersBusiness
{
    public class Property : Business.BusinessBase
    {
        public Int64? AddProperty(Int64 userId, string latitude, string longitude, string propertyName)
        {
            return PropertyRepository.AddProperty(userId, latitude, longitude, propertyName);
        }
        public List<PropertyResponse> PropertyList(Int64 userid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            var properties = entities.tblproperties.Where(s => s.isactive == true && s.userid==userid).ToList();
            List<PropertyResponse> lstproperty = new List<PropertyResponse>();
            foreach (var item in properties)
            {
                PropertyResponse objproperty = new PropertyResponse();
                objproperty.latitude = item.latitude;
                objproperty.longitude = item.longitude;
                objproperty.propertyId = item.id;
                objproperty.propertyName = item.propertyname;
                objproperty.userId = item.userid;
                lstproperty.Add(objproperty);
            }
            return lstproperty;
        }
        public List<sp_propertylist_Result> Propertlist_admin()
        {
            return PropertyRepository.Propertylist_admin();
        }

        public List<tblproperty> PropertyList_front(Int64 userid)
        {
            using (BeeKeepersEntities entities = new BeeKeepersEntities())
            {
                return entities.tblproperties.Where(X => X.isactive == true && X.userid == userid).ToList();
            }
        }
        public PropertyResponse EditProperty(Int64 propertyid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            var properties = entities.tblproperties.Where(s=>s.id==propertyid).FirstOrDefault();
            if (properties != null)
            {
                PropertyResponse objproperty = new PropertyResponse();
                objproperty.latitude = properties.latitude;
                objproperty.longitude = properties.longitude;
                objproperty.propertyId = properties.id;
                objproperty.propertyName = properties.propertyname;
                objproperty.userId = properties.userid;
                return objproperty;
            }
            else
            {
                return null;
            }
        }

        public Int64? UpdateProperty(Int64 userId, string latitude, string longitude, string propertyName,Int64 propertyid)
        {
            return PropertyRepository.UpdateProperty(userId, latitude, longitude, propertyName,propertyid);
        }
        public Int64 DeleteProperty(Int64 propertyid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            long? result  = entities.sp_Deleteproperty(propertyid).FirstOrDefault().result;
            if (result != 0)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public List<NearbyPropertyResponse> NearbyProperty(int distance,string latitude,string longitude)
        {
            List<NearbyPropertyResponse> lstproperty = new List<NearbyPropertyResponse>();
            var properties = PropertyRepository.NearestProperty(distance, latitude, longitude);
            foreach (var item in properties)
            {
                NearbyPropertyResponse objproperty = new NearbyPropertyResponse();
                objproperty.latitude = item.latitude;
                objproperty.longitude = item.longitude;
                objproperty.id = item.id;
                objproperty.propertyName = item.propertyname;
                objproperty.userId = item.userid;
                objproperty.userType = item.usertype;
                objproperty.userName = item.username;
                lstproperty.Add(objproperty);
            }
            return lstproperty;
        }
        #region Service Methods
        public ApiResponse<ScalarResponse> AddPropertyJson(Int64 userId, string latitude, string longitude, string propertyName, string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            Int64? result = AddProperty(userId, latitude, longitude, propertyName);
            if (result >= 1)
            {
                objresponse.result = result.Value;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Propertyaddedsuccessfully"), "1");
            }
            else if(result==-2)
            {
                objresponse.result = -2;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Userdoesnotexits"), "0");
            }
            else
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponseList<PropertyResponse> PropertyListJson(Int64 userId, string DeviceLang)
        {
            List<PropertyResponse> objresponse = new List<PropertyResponse>();
            ApiResponseList<PropertyResponse> objapiresponse = new ApiResponseList<PropertyResponse>();
            try
            {
                objresponse = PropertyList(userId);
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Propertylistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Propertynotavailable"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponseList<NearbyPropertyResponse> NearbyPropertyJson(int distance,string latitude,string longitude,string DeviceLang)
        {
            List<NearbyPropertyResponse> objresponse = new List<NearbyPropertyResponse>();
            ApiResponseList<NearbyPropertyResponse> objapiresponse = new ApiResponseList<NearbyPropertyResponse>();
            try
            {
                objresponse = NearbyProperty(distance, latitude, longitude);
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nearbypropertylistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nearbypropertynotavailable"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        
        public ApiResponse<PropertyResponse> EditPropertyJson(Int64 Propertyid,string DeviceLang)
        {
            PropertyResponse objresponse = new PropertyResponse();
            ApiResponse<PropertyResponse> objapiresponse = new ApiResponse<PropertyResponse>();
            try
            {
                objresponse = EditProperty(Propertyid);
                if (objresponse != null)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Propertydetailsget"), "1");
                }
                else
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }

        public ApiResponse<ScalarResponse> UpdatePropertyJson(string latitude, string longitude, Int64 propertyId, string propertyName, Int64 userId,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                Int64? result = UpdateProperty(userId, latitude, longitude, propertyName, propertyId);
                if (result >= 1)
                {
                    objresponse.result = result.Value;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Propertyupdatedsuccessfully"), "1");
                }
                else if (result == -2)
                {
                    objresponse.result = -2;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Userdoesnotexits"), "0");
                }
                else if (result == -3)
                {
                    objresponse.result = -3;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Propertydoesnotexits"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            
            return objapiresponse;
        }

        public ApiResponse<ScalarResponse> DeletePropertyJson(Int64 propertyId,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                Int64? result = DeleteProperty(propertyId);
                if (result == 1)
                {
                    objresponse.result = result.Value;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Propertydeletedsuccessfully"), "1");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            
            return objapiresponse;
        }
        public List<NearbyPropertyResponse> NearbyPropertyFiltered(int distance, string latitude, string longitude, string propertyids)
        {
            List<NearbyPropertyResponse> lstproperty = new List<NearbyPropertyResponse>();
            List<sp_getNearbyProperty_Result> properties = PropertyRepository.NearestProperty(distance, latitude, longitude);
            if (!string.IsNullOrEmpty(propertyids))
            {
                IEnumerable<Int64> ids = propertyids.Split(',').Select(str => Convert.ToInt64(str));
                properties = properties.Where(rec => !ids.Contains(rec.id)).ToList();
            }
            foreach (var item in properties)
            {
                NearbyPropertyResponse objproperty = new NearbyPropertyResponse();
                objproperty.latitude = item.latitude;
                objproperty.longitude = item.longitude;
                objproperty.id = item.id;
                objproperty.propertyName = item.propertyname;
                objproperty.userId = item.userid;
                objproperty.userType = item.usertype;
                objproperty.userName = item.username;
                lstproperty.Add(objproperty);
            }
            return lstproperty;
        }
        public ApiResponseList<NearbyPropertyResponse> NearbyPropertyFilteredJson(string latitude, string longitude, string DeviceLang, string propertyids)
        {
            List<NearbyPropertyResponse> objresponse = new List<NearbyPropertyResponse>();
            ApiResponseList<NearbyPropertyResponse> objapiresponse = new ApiResponseList<NearbyPropertyResponse>();
            try
            {
                objresponse = NearbyPropertyFiltered(5, latitude, longitude, propertyids);

                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nearbypropertylistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nearbypropertynotavailable"), "1");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        #endregion
    }
}
