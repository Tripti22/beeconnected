﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersBusiness;
using BeeKeepersDataBase;
using BeeKeepersBusiness.utility;
using System.Data.SqlClient;
using System.Data;
namespace BeeKeepersBusiness
{
    public class SynchDataBusiness
    {
        delegate void EmailDelegate(List<UserEmail> lstuseremail);

        public SynchData GetAllSynchData(long userid, string latitude, string longitude, long installationid, string LastSynchDateTime, bool IsDistanceGT50)
        {
            List<SynchProperties> lstsynchproperties = new List<BeeKeepersBusiness.SynchProperties>();
            List<SynchActivities> lstsynchactivities = new List<BeeKeepersBusiness.SynchActivities>();
            List<SynchMessages> lstsynchmessages = new List<BeeKeepersBusiness.SynchMessages>();
            List<SynchUsers> lstsynchusers = new List<BeeKeepersBusiness.SynchUsers>();
            DeletedData deleteddata = new DeletedData();

            StringBuilder sbActivityIds = new StringBuilder();
            StringBuilder sbPropertyIds = new StringBuilder();
            StringBuilder sbMessageIds = new StringBuilder();
            StringBuilder sbUserIds = new StringBuilder();
            int MAXDistance = 50;
            Activities objActivities = new Activities();
            SynchData objsynch = new SynchData();


            ProfileDetails editprofile = new ProfileDetails();
            User objuser = new User();
            var userdetails = objuser.EditProfile(userid);
            if (LastSynchDateTime != "")
            {
                DateTime dt = Convert.ToDateTime(LastSynchDateTime);
                dt.AddSeconds(-20);
                LastSynchDateTime = dt.ToString();
            }
            if (userdetails != null)
            {
                editprofile.email = userdetails.email;
                editprofile.firstname = userdetails.firstname;
                editprofile.lastname = userdetails.lastname;
                editprofile.nickName = userdetails.nickName;
                editprofile.userName = userdetails.userName;
                editprofile.userType = userdetails.userType;
                if (LastSynchDateTime == "")
                {
                    editprofile.Status = 1;
                }
                else if (Convert.ToString(userdetails.UpdatedOn) != "" && Convert.ToDateTime(userdetails.UpdatedOn) > Convert.ToDateTime(LastSynchDateTime))
                {
                    editprofile.Status = 1;
                }
                else
                {
                    editprofile.Status = 0;
                }
                objsynch.ProfileDetails = editprofile;
            }


            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ToString()))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                try
                {
                    cmd = new SqlCommand("sp_GetAllDataIds", con);
                    cmd.Parameters.Add("@userid", SqlDbType.VarChar).Value = userid;
                    cmd.Parameters.Add("@usertype", SqlDbType.VarChar).Value = objsynch.ProfileDetails.userType;
                    cmd.Parameters.Add("@radius", SqlDbType.VarChar).Value = MAXDistance;
                    cmd.Parameters.Add("@latitude", SqlDbType.VarChar).Value = latitude;
                    cmd.Parameters.Add("@longitude", SqlDbType.VarChar).Value = longitude;
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand = cmd;
                    da.Fill(ds);

                    objsynch.CurrentSynchDateTime = Convert.ToDateTime(ds.Tables[6].Rows[0][0].ToString()).ToString("M/d/yyyy h:mm tt");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int item = 0; item < ds.Tables[0].Rows.Count; item++)
                        {

                            SynchActivities synchactivities = new SynchActivities();
                            //synchactivities.AddedOn = item.addedon;
                            synchactivities.EndDate = Convert.ToDateTime(ds.Tables[0].Rows[item]["enddate"].ToString()).ToString("M/d/yyyy h:mm:ss tt");
                            synchactivities.ActivityId = Convert.ToInt64(ds.Tables[0].Rows[item]["id"].ToString());
                            synchactivities.Latitude = ds.Tables[0].Rows[item]["latitude"].ToString();
                            synchactivities.Longitude = ds.Tables[0].Rows[item]["longitude"].ToString();
                            synchactivities.Note = ds.Tables[0].Rows[item]["note"].ToString();
                            synchactivities.StartDate = Convert.ToDateTime(ds.Tables[0].Rows[item]["startdate"].ToString()).ToString("M/d/yyyy h:mm:ss tt");
                            synchactivities.ExpiryDate = Convert.ToDateTime(ds.Tables[0].Rows[item]["expirydate"].ToString()).ToString("M/d/yyyy h:mm:ss tt");
                            //synchactivities.UpdatedOn = item.updatedon;
                            synchactivities.UserId = Convert.ToInt64(ds.Tables[0].Rows[item]["userid"].ToString());
                            if (LastSynchDateTime == "")
                            {
                                synchactivities.Status = 0;
                                lstsynchactivities.Add(synchactivities);
                            }
                            else
                            {
                                if (Convert.ToDateTime(ds.Tables[0].Rows[item]["addedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                                {
                                    synchactivities.Status = 0;
                                    lstsynchactivities.Add(synchactivities);
                                }
                                else if (Convert.ToString(userdetails.UpdatedOn) != "" && Convert.ToDateTime(ds.Tables[0].Rows[item]["updatedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                                {
                                    synchactivities.Status = 1;
                                    lstsynchactivities.Add(synchactivities);
                                }
                            }
                        }
                    }


                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        for (int item = 0; item < ds.Tables[1].Rows.Count; item++)
                        {
                            SynchProperties synchproperties = new SynchProperties();
                            //synchproperties.AddedOn = item.addedon;
                            synchproperties.PropertyId = Convert.ToInt64(ds.Tables[1].Rows[item]["id"].ToString());
                            synchproperties.Latitude = ds.Tables[1].Rows[item]["latitude"].ToString();
                            synchproperties.Longitude = ds.Tables[1].Rows[item]["longitude"].ToString();
                            synchproperties.PropertyName = ds.Tables[1].Rows[item]["propertyname"].ToString();
                            //synchproperties.UpdatedOn = item.updatedon;
                            synchproperties.UserId = Convert.ToInt64(ds.Tables[1].Rows[item]["userid"].ToString());
                            if (LastSynchDateTime == "")
                            {
                                synchproperties.Status = 0;
                                lstsynchproperties.Add(synchproperties);
                            }
                            else
                            {
                                if (Convert.ToDateTime(ds.Tables[1].Rows[item]["addedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                                {
                                    synchproperties.Status = 0;
                                    lstsynchproperties.Add(synchproperties);
                                }
                                else if (Convert.ToString(userdetails.UpdatedOn) != "" && Convert.ToDateTime(ds.Tables[1].Rows[item]["updatedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                                {
                                    synchproperties.Status = 1;
                                    lstsynchproperties.Add(synchproperties);
                                }
                            }
                        }
                    }


                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        for (int item = 0; item < ds.Tables[2].Rows.Count; item++)
                        {
                            SynchMessages synchmessages = new SynchMessages();
                            synchmessages.AddedOn = Convert.ToDateTime(ds.Tables[2].Rows[item]["addedon"].ToString()).ToString("M/d/yyyy h:mm:ss tt");
                            synchmessages.Body = ds.Tables[2].Rows[item]["body"].ToString();
                            synchmessages.FromUserId = Convert.ToInt64(ds.Tables[2].Rows[item]["fromuserid"].ToString());
                            synchmessages.MessageId = Convert.ToInt64(ds.Tables[2].Rows[item]["id"].ToString());
                            synchmessages.ToUserId = Convert.ToInt64(ds.Tables[2].Rows[item]["touserid"].ToString());
                            synchmessages.ReadUnreadStatus = Convert.ToInt32(ds.Tables[2].Rows[item]["status"].ToString());
                            synchmessages.IsAdmin = false;
                            if (LastSynchDateTime == "")
                            {
                                synchmessages.Status = 0;
                                lstsynchmessages.Add(synchmessages);
                            }
                            else
                            {
                                if (Convert.ToDateTime(ds.Tables[2].Rows[item]["addedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                                {
                                    synchmessages.Status = 0;
                                    lstsynchmessages.Add(synchmessages);
                                }
                                else if (Convert.ToString(ds.Tables[2].Rows[item]["updatedon"].ToString()) != "" && Convert.ToDateTime(ds.Tables[2].Rows[item]["updatedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                                {
                                    synchmessages.Status = 1;
                                    lstsynchmessages.Add(synchmessages);
                                }
                            }

                        }
                    }

                    for (int item = 0; item < ds.Tables[3].Rows.Count; item++)
                    {
                        SynchMessages synchmessages = new SynchMessages();
                        synchmessages.AddedOn = Convert.ToDateTime(ds.Tables[3].Rows[item]["addedon"].ToString()).ToString("M/d/yyyy h:mm:ss tt");
                        synchmessages.Body = ds.Tables[3].Rows[item]["message"].ToString();
                        synchmessages.FromUserId = 0;
                        synchmessages.MessageId = Convert.ToInt64(ds.Tables[3].Rows[item]["id"].ToString());
                        synchmessages.ToUserId = 0;
                        synchmessages.IsAdmin = true;
                        synchmessages.ReadUnreadStatus = Convert.ToInt32(ds.Tables[3].Rows[item]["status"].ToString());
                        if (LastSynchDateTime == "")
                        {
                            synchmessages.Status = 0;
                            lstsynchmessages.Add(synchmessages);
                        }
                        else
                        {
                            if (Convert.ToDateTime(ds.Tables[3].Rows[item]["addedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                            {
                                synchmessages.Status = 0;
                                lstsynchmessages.Add(synchmessages);
                            }
                            else if (Convert.ToString(ds.Tables[3].Rows[item]["updatedon"].ToString()) != "" && Convert.ToDateTime(ds.Tables[3].Rows[item]["updatedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                            {
                                synchmessages.Status = 1;
                                lstsynchmessages.Add(synchmessages);
                            }
                        }

                    }

                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        for (int item = 0; item < ds.Tables[4].Rows.Count; item++)
                        {
                            SynchUsers synchusers = new SynchUsers();
                            synchusers.UserId = Convert.ToInt64(ds.Tables[4].Rows[item]["id"].ToString());
                            synchusers.UserType = Convert.ToInt32(ds.Tables[4].Rows[item]["usertype"].ToString());
                            synchusers.UserName = ds.Tables[4].Rows[item]["username"].ToString();
                            if (LastSynchDateTime == "")
                            {
                                synchusers.Status = 0;
                                lstsynchusers.Add(synchusers);
                            }
                            else
                            {
                                //if (Convert.ToDateTime(ds.Tables[4].Rows[item]["addedon"].ToString()) > Convert.ToDateTime(LastSynchDateTime))
                                //{
                                //    synchusers.Status = 0;
                                //    lstsynchusers.Add(synchusers);
                                //}
                                //else
                                //{
                                synchusers.Status = 0;
                                lstsynchusers.Add(synchusers);
                                //}
                            }
                        }
                    }


                    DataView lstDeletedData = new DataView(ds.Tables[5]);

                    if (LastSynchDateTime != "")
                    {
                        DateTime dtLastSynchDateTime = Convert.ToDateTime(LastSynchDateTime);
                        lstDeletedData.RowFilter = "deletedon>='" + dtLastSynchDateTime + "'";
                    }
                    if (LastSynchDateTime != "")
                    {
                        List<DeletedActivity> lstdeletedactivity = new List<DeletedActivity>();
                        List<DeletedProperty> lstdeletedproperty = new List<DeletedProperty>();
                        List<DeletedUser> lstdeleteduser = new List<DeletedUser>();
                        for (int item = 0; item < lstDeletedData.Count; item++)
                        {
                            if (lstDeletedData[item]["tablename"].ToString() == "Activity")
                            {
                                DeletedActivity deletedactivity = new DeletedActivity();
                                deletedactivity.PrimaryId = Convert.ToInt64(lstDeletedData[item]["primarykey"].ToString());
                                lstdeletedactivity.Add(deletedactivity);
                            }
                            else if (Convert.ToString(userdetails.userType) != "" && userdetails.userType == 2 && lstDeletedData[item]["tablename"].ToString() == "Property")
                            {
                                DeletedProperty deletedproperty = new DeletedProperty();
                                deletedproperty.PrimaryId = Convert.ToInt64(lstDeletedData[item]["primarykey"].ToString());
                                lstdeletedproperty.Add(deletedproperty);
                            }
                            else if (lstDeletedData[item]["tablename"].ToString() == "User")
                            {
                                DeletedUser deleteduser = new DeletedUser();
                                deleteduser.PrimaryId = Convert.ToInt64(lstDeletedData[item]["primarykey"].ToString());
                                lstdeleteduser.Add(deleteduser);
                            }
                        }
                        deleteddata.DeletedActivity = lstdeletedactivity;
                        deleteddata.DeletedProperty = lstdeletedproperty;
                        deleteddata.DeletedUser = lstdeleteduser;
                    }

                }
                catch (Exception ex)
                {

                }
            }
            objsynch.SynchActivities = lstsynchactivities;
            objsynch.SynchProperties = lstsynchproperties;
            objsynch.SynchMessages = lstsynchmessages;
            objsynch.SynchUsers = lstsynchusers;
            objsynch.DeletedData = deleteddata;
            return objsynch;
        }
        public ScalarResponse AddLocalData(List<LocalActivity> localactivity, List<LocalProperty> localproperty, List<LocalMessage> localmsg, LocalDeletedData localdeleted,List<BroadcastMessageRequest> BroadCastMessages)
        {
            ScalarResponse scalarresponse = new ScalarResponse();
            scalarresponse.result = 0;
            try
            {
                foreach (var item in localactivity)
                {
                    if (item.Status == 0)
                    {
                        string startDate = DateTime.ParseExact(item.StartDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                        string endDate = DateTime.ParseExact(item.EndDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                        new Activities().AddActivity(item.UserId, item.UserType, item.Note, item.Latitude, item.Longitude, item.StartDate, item.EndDate);
                    }
                    else if (item.Status == 1)
                    {
                        string startDate = DateTime.ParseExact(item.StartDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                        string endDate = DateTime.ParseExact(item.EndDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                        new Activities().UpdateActivity(item.UserId, item.Latitude, item.Longitude, item.UserType, item.StartDate, item.EndDate, item.ActivityId, item.Note);
                    }
                }
                foreach (var item in localproperty)
                {
                    if (item.Status == 0)
                    {
                        new Property().AddProperty(item.UserId, item.Latitude, item.Longitude, item.PropertyName);
                    }
                    else if (item.Status == 1)
                    {
                        new Property().UpdateProperty(item.UserId, item.Latitude, item.Longitude, item.PropertyName, item.PropertyId);
                    }
                }
                foreach (var item in localmsg)
                {
                    if (item.MessageId == 0)
                    {
                        new Messages().SendMessage(item.FromUserId, item.ToUserId, item.Message, item.AddedOn, item.status);
                    }
                    else
                    {
                        if (item.FromUserId == 0 && item.ToUserId == 0)
                        {
                            new Messages().UpdateMessage(item.MessageId, 0);
                        }
                        else
                        {
                            new Messages().UpdateMessage(item.MessageId, 1);
                        }
                    }
                }
                if (localdeleted.DeletedActivity != null)
                {
                    foreach (var item in localdeleted.DeletedActivity)
                    {
                        new Activities().DeleteActivity(item.PrimaryId);
                    }
                }
                if (localdeleted.DeletedProperty != null)
                {
                    foreach (var item in localdeleted.DeletedProperty)
                    {
                        new Property().DeleteProperty(item.PrimaryId);
                    }
                }
                
                foreach (var item2 in BroadCastMessages)
	            {
		            var broadcastUsers =new Messages().SendBroadCastMessage(item2.activityId, item2.message).GroupBy(x => x.userid).Select(y => y.First());

                    if (broadcastUsers != null && broadcastUsers.Count() > 0)
                    {
                        var fromuserinfo = (from s in Data.Entities.tblusers
                                            join a in Data.Entities.tblactivities on s.id equals a.userid
                                            where a.id == item2.activityId
                                            select new { s.username, s.id }).FirstOrDefault();
                        BeeKeepersEntities entity = new BeeKeepersEntities();
                        List<UserEmail> lstuseremail = new List<UserEmail>();
                        foreach (var item in broadcastUsers)
                        {
                            tblmessage objmessage = new tblmessage();
                            objmessage.addedon = Convert.ToDateTime(DateTime.Now.ToString("MMM/dd/yyyy h:mm:ss tt"));
                            objmessage.body = item2.message;
                            objmessage.fromuserid = fromuserinfo.id;
                            objmessage.status = 1;
                            objmessage.touserid = item.userid;
                            entity.tblmessages.AddObject(objmessage);

                            string LangEmail = new Common().GetLanguage4Emails(item.userid);
                            string LangNoti = new Common().GetLanguage4Notification(item.userid);

                            UserEmail objuseremail = new UserEmail();
                            objuseremail.fromUserName = fromuserinfo.username;
                            objuseremail.toUserName = item.username;
                            objuseremail.toEmailId = item.email;
                            objuseremail.message = fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "sentyouamessage");
                            objuseremail.UserLanguage = LangEmail;
                            lstuseremail.Add(objuseremail);
                            int? badgecount = new Common().GetNotificationCounter(item.userid);
                            if (item.devicetype != null && item.devicetoken != null && item.devicetype == "1")
                            {
                                iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromuserinfo.id, 0, "");
                            }
                            else if (item.devicetype != null && item.devicetoken != null && item.devicetype == "2")
                            {
                                AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromuserinfo.id, 0, "");
                            }
                        }
                        EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Message);
                        AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                        IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail, cb, emailDelegate);
                        entity.SaveChanges();
                    }
	            }
                scalarresponse.result = 1;
            }
            catch (Exception ex)
            {
                scalarresponse.result = -1;
            }
            return scalarresponse;
        }
        public static void MyAsyncCallback(IAsyncResult ar)
        {

        }
        public ApiResponse<ScalarResponse> AddLocalDataJson(List<LocalActivity> localactivity, List<LocalProperty> localproperty, List<LocalMessage> localmsg, LocalDeletedData localdeleted, List<BroadcastMessageRequest> BroadCastMessages,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                objresponse = AddLocalData(localactivity, localproperty, localmsg, localdeleted, BroadCastMessages);
                
                if (objresponse.result == 1)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Localdatauploadedonserver"), "1");
                }
                else
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponse<SynchData> GetAllDataSynchJson(long userid, string latitude, string longitude, long installationid, string LastSynchDateTime, bool IsDistanceGT50,string DeviceLang)
        {
            SynchData objresponse = new SynchData();
            ApiResponse<SynchData> objapiresponse = new ApiResponse<SynchData>();
            try
            {
                objresponse = GetAllSynchData(userid, latitude, longitude, installationid, LastSynchDateTime, IsDistanceGT50);
                
                if (objresponse != null)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Synchronizedataget"), "1");
                }
                else
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
    }
}
