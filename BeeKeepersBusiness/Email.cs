﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersBusiness
{
    public class Email
    {
        public static void Send(MailMessage msg)
        {

            //SmtpClient smtp = new SmtpClient();
            //smtp.UseDefaultCredentials = true;
            //smtp.Host = "localhost";
            //smtp.Port = 25;
            ////msg.CC.Add("hemant.sharma@dotsquares.com");
            //smtp.Send(msg);

            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = false;
            smtp.Host = "mail.dotsquares.com";
            smtp.Credentials = new System.Net.NetworkCredential("wwwsmtp@dotsquares.com", "dsmtp909#");
            smtp.Port = 25;
            smtp.Send(msg);
        }
        public static int SendForgotPasswordMail(string name,string Mailto,string username,string password,string physicalpath,string lang)
        {
            try
            {
                MailMessage msg;
                string htmMessage = System.IO.File.ReadAllText(physicalpath + "\\EmailTemplate\\forgotpassword.html");
                htmMessage = htmMessage.Replace("@v1@", new SiteLanguages().GetResxNameByValue(lang, "Dear"));
                htmMessage = htmMessage.Replace("@v2@", name);
                htmMessage = htmMessage.Replace("@v3@", new SiteLanguages().GetResxNameByValue(lang, "YourpasswordforBeeConnectedis"));
                htmMessage = htmMessage.Replace("@v5@", password);
                htmMessage = htmMessage.Replace("@v6@", new SiteLanguages().GetResxNameByValue(lang, "Thisisanautomatednotification"));
                msg = new MailMessage("donotreply@beeconnected.ca", Mailto, new SiteLanguages().GetResxNameByValue(lang, "PasswordrecoveryforBeeConnected"), htmMessage);
                msg.IsBodyHtml = true;
                Email.Send(msg);
                return 1;
            }
            catch (Exception x)
            {
                return -1;
            }
        }
        public static int SendForgotUsernameMail(string name, string Mailto, string username, string password, string physicalpath, string lang)
        {
            try
            {
                MailMessage msg;
                string htmMessage = System.IO.File.ReadAllText(physicalpath + "\\EmailTemplate\\forgotusername.html");
                htmMessage = htmMessage.Replace("@v1@", new SiteLanguages().GetResxNameByValue(lang, "Dear"));
                htmMessage = htmMessage.Replace("@v2@", name);
                htmMessage = htmMessage.Replace("@v3@", new SiteLanguages().GetResxNameByValue(lang, "YourusernameforBeeConnectedis"));
                htmMessage = htmMessage.Replace("@v5@", username);
                htmMessage = htmMessage.Replace("@v6@", new SiteLanguages().GetResxNameByValue(lang, "Thisisanautomatednotification"));
                msg = new MailMessage("donotreply@beeconnected.ca", Mailto, new SiteLanguages().GetResxNameByValue(lang, "UsernamerecoveryforBeeConnected"), htmMessage);
                msg.IsBodyHtml = true;
                Email.Send(msg);
                return 1;
            }
            catch (Exception x)
            {
                return -1;
            }
        }
        public static int SendResistrationMail(string email,string firstname,string lastname, string usertype,string username,string password,string physicalpath,string lang)
        {
            try
            {
                MailMessage msg;
                string htmMessage = System.IO.File.ReadAllText(physicalpath + "\\EmailTemplate\\userregistration.html");
                htmMessage = htmMessage.Replace("@v1@", new SiteLanguages().GetResxNameByValue(lang, "Dear"));
                htmMessage = htmMessage.Replace("@v2@", firstname + " " + lastname);
                htmMessage = htmMessage.Replace("@v3@", new SiteLanguages().GetResxNameByValue(lang, "WelcometoBeeConnected"));
                htmMessage = htmMessage.Replace("@v4@", new SiteLanguages().GetResxNameByValue(lang, "FormoreinformationonBeeConnected"));
                htmMessage = htmMessage.Replace("@v5@", new SiteLanguages().GetResxNameByValue(lang, "Thisisanautomatednotification"));
                msg = new MailMessage("donotreply@beeconnected.ca", email, new SiteLanguages().GetResxNameByValue(lang, "WelcometoBeeConnected"), htmMessage);
                msg.IsBodyHtml = true;
                Email.Send(msg);
                return 1;
            }
            catch (Exception x)
            {
                return -1;
            }
        }
        
    }
}
