﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersDataBase;
using BeeKeepersBusiness.utility;
namespace BeeKeepersBusiness
{
    public class Messages : Business.BusinessBase
    {

        delegate void EmailDelegate(List<UserEmail> lstuseremail);


        public Int64? SendMessage(Int64 fromUserId, Int64 toUserId, string message, string addedon, int status)
        {
            return MessageRepository.SendMessage(fromUserId, toUserId, message, addedon, status);
        }
        public Int64? UpdateMessage(Int64 MessageId, int status)
        {
            return MessageRepository.UpdateMessageStatus(MessageId, status);
        }
        public List<sp_SendBroadCastMessage_Result> SendBroadCastMessage(Int64 activityId, string message)
        {
            return MessageRepository.SendBroadCastMessageUserList(activityId, message);
        }
        public int SendBroadcastMessage4web(Int64 activityId, string message)
        {
            try
            {
                var broadcastUsers = SendBroadCastMessage(activityId, message).GroupBy(s => s.userid).Select(s => s.First());
                if (broadcastUsers != null && broadcastUsers.Count() > 0)
                {
                    var fromuserinfo = (from s in Data.Entities.tblusers
                                        join a in Data.Entities.tblactivities on s.id equals a.userid
                                        where a.id == activityId
                                        select new { s.username, s.id }).FirstOrDefault();
                    BeeKeepersEntities entity = new BeeKeepersEntities();
                    List<UserEmail> lstuseremail = new List<UserEmail>();
                    foreach (var item in broadcastUsers)
                    {

                        tblmessage objmessage = new tblmessage();
                        objmessage.addedon = DateTime.Now;
                        objmessage.body = message;
                        objmessage.fromuserid = fromuserinfo.id;
                        objmessage.status = 1;
                        objmessage.touserid = item.userid;
                        entity.tblmessages.AddObject(objmessage);
                        
                        string LangEmail = new Common().GetLanguage4Emails(item.userid);
                        string LangNoti = new Common().GetLanguage4Notification(item.userid);

                        UserEmail objuseremail = new UserEmail();
                        objuseremail.fromUserName = fromuserinfo.username;
                        objuseremail.toUserName = item.username;
                        objuseremail.toEmailId = item.email;
                        objuseremail.message = fromuserinfo.username + " "+new SiteLanguages().GetResxNameByValue(LangEmail,"sentyouamessage");
                        objuseremail.UserLanguage = LangEmail;
                        lstuseremail.Add(objuseremail);
                        int? badgecount = new Common().GetNotificationCounter(item.userid);
                        if (item.devicetype != null && item.devicetoken != null && item.devicetype == "1")
                        {
                            iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromuserinfo.id, objmessage.id, message);
                        }
                        else if (item.devicetype != null && item.devicetoken != null && item.devicetype == "2")
                        {
                            AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromuserinfo.id, objmessage.id, message);
                        }
                    }
                    EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Message);
                    AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                    IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail, cb, emailDelegate);
                    entity.SaveChanges();
                    return broadcastUsers.Count();
                }
                else
                {
                    return -2;
                }

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public static void MyAsyncCallback(IAsyncResult ar)
        {

        }
        public List<GetMessagesResponse> GetMessages(Int64 userId, Int64 otherUserId)
        {
            List<GetMessagesResponse> lstgetmessage = new List<GetMessagesResponse>();
            GetMessagesResponse objmessage = new GetMessagesResponse();
            var messages = MessageRepository.GetMessages(userId, otherUserId);
            if (messages != null)
            {
                foreach (var item in messages)
                {
                    objmessage = new GetMessagesResponse();
                    objmessage.date = Convert.ToDateTime(item.addedon).ToString("M/d/yyyy h:mm:ss tt");
                    objmessage.fromUserId = item.fromuserid;
                    objmessage.fromUsername = item.username;
                    objmessage.fromUserType = item.usertype;
                    objmessage.message = item.body;
                    objmessage.toUserId = item.touserid;
                    objmessage.messageId = item.id;
                    lstgetmessage.Add(objmessage);
                }
            }
            return lstgetmessage;
        }
        public GetMessagesResponseNew GetMessagesNew(Int64 userId, Int64 otherUserId, int pagenumber, int pagesize)
        {
            List<GetMessagesResponse> lstgetmessage = new List<GetMessagesResponse>();
            //List<GetMessagesResponse> lstgetmessageNew = new List<GetMessagesResponse>();
            GetMessagesResponseNew objmessageNew = new GetMessagesResponseNew();
            GetMessagesResponse objmessage = new GetMessagesResponse();
            var messages = MessageRepository.GetMessages(userId, otherUserId).ToList();
            int messagecountTotal = messages.Count();
            if (pagenumber == 0)
            {
                if (messages.Where(s => s.status == 1).Count() < 10)
                {
                    messages = messages.Take(10).ToList();
                }
                else
                {
                    messages = messages.Take(messages.Where(s => s.status == 1).Count()).ToList();
                }
            }
            else
            {
                messages = messages.Skip(pagenumber).Take(pagesize).ToList();
            }
            int messagecountFiltered = messages.Count();
            if (messages != null)
            {
                foreach (var item in messages)
                {
                    objmessage = new GetMessagesResponse();
                    objmessage.date = Convert.ToDateTime(item.addedon).ToString("M/d/yyyy h:mm:ss tt");
                    objmessage.fromUserId = item.fromuserid;
                    objmessage.fromUsername = item.username;
                    objmessage.fromUserType = item.usertype;
                    objmessage.message = item.body;
                    objmessage.toUserId = item.touserid;
                    objmessage.messageId = item.id;
                    objmessage.status = item.status;
                    lstgetmessage.Add(objmessage);
                }
                if (messagecountTotal > (pagenumber + messages.Count()))
                {
                    objmessageNew.flag_Records = 1;
                }
                else
                {
                    objmessageNew.flag_Records = 0;
                }
                objmessageNew.listMessage = lstgetmessage;
            }
            return objmessageNew;
        }
        //public List<GetInboxUserResponse> getUsersListForInbox(Int64 userId)
        //{
        //    List<GetInboxUserResponse> lstgetuser = new List<GetInboxUserResponse>();
        //    GetInboxUserResponse objuser = new GetInboxUserResponse();
        //    var users = MessageRepository.getUsersListForInbox(userId);
        //    if (users != null)
        //    {
        //        int status = 2;// set read / unread message status
        //        var adminmsg = MessageRepository.GetAdminMessagesWithoutstatusupdate(userId).OrderByDescending(s => s.addedon).ToList();
        //        long? lastmsgdateTicks = null;
        //        if (adminmsg.FirstOrDefault() != null && adminmsg.FirstOrDefault().addedon != null)
        //        {
        //            lastmsgdateTicks = adminmsg.FirstOrDefault().addedon.Ticks;
        //        }
        //        if (MessageRepository.GetAdminMessagesWithoutstatusupdate(userId).Where(s => s.status == 1).Count() > 0)
        //        {
        //            status = 1;
        //        }
        //        objuser = new GetInboxUserResponse();
        //        objuser.userId = 0;
        //        objuser.userName = "admin";
        //        objuser.userType = 0;
        //        objuser.status = status;
        //        objuser.lastMsgDateTime = lastmsgdateTicks;
        //        lstgetuser.Add(objuser);

        //        foreach (var item in users)
        //        {
        //            objuser = new GetInboxUserResponse();
        //            long? lastmsgdateTicks1 = null;
        //            objuser.userId = item.userid;
        //            objuser.userName = item.username;
        //            objuser.userType = item.usertype;
        //            objuser.status = item.status;
        //            if (item.lastmsgdate != null)
        //            {
        //                lastmsgdateTicks1 = item.lastmsgdate.Value.Ticks;
        //            }
        //            objuser.lastMsgDateTime = lastmsgdateTicks1;
        //            lstgetuser.Add(objuser);
        //        }

        //    }
        //    return lstgetuser.OrderBy(s => s.status).OrderByDescending(s => s.lastMsgDateTime).ToList();
        //}
        public List<GetInboxUserResponse> getUsersListForInbox(Int64 userId)
        {
            List<GetInboxUserResponse> lstgetuser = new List<GetInboxUserResponse>();
            GetInboxUserResponse objuser = new GetInboxUserResponse();
            var users = MessageRepository.getUsersListForInbox(userId);
            if (users != null)
            {
                int status = 2;// set read / unread message status
                var adminmsg = MessageRepository.GetAdminMessagesWithoutstatusupdate(userId).OrderByDescending(s => s.addedon).ToList();
                long? lastmsgdateTicks = null;
                if (adminmsg.FirstOrDefault() != null && adminmsg.FirstOrDefault().addedon != null)
                {
                    lastmsgdateTicks = adminmsg.FirstOrDefault().addedon.Ticks;
                }
                if (MessageRepository.GetAdminMessagesWithoutstatusupdate(userId).Where(s => s.status == 1).Count() > 0)
                {
                    status = 1;
                }
                objuser = new GetInboxUserResponse();
                objuser.userId = 0;
                objuser.userName = "admin";
                objuser.userType = 0;
                objuser.status = status;
                objuser.lastMsgDateTime = lastmsgdateTicks;
                lstgetuser.Add(objuser);

                foreach (var item in users)
                {
                    objuser = new GetInboxUserResponse();
                    long? lastmsgdateTicks1 = null;
                    objuser.userId = item.userid;
                    objuser.userName = item.username;
                    objuser.userType = item.usertype;
                    objuser.status = item.status;
                    if (item.lastmsgdate != null)
                    {
                        lastmsgdateTicks1 = item.lastmsgdate.Value.Ticks;
                    }
                    objuser.lastMsgDateTime = lastmsgdateTicks1;
                    lstgetuser.Add(objuser);
                }

            }
            return lstgetuser.OrderBy(s => s.status).ThenByDescending(s => s.lastMsgDateTime).ToList();
        }
        public Int64 Installation(string deviceid, string devicetoken, int devicetype)
        {
            return MessageRepository.Installation(deviceid,devicetoken, devicetype);
        }
        public Int64 SetUserInstallation(Int64 userid, Int64 installationid)
        {
            return MessageRepository.SetUserInstallation(userid, installationid);
        }
        public Int64 DeleteInstallation(Int64 installationid)
        {
            return MessageRepository.DeleteInstallation(installationid);
        }

        #region service methods
        public ApiResponse<MessageResponse> SendMessageJson(Int64 fromUserId, Int64 toUserId, string message,string DeviceLang)
        {
            MessageResponse objresponse = new MessageResponse();
            ApiResponse<MessageResponse> objapiresponse = new ApiResponse<MessageResponse>();
            try
            {
                Int64? result = SendMessage(fromUserId, toUserId, message, "",1);
                if (result > 0)
                {
                    var installation = Data.Entities.tblinstallations.Where(s => s.userid == toUserId).ToList();
                    var fromuserinfo = Data.Entities.tblusers.Where(s => s.id == fromUserId).FirstOrDefault();
                    var touserinfo = Data.Entities.tblusers.Where(s => s.id == toUserId).FirstOrDefault();


                    if (installation != null)
                    {
                        List<UserEmail> lstuseremail = new List<UserEmail>();
                        string LangEmail = new Common().GetLanguage4Emails(touserinfo.id);
                        UserEmail objuseremail = new UserEmail();
                        objuseremail.fromUserName = fromuserinfo.username;
                        objuseremail.toUserName = touserinfo.username;
                        objuseremail.toEmailId = touserinfo.email;
                        objuseremail.message = fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "sentyouamessage");
                        objuseremail.UserLanguage = LangEmail;
                        lstuseremail.Add(objuseremail);

                        int? badgecount = new Common().GetNotificationCounter(toUserId);
                        foreach (var item in installation)
                        {
                            
                            string LangNoti = new Common().GetLanguage4Notification(item.userid);
                            if (item.devicetype == "1")
                            {
                                iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromUserId, (Int64)result, message);
                            }
                            else if (item.devicetype == "2")
                            {
                                AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromUserId, (Int64)result, message);
                            }
                        }
                        EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Message);
                        AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                        IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail, cb, emailDelegate);

                    }
                    using (BeeKeepersEntities entities = new BeeKeepersEntities())
                    {
                        var objmessage = entities.tblmessages.Where(s => s.id == result.Value).FirstOrDefault();
                        if (objmessage != null)
                        {
                            objresponse.result = result.Value;
                            objresponse.message = message;
                            objresponse.addedon = objmessage.addedon.ToString("M/d/yyyy h:mm:ss tt");

                            objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Messagesentsuccessfully"), "1");
                        }
                        else
                        {
                            objresponse.result = -1;
                            objresponse.message = "";
                            objresponse.addedon = "";

                            objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                        }

                    }

                }
                else
                {
                    objresponse.result = -1;
                    objresponse.message = "";
                    objresponse.addedon = "";

                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                
                objresponse.result = -1;
                objresponse.message = "";
                objresponse.addedon = "";
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponse<ScalarResponse> SendBroadCastMessageJson(Int64 activityId, string message,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                var broadcastUsers = SendBroadCastMessage(activityId, message).GroupBy(x => x.userid).Select(y => y.First());
                if (broadcastUsers != null && broadcastUsers.Count() > 0)
                {
                    var fromuserinfo = (from s in Data.Entities.tblusers
                                        join a in Data.Entities.tblactivities on s.id equals a.userid
                                        where a.id == activityId
                                        select new { s.username, s.id }).FirstOrDefault();
                    BeeKeepersEntities entity = new BeeKeepersEntities();
                    List<UserEmail> lstuseremail = new List<UserEmail>();
                    foreach (var item in broadcastUsers)
                    {
                        tblmessage objmessage = new tblmessage();
                        objmessage.addedon = Convert.ToDateTime(DateTime.Now.ToString("MMM/dd/yyyy h:mm:ss tt"));
                        objmessage.body = message;
                        objmessage.fromuserid = fromuserinfo.id;
                        objmessage.status = 1;
                        objmessage.touserid = item.userid;
                        entity.tblmessages.AddObject(objmessage);

                        string LangEmail = new Common().GetLanguage4Emails(item.userid);
                        string LangNoti = new Common().GetLanguage4Notification(item.userid);

                        UserEmail objuseremail = new UserEmail();
                        objuseremail.fromUserName = fromuserinfo.username;
                        objuseremail.toUserName = item.username;
                        objuseremail.toEmailId = item.email;
                        objuseremail.message = fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "sentyouamessage");
                        objuseremail.UserLanguage = LangEmail;
                        lstuseremail.Add(objuseremail);
                        int? badgecount = new Common().GetNotificationCounter(item.userid);
                        if (item.devicetype != null && item.devicetoken != null && item.devicetype == "1")
                        {
                            iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromuserinfo.id, objmessage.id, message);
                        }
                        else if (item.devicetype != null && item.devicetoken != null && item.devicetype == "2")
                        {
                            AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangNoti, "sentyouamessage"), badgecount, "Message", fromuserinfo.id, objmessage.id, message);
                        }
                    }
                    EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Message);
                    AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                    IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail, cb, emailDelegate);
                    entity.SaveChanges();
                    objresponse.result = 1;

                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Messagesentsuccessfully"), "1");
                }
                else
                {
                    
                    objresponse.result = -3;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Usersnotavailabletosendmessage"), "0");
                }

            }
            catch (Exception ex)
            {
                
                errorlogBusiness log = new errorlogBusiness();
                log.InsertErrorLog(0, "broadcast", ex.Message, "");
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }

        public ApiResponseList<GetMessagesResponse> GetMessagesJson(Int64 userId, Int64 otherUserId,string DeviceLang)
        {
            List<GetMessagesResponse> objresponse = new List<GetMessagesResponse>();
            ApiResponseList<GetMessagesResponse> objapiresponse = new ApiResponseList<GetMessagesResponse>();
            try
            {
                objresponse = GetMessages(userId, otherUserId);
                
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Messageslistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nomessagesininbox"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public GetMessagesResponse GetMessageById(Int64 MsgID)
        {
            GetMessagesResponse objmessage = new GetMessagesResponse();
            var messages = MessageRepository.GetMessageById(MsgID);
            if (messages != null)
            {
                objmessage = new GetMessagesResponse();
                objmessage.date = Convert.ToDateTime(messages.addedon).ToString("M/d/yyyy h:mm:ss tt");
                objmessage.fromUserId = messages.fromuserid;
                objmessage.fromUsername = messages.username;
                objmessage.fromUserType = messages.usertype;
                objmessage.message = messages.body;
                objmessage.toUserId = messages.touserid;
                objmessage.messageId = messages.id;
            }
            return objmessage;
        }
        public ApiResponse<GetMessagesResponse> GetMessageByIdJson(Int64 MsgId, string DeviceLang)
        {
            GetMessagesResponse objresponse = new GetMessagesResponse();
            ApiResponse<GetMessagesResponse> objapiresponse = new ApiResponse<GetMessagesResponse>();
            try
            {
                objresponse = GetMessageById(MsgId);

                if (objresponse != null)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nomessagesininbox"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nomessagesininbox"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponse<GetMessagesResponseNew> GetMessagesNewJson(Int64 userId, Int64 otherUserId, int pagenumber, int pagesize,string DeviceLang)
        {
            GetMessagesResponseNew objresponse = new GetMessagesResponseNew();
            ApiResponse<GetMessagesResponseNew> objapiresponse = new ApiResponse<GetMessagesResponseNew>();
            try
            {
                objresponse = GetMessagesNew(userId, otherUserId, pagenumber, pagesize);
                
                if (objresponse != null)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Messageslistget"), "1");
                }
                else
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }

            return objapiresponse;
        }

        public ApiResponseList<GetInboxUserResponse> getUsersListForInboxJson(Int64 userId,string DeviceLang)
        {
            List<GetInboxUserResponse> objresponse = new List<GetInboxUserResponse>();
            ApiResponseList<GetInboxUserResponse> objapiresponse = new ApiResponseList<GetInboxUserResponse>();
            try
            {
                objresponse = getUsersListForInbox(userId);
                
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "UserslistGet"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nomessagesininbox"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }

            return objapiresponse;
        }
        public ApiResponse<ScalarResponse> InstallationJson(string deviceid, string devicetoken, int devicetype, string Lang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                if (devicetoken != null && devicetoken != "")
                {
                    Int64 result = Installation(deviceid, devicetoken, devicetype);
                    
                    if (result > 0)
                    {
                        objresponse.result = result;
                        objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Lang,"Installationsuccessfully"), "1");
                    }
                    else
                    {
                        objresponse.result = -1;
                        objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Lang,"ThereisaproblemPleasetryagainlater"), "0");
                    }
                }
                else
                {
                    
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Lang,"Devicetokenisempty"), "0");
                }
            }
            catch (Exception ex)
            {
                
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse,new SiteLanguages().GetResxNameByValue(Lang,"ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponse<ScalarResponse> SetUserInstallationJson(Int64 userid, Int64 installationid,string Lang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                Int64 result = SetUserInstallation(userid, installationid);
                
                if (result > 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Lang, "Userhasbeensettoinstallationsuccessfully"), "1");
                }
                else if (result == -2)
                {
                    objresponse.result = -2;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Lang, "Installationnotfound"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Lang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(Lang, "ThereisaproblemPleasetryagainlater"), "0");
            }

            return objapiresponse;
        }
        public ApiResponse<ScalarResponse> DeleteInstallationJson(Int64 installationid)
        {
            string DeviceLang = new Common().GetDeviceLanguageByToken((int)installationid);
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                Int64 result = DeleteInstallation(installationid);
                
                if (result > 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Installationdeletedsuccessfully"), "1");
                }
                else if (result == -2)
                {
                    objresponse.result = -2;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Installationnotfound"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public List<UnreadCountResponse> UnreadMsgCounter(Int64 userId)
        {
            List<UnreadCountResponse> lstunreadCount = (from a in MessageRepository.getUnreadMsgCounter(userId).AsEnumerable() select new UnreadCountResponse { fromUserId = a.fromuserid, toUserId = a.touserid, unReadCount = a.unreadcount.ToString() }).ToList();
            lstunreadCount.Add(new UnreadCountResponse { fromUserId = 0, toUserId = userId, unReadCount = MessageRepository.GetUnreadAdminCounter(userId) });
            return lstunreadCount;
        }
        public ApiResponseList<UnreadCountResponse> UnreadMsgCounterJson(Int64 userId, string DeviceLang)
        {
            List<UnreadCountResponse> objresponse = new List<UnreadCountResponse>();
            ApiResponseList<UnreadCountResponse> objapiresponse = new ApiResponseList<UnreadCountResponse>();
            try
            {
                objresponse = UnreadMsgCounter(userId);
                
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "UserslistGet"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Nomessagesininbox"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }

            return objapiresponse;
        }
        #endregion

    }
}
