﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeeKeepersBusiness
{
    public class ApiResponseList<T>
    {
        public string status { get; set; }
        public string message { get; set; }
        public List<T> data { get; set; }
        public void AddResponsePacketList(List<T> data, string message, string status)
        {
            this.status = status;
            this.data = data;
            this.message = message;
        }
    }
}
