﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using BeeKeepersBusiness;

namespace BeeKeepersBusiness.utility
{
    public class AndroidPushNotice
    {

        delegate void MethodDelegate(string deviceToken, string message, int? badge, string alertFor, Int64? primaryId, Int64? MessageId, string msgtxt);


        public static void SendNotice(string deviceToken, string message, int? badge, string alertFor, Int64? primaryId, Int64? MessageId, string msgtxt)
        {

            MethodDelegate dlgt = new MethodDelegate(AndroidPush);
            dlgt.BeginInvoke(deviceToken, message, badge, alertFor, primaryId, MessageId, msgtxt, null, null);
        }



        static void AndroidPush(string devicetoken, string message, int? badgeCount, string alertFor, Int64? primaryId, Int64? MessageId, string msgtxt)
        {
            // your RegistrationID paste here which is received from GCM server.                                                               
            string regId = devicetoken;// "APA91bG_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_V6hO2liMx-eIGAbG2cR4DiIgm5Q";
            // applicationID means google Api key                                                                                                     
            var applicationID = BeeKeepersBusiness.Properties.Settings1.Default.applicationID;
            // SENDER_ID is nothing but your ProjectID (from API Console- google code)//                                          
            var SENDER_ID = BeeKeepersBusiness.Properties.Settings1.Default.SENDER_ID;

            var value = message; //message text box                                                                               

            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
            //Data post to server                                                                                                                                         
            string postData =
           "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&data.time=" + System.DateTime.Now.ToString() + "&registration_id=" + regId + "&data.alertFor=" + alertFor + "&data.primaryId=" + primaryId + "&data.msgid=" + MessageId + "&data.msgtxt=" + msgtxt + "&data.badge=" + badgeCount;
            //Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;
            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse tResponse = tRequest.GetResponse();
            dataStream = tResponse.GetResponseStream();
            StreamReader tReader = new StreamReader(dataStream);
            String sResponseFromServer = tReader.ReadToEnd();   //Get response from GCM server.
            //Assigning GCM response to Label text 


            using (errorlogBusiness objError = new errorlogBusiness())
                objError.InsertErrorLog(0, "AndroidPushNotice: ", postData, sResponseFromServer);



            tReader.Close();
            dataStream.Close();
            tResponse.Close();
        }
    }
}
