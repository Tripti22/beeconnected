﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JdSoft.Apple.Apns.Notifications;
using BeeKeepersBusiness;

namespace BeeKeepersBusiness.utility
{
    public class iPhonePush
    {
        delegate void MethodDelegate(string deviceToken, string message, int? badge, string alertFor, Int64? primaryId, Int64? MessageId, string msgtxt);


        public static void SendNotice(string deviceToken, string message, int? badge, string alertFor, Int64? primaryId, Int64? MessageId, string msgtxt)
        {
            //SendNotificationMessage(deviceToken, message, badge);
            MethodDelegate dlgt = new MethodDelegate(SendNotificationMessage);
            dlgt.BeginInvoke(deviceToken, message, badge,alertFor,primaryId,MessageId,msgtxt, null, null);
        }

        static void SendNotificationMessage(string deviceToken, string message, int? badge, string alertFor, Int64? primaryId, Int64? MessageId, string msgtxt)
        {

            //True if you are using sandbox certificate, or false if using production
            bool sandbox = false;
            //bool sandbox = true;

            //Put your PKCS12 .p12 or .pfx filename here.
            //string p12File = "Development.p12"; //demo
            string p12File = "Production.p12";  //live 
            //string p12File = "testnotification.p12";  //live 

            //This is the password that you protected your p12File 
            //string p12FilePassword = "admin"; // "yourpassword";
            string p12FilePassword = "1"; // "yourpassword";
            // This is just to demonstrate that the APNS connection stays alive between messages
            int sleepBetweenNotifications = 5000;

            //Actual Code starts below:
            //--------------------------------

            string p12Filename = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "iPhoneAPNS_Certificate\\" + p12File);

            NotificationService service = new NotificationService(sandbox, p12Filename, p12FilePassword, 1);

            service.SendRetries = 5; //5 retries before generating notificationfailed event
            service.ReconnectDelay = 5000; //5 seconds

            service.Error += new NotificationService.OnError(service_Error);
            service.NotificationTooLong += new NotificationService.OnNotificationTooLong(service_NotificationTooLong);

            service.BadDeviceToken += new NotificationService.OnBadDeviceToken(service_BadDeviceToken);
            service.NotificationFailed += new NotificationService.OnNotificationFailed(service_NotificationFailed);
            service.NotificationSuccess += new NotificationService.OnNotificationSuccess(service_NotificationSuccess);
            service.Connecting += new NotificationService.OnConnecting(service_Connecting);
            service.Connected += new NotificationService.OnConnected(service_Connected);
            service.Disconnected += new NotificationService.OnDisconnected(service_Disconnected);


            //Create a new notification to send
            Notification alertNotification = new Notification(deviceToken);

            alertNotification.Payload.Alert.Body = string.Format(message, badge);
            alertNotification.Payload.Sound = "default";
            alertNotification.Payload.Badge = badge;
            alertNotification.Payload.AddCustom("alertFor", alertFor);
            alertNotification.Payload.AddCustom("primaryId", primaryId);
            alertNotification.Payload.AddCustom("MessageId", MessageId);
            alertNotification.Payload.AddCustom("msgtxt", msgtxt);
            //alertNotification.Payload.CustomItems.Add("contentType", new object[] { MessageType });

            service.QueueNotification(alertNotification);

            //if (!service.QueueNotification(alertNotification))
            //    //    using (ErrorLog objError = new ErrorLog())
            //    //        objError.InsertErrorLog("IphonePush1", "IphonePush:Queue", "Notification Queued!");
            //    //else
            //    using (ErrorLog objError = new ErrorLog())
            //        objError.InsertErrorLog("IphonePush1", "IphonePush:Queue", "Notification Failed to be Queued!");

            System.Threading.Thread.Sleep(sleepBetweenNotifications);
            service.Close();
            //Clean up
            service.Dispose();


            ////string filePath = @"C:\Documents and Settings\admin\Desktop\Moon-APNS-master\Moon-APNS-master\Application\MoonApns\CertificatesMyFacePaidP12.p12";
            //// if you are using production mode then false otherwise for sandbox mode use tru
            //bool mode = false;
            //string p12File = string.Empty;
            //p12File = "CarAprCertificates.p12";
            //string p12Filename = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "iPhoneAPNS_Certificate\\" + p12File);
            //string password = "admin";
            //var payload1 = new NotificationPayload(deviceToken, message, 1, "default");
            ////payload1.AddCustom("contentType", MessageType);
            //var p = new List<NotificationPayload> { payload1 };
            //var push = new PushNotification(mode, p12Filename, password);
            //var rejected = push.SendToApple(p);
            //if (rejected.Count > 0)
            //{
            //    using (errorlogBusiness objError = new errorlogBusiness())
            //        objError.InsertErrorLog(0, "SendNotificationMessage", Convert.ToString(rejected[0]), "IphonePush:rejected1");
            //    var rejected1 = push.SendToApple(p);
            //    if (rejected1.Count > 0)
            //    {
            //        System.Threading.Thread.Sleep(1000);
            //        using (errorlogBusiness objError = new errorlogBusiness())
            //            objError.InsertErrorLog(0, "SendNotificationMessage", Convert.ToString(rejected1[0]), "IphonePush:rejected2");
            //    }
            //}

        }

        static void service_BadDeviceToken(object sender, BadDeviceTokenException ex)
        {

        }

        static void service_Disconnected(object sender)
        {
        }

        static void service_Connected(object sender)
        {
        }

        static void service_Connecting(object sender)
        {
        }

        static void service_NotificationTooLong(object sender, NotificationLengthException ex)
        {
            using (errorlogBusiness objError = new errorlogBusiness())
                objError.InsertErrorLog(0, "service_NotificationTooLong: ", ex.Message.ToString(), "service_NotificationTooLong");

        }

        static void service_NotificationSuccess(object sender, Notification notification)
        {
            using (errorlogBusiness objError = new errorlogBusiness())
                objError.InsertErrorLog(0, "service_NotificationSuccess: ", notification.Payload.ToString(), "service_NotificationSuccess:" + notification.DeviceToken);
        }

        static void service_NotificationFailed(object sender, Notification notification)
        {
            using (errorlogBusiness objError = new errorlogBusiness())
                objError.InsertErrorLog(0, "service_NotificationFailed: ", notification.Payload.ToString(), "service_NotificationFailed:" + notification.DeviceToken);

        }

        static void service_Error(object sender, Exception ex)
        {
            using (errorlogBusiness objError = new errorlogBusiness())
                objError.InsertErrorLog(0, "service_Error111", ex.Message.ToString(), ex.Data.ToString());
        }

    }
}
