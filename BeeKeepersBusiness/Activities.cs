﻿using BeeKeepersBusiness.utility;
using BeeKeepersDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersBusiness
{
    public class Activities : Business.BusinessBase
    {
        delegate void EmailDelegate(List<UserEmail> lstuseremail);
        public static void MyAsyncCallback(IAsyncResult ar)
        {

        }

        public Int64? AddActivity(Int64 userId, Int32 userType, string note, string latitude, string longitude, string startDate, string endDate)
        {
            Int64? result =ActivityRepository.AddActivity(userId, userType, note, latitude, longitude, startDate, endDate);
            return result;
        }
        public void SendNotificationToUsers(Int64? actid, Int64 userId, Int32 userType, string latitude, string longitude, string startDate, string endDate, string actnote)
        {
            try
            {

                var userswithactivity = ActivityRepository.GetUsersForNotification(userId, userType, latitude, longitude, startDate, endDate).GroupBy(s => s.devicetoken).Select(s => s.First());
                var users = userswithactivity.GroupBy(s => s.userid).Select(grp => grp.First()).ToList();
                if (users != null && users.Count() > 0)
                {
                    List<UserEmail> lstuseremail = new List<UserEmail>();
                    foreach (var item in users)
                    {

                        var activities = userswithactivity.GroupBy(s => s.id).Select(grp => grp.First()).ToList();
                        string strnotes = "";
                        foreach (var item2 in activities.Where(s => s.userid == item.userid))
                        {
                            strnotes += item2.note+", ";
                        }
                        if (strnotes != "")
                        {
                            strnotes = strnotes.Remove(strnotes.Length - 2);
                        }
                        string LangEmail = new Common().GetLanguage4Emails(item.userid);
                        string LangNoti = new Common().GetLanguage4Notification(item.userid);

                        var fromuserinfo = Data.Entities.tblusers.Where(s => s.id == userId).FirstOrDefault();
                        UserEmail objuseremail = new UserEmail();
                        objuseremail.fromUserName = fromuserinfo.username;
                        objuseremail.toUserName = item.username;
                        objuseremail.toEmailId = item.email;
                        objuseremail.message = fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "addedanactivity") + " " + actnote + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "arroundyouractivity")+" "+strnotes;
                        objuseremail.UserLanguage = LangEmail;
                        lstuseremail.Add(objuseremail);
                        int? badgecount = new Common().GetNotificationCounter(item.userid);
                        if (item.devicetype == "1")
                        {
                            iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "addedanactivity") + " " + actnote + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "arroundyouractivity") + " " + strnotes, badgecount, "Activity", actid, 0, "");
                        }
                        else if (item.devicetype == "2")
                        {
                            AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "addedanactivity") + " " + actnote + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "arroundyouractivity") + " " + strnotes, badgecount, "Activity", actid, 0, "");
                        }
                    }
                    EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Activity);
                    AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                    IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail,cb, emailDelegate);
                }

                // This isn't great to copy/paste but need it done asap.

                var farmersswithactivity = PropertyRepository.GetUsersForNotification(userId, userType, latitude, longitude, startDate, endDate).GroupBy(s => s.devicetoken).Select(s => s.First());
                var farmers = farmersswithactivity.GroupBy(s => s.userid).Select(grp => grp.First()).ToList();
                if (farmers != null && farmers.Count() > 0)
                {
                    List<UserEmail> lstuseremail = new List<UserEmail>();
                    foreach (var item in farmers)
                    {

                        var activities = farmersswithactivity.GroupBy(s => s.id).Select(grp => grp.First()).ToList();
                        string strnotes = "";
                        /*
                        foreach (var item2 in activities.Where(s => s.userid == item.userid))
                        {
                            strnotes += item2.note + ", ";
                        }
                        if (strnotes != "")
                        {
                            strnotes = strnotes.Remove(strnotes.Length - 2);
                        }
                        */
                        string LangEmail = new Common().GetLanguage4Emails(item.userid);
                        string LangNoti = new Common().GetLanguage4Notification(item.userid);

                        var fromuserinfo = Data.Entities.tblusers.Where(s => s.id == userId).FirstOrDefault();
                        UserEmail objuseremail = new UserEmail();
                        objuseremail.fromUserName = fromuserinfo.username;
                        objuseremail.toUserName = item.username;
                        objuseremail.toEmailId = item.email;
                        objuseremail.message = fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "addedanactivity") + " " + actnote + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "arroundyourproperty") + " " + strnotes;
                        objuseremail.UserLanguage = LangEmail;
                        lstuseremail.Add(objuseremail);
                        int? badgecount = new Common().GetNotificationCounter(item.userid);
                        if (item.devicetype == "1")
                        {
                            iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "addedanactivity") + " " + actnote + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "arroundyourproperty") + " " + strnotes, badgecount, "Activity", actid, 0, "");
                        }
                        else if (item.devicetype == "2")
                        {
                            AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "addedanactivity") + " " + actnote + " " + new SiteLanguages().GetResxNameByValue(LangEmail, "arroundyourproperty") + " " + strnotes, badgecount, "Activity", actid, 0, "");
                        }
                    }
                    EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Activity);
                    AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                    IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail, cb, emailDelegate);
                }
                
            }
            catch (Exception ex)
            {
                using (errorlogBusiness errorlog = new errorlogBusiness())
                {
                    errorlog.InsertErrorLog(0, "notification", ex.Message, "");
                }
            }
        }
        public List<ActivityResponse> MyActivities(Int64 userid, string dateFrom, string dateTo)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            var activities = ActivityRepository.MyActivityList(userid, dateFrom, dateTo);
            List<ActivityResponse> lstactivity = new List<ActivityResponse>();
            foreach (var item in activities)
            {
                ActivityResponse objactivity = new ActivityResponse(); 
                objactivity.id = item.id;
                objactivity.userId= item.userid;
                objactivity.userType= item.usertype;
                objactivity.latitude = item.latitude;
                objactivity.longitude= item.longitude;
                objactivity.note= item.note;
                objactivity.startDate = item.startdate.ToString("M/d/yyyy h:mm:ss tt");
                objactivity.endDate = item.enddate.ToString("M/d/yyyy h:mm:ss tt");
                lstactivity.Add(objactivity);
            }
            return lstactivity;
        }
        public List<sp_ListActivities4Web_Result> ActivityList4Web(Int64 userId, Int64 userType, string latitude, string longitude, string startDate, string endDate, int radius)
        {
            return ActivityRepository.ActivityList4web(userId, userType, latitude, longitude, startDate, endDate, radius);
        }
        public List<ActivityResponse1> ListActivities(Int64 userId, Int64 userType, string latitude, string longitude, string startDate, string endDate, int radius)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            var activities = ActivityRepository.ActivityList(userId, userType, latitude, longitude, startDate, endDate, radius);
            List<ActivityResponse1> lstactivity = new List<ActivityResponse1>();
            foreach (var item in activities)
            {
                ActivityResponse1 objactivity = new ActivityResponse1();
                objactivity.id = item.id;
                objactivity.userId = item.userid;
                objactivity.userType = item.usertype;
                objactivity.latitude = item.latitude;
                objactivity.longitude = item.longitude;
                objactivity.note = item.note;
                objactivity.startDate = item.startdate.ToString("M/d/yyyy h:mm:ss tt");
                objactivity.endDate = item.enddate.ToString("M/d/yyyy h:mm:ss tt");
                objactivity.distance = Convert.ToDecimal(item.distance).ToString("0.00");
                objactivity.userName = item.username;
                lstactivity.Add(objactivity);
            }
            return lstactivity;
        }
        public List<sp_MyActivitiesAround_Result> MyActivitiesAroundList4Web(Int64 userId, Int64 activityId, int radius)
        {
            return ActivityRepository.MyActivitiesAroundList(userId, activityId, radius);
        }
        public List<MyActivitiesAroundResponse> MyActivitiesAround(Int64 userId, Int64 activityId, int radius)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            var activities = ActivityRepository.MyActivitiesAroundList(userId, activityId, radius);
            List<MyActivitiesAroundResponse> lstactivity = new List<MyActivitiesAroundResponse>();
            foreach (var item in activities)
            {
                MyActivitiesAroundResponse objactivity = new MyActivitiesAroundResponse();
                objactivity.id = item.id;
                objactivity.latitude = item.latitude;
                objactivity.longitude = item.longitude;
                objactivity.note = item.note;
                objactivity.startDate = item.startdate.ToString("M/d/yyyy h:mm:ss tt");
                objactivity.endDate = item.enddate.ToString("M/d/yyyy h:mm:ss tt");
                objactivity.distance = Convert.ToDecimal(item.distance).ToString("0.00");
                lstactivity.Add(objactivity);
            }
            return lstactivity;
        }
        public ActivityResponse EditActivity(Int64 activityid)
        {
            try
            {
                BeeKeepersEntities entities = new BeeKeepersEntities();
                var activity = entities.tblactivities.Where(s => s.id == activityid).FirstOrDefault();
                if (activity != null)
                {
                    ActivityResponse objactivity = new ActivityResponse();
                    objactivity.latitude = activity.latitude;
                    objactivity.longitude = activity.longitude;
                    objactivity.id = activity.id;
                    objactivity.note = activity.note;
                    objactivity.startDate = string.Format("{0:M/d/yyyy h:mm:ss tt}", activity.startdate);
                    objactivity.endDate = string.Format("{0:M/d/yyyy h:mm:ss tt}", activity.enddate);
                    objactivity.userId = activity.userid;
                    objactivity.userType = activity.usertype;
                    return objactivity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                using (errorlogBusiness errorlog = new errorlogBusiness())
                {
                    errorlog.InsertErrorLog(0, "Edit activity", "", "");
                }
                return null;
            }
            
        }
        public ActivityResponse EditActivity4Web2(Int64 activityid)
        {
            try
            {
                BeeKeepersEntities entities = new BeeKeepersEntities();
                var activity = entities.tblactivities.Where(s => s.id == activityid).FirstOrDefault();
                if (activity != null)
                {
                    ActivityResponse objactivity = new ActivityResponse();
                    objactivity.latitude = activity.latitude;
                    objactivity.longitude = activity.longitude;
                    objactivity.id = activity.id;
                    objactivity.note = activity.note;
                    objactivity.startDate = activity.startdate.ToString("dd-MM-yyyy");
                    objactivity.endDate = activity.enddate.ToString("dd-MM-yyyy");
                    //objactivity.endDate = string.Format("{0:M/d/yyyy h:mm:ss tt}", activity.enddate);
                    //objactivity.endDate = string.Format("{0:M/d/yyyy h:mm:ss tt}", activity.enddate);
                    objactivity.userId = activity.userid;
                    objactivity.userType = activity.usertype;
                    return objactivity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                using (errorlogBusiness errorlog = new errorlogBusiness())
                {
                    errorlog.InsertErrorLog(0, "Edit activity", "", "");
                }
                return null;
            }

        }
        public ActivityResponse EditActivity4Web(Int64 activityid)
        {
            try
            {
                BeeKeepersEntities entities = new BeeKeepersEntities();
                var activity = entities.tblactivities.Where(s => s.id == activityid).FirstOrDefault();
                if (activity != null)
                {
                    ActivityResponse objactivity = new ActivityResponse();
                    objactivity.latitude = activity.latitude;
                    objactivity.longitude = activity.longitude;
                    objactivity.id = activity.id;
                    objactivity.note = activity.note;
                    objactivity.startDate = activity.startdate.ToString("dd/MM/yyyy");
                    objactivity.endDate = activity.enddate.ToString("dd/MM/yyyy");
                    //objactivity.endDate = string.Format("{0:M/d/yyyy h:mm:ss tt}", activity.enddate);
                    //objactivity.endDate = string.Format("{0:M/d/yyyy h:mm:ss tt}", activity.enddate);
                    objactivity.userId = activity.userid;
                    objactivity.userType = activity.usertype;
                    return objactivity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                using (errorlogBusiness errorlog = new errorlogBusiness())
                {
                    errorlog.InsertErrorLog(0, "Edit activity", "", "");
                }
                return null;
            }

        }
        public Int64? UpdateActivity(Int64 userId, string latitude, string longitude, Int64 userType, string startDate, string endDate, Int64 id, string note)
        {
            return ActivityRepository.UpdateActivity(userId, latitude, longitude, userType, startDate, endDate, id, note);
        }
        public Int64? DeleteActivity(Int64 activityId)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return ActivityRepository.DeleteActivity(activityId);
         }

        public List<SP_Select_Activitylist_Result> ActivityList()
        {
            return ActivityRepository.ActivityList();
        }
        public List<SP_Select_Activitylist_Result> GetUserActivityList(Int64 userid)
        {
            return ActivityRepository.ActivityList().Where(s=>s.userid==userid).ToList();
        }
        #region Service Methods
        public ApiResponse<ScalarResponse> AddActivityJson(Int64 userId, Int32 userType, string note, string latitude,string longitude,string startDate,string endDate,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();

            try
            {
                startDate = DateTime.ParseExact(startDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                endDate = DateTime.ParseExact(endDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");

                Int64? result = AddActivity(userId, userType, note, latitude, longitude, startDate, endDate);
                
                if (result >= 1)
                {
                    try
                    {
                        SendNotificationToUsers(result,userId, userType, latitude, longitude, startDate, endDate, note);
                    }
                    catch (Exception ex)
                    {
                        using (errorlogBusiness errorlog = new errorlogBusiness())
                        {
                            errorlog.InsertErrorLog(0, "notification", ex.Message, "");
                        }
                    }
                    objresponse.result = result.Value;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activityaddedsuccessfully"), "1");
                }
                else if (result == null)
                {
                    objresponse.result = -2;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ActivitynotaddedTryagain"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponseList<ActivityResponse> MyActivitiesJson(Int64 userId, string dateFrom, string dateTo,string DeviceLang)
        {
            List<ActivityResponse> objresponse = new List<ActivityResponse>();
            ApiResponseList<ActivityResponse> objapiresponse = new ApiResponseList<ActivityResponse>();

            try
            {
                if (dateFrom != null && dateFrom != "")
                {
                    dateFrom = DateTime.ParseExact(dateFrom, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                }
                if (dateTo != null && dateTo != "")
                {
                    dateTo = DateTime.ParseExact(dateTo, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                }


                objresponse = MyActivities(userId, dateFrom, dateTo).ToList();
                
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Myactivitylistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activitiesnotavailable"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponse<ActivityResponse> EditActivityJson(Int64 activityid,string DeviceLang)
        {
            ActivityResponse objresponse = new ActivityResponse();
            ApiResponse<ActivityResponse> objapiresponse = new ApiResponse<ActivityResponse>();
            try
            {
                objresponse = EditActivity(activityid);
                
                if (objresponse != null)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activitydetailsget"), "1");
                }
                else
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }

            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponse<ScalarResponse> UpdateActivityJson(Int64 userId, string latitude, string longitude, Int64 userType, string startDate, string endDate, Int64 id, string note,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                startDate = DateTime.ParseExact(startDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                endDate = DateTime.ParseExact(endDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                Int64? result = UpdateActivity(userId, latitude, longitude, userType, startDate, endDate, id, note);
                
                if (result >= 1)
                {
                    objresponse.result = result.Value;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activityupdatedsuccessfully"), "1");
                }
                else if (result == -2)
                {
                    objresponse.result = -2;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Userdoesnotexits"), "0");
                }
                else if (result == -3)
                {
                    objresponse.result = -3;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activitydoesnotexits"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponse<ScalarResponse> DeleteActivityJson(Int64 activityId,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                Int64? result = DeleteActivity(activityId);
                
                if (result == 1)
                {
                    objresponse.result = result.Value;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activitydeletedsuccessfully"), "1");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponseList<ActivityResponse1> ActivityListJson(Int64 userId, Int64 userType, string latitude, string longitude, string startDate, string endDate, int radius,string DeviceLang)
        {
            List<ActivityResponse1> objresponse = new List<ActivityResponse1>();
            ApiResponseList<ActivityResponse1> objapiresponse = new ApiResponseList<ActivityResponse1>();
            try
            {
                if (startDate != null && startDate != "")
                {
                    startDate = DateTime.ParseExact(startDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                }
                if (endDate != null && endDate != "")
                {
                    endDate = DateTime.ParseExact(endDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                }
                objresponse = ListActivities(userId, userType, latitude, longitude, startDate, endDate, radius);
                
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Myactivitylistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activitiesnotavailable"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public ApiResponseList<MyActivitiesAroundResponse> MyActivitiesAroundJson(Int64 userId, Int64 activityId, int radius,string DeviceLang)
        {
            List<MyActivitiesAroundResponse> objresponse = new List<MyActivitiesAroundResponse>();
            ApiResponseList<MyActivitiesAroundResponse> objapiresponse = new ApiResponseList<MyActivitiesAroundResponse>();
            try
            {
                objresponse = MyActivitiesAround(userId, activityId, radius);
                
                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Myactivitylistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activitiesnotavailable"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public List<ActivityResponse1> ListActivitiesFiltered(Int64 userId, Int64 userType, string latitude, string longitude, string startDate, string endDate, int radius, string activityids)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            List<sp_ListActivities_Result> activities = ActivityRepository.ActivityList(userId, userType, latitude, longitude, startDate, endDate, radius);
            if (!string.IsNullOrEmpty(activityids))
            {
                IEnumerable<Int64> ids = activityids.Split(',').Select(str => Convert.ToInt64(str));
                activities = activities.Where(rec => !ids.Contains(rec.id)).ToList();
            }
            List<ActivityResponse1> lstactivity = new List<ActivityResponse1>();
            foreach (var item in activities)
            {
                ActivityResponse1 objactivity = new ActivityResponse1();
                objactivity.id = item.id;
                objactivity.userId = item.userid;
                objactivity.userType = item.usertype;
                objactivity.latitude = item.latitude;
                objactivity.longitude = item.longitude;
                objactivity.note = item.note;
                objactivity.startDate = item.startdate.ToString("M/d/yyyy h:mm:ss tt");
                objactivity.endDate = item.enddate.ToString("M/d/yyyy h:mm:ss tt");
                objactivity.distance = Convert.ToDecimal(item.distance).ToString("0.00");
                objactivity.userName = item.username;
                lstactivity.Add(objactivity);
            }
            return lstactivity;
        }
        public ApiResponseList<ActivityResponse1> ActivityListFilteredJson(Int64 userId, Int64 userType, string latitude, string longitude, string startDate, string endDate, string DeviceLang, string activityids)
        {
            List<ActivityResponse1> objresponse = new List<ActivityResponse1>();
            ApiResponseList<ActivityResponse1> objapiresponse = new ApiResponseList<ActivityResponse1>();
            try
            {
                if (startDate != null && startDate != "")
                {
                    startDate = DateTime.ParseExact(startDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                }
                if (endDate != null && endDate != "")
                {
                    endDate = DateTime.ParseExact(endDate, "MM-dd-yyyy", null).ToString("dd-MMM-yyyy");
                }
                objresponse = ListActivitiesFiltered(userId, userType, latitude, longitude, startDate, endDate, 5, activityids);

                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Myactivitylistget"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Activitiesnotavailable"), "1");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        
        #endregion
    }
}
