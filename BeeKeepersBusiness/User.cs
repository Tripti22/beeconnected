﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BeeKeepersDataBase;
using System.Net.Mail;
namespace BeeKeepersBusiness
{
    
    public class User : BeeKeepersDataBase.tbluser
    {
        
        public List<sp_users_Result> UsersList_Admin(int type, string searchkwd)
        {
            return UserRepository.UsersList(type, searchkwd);
        }
        //public Int64? DeleteUser(Int64 userid)
        //{
        //    return UserRepository.DeleteUser(userid);
        //}

        public int SignUp(string DeviceLang, int tokenId)
        {
            BeeKeepersDataBase.tbluser objUser = new BeeKeepersDataBase.tbluser()
            {
                username = this.username,
                password = this.password,
                email = this.email,
                usertype = this.usertype,
                isactive = true,
                isdeleted = false,
                addedon = DateTime.Now,
                addedby = this.addedby,
                nickname = this.nickname,
                firstname=this.firstname,
                lastname=this.lastname
            };
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                if (DB.tblusers.Where(L => L.username == username).Count() == 0)
                {
                    if (DB.tblusers.Where(L => L.email == email).Count() == 0)
                    {
                        //if (DB.tblusers.Where(L => L.nickname == nickname).Count() == 0)
                        //{
                        DB.tblusers.AddObject(objUser);
                        DB.SaveChanges();
                        BeeKeepersDataBase.tblLanguage objLanguage = new BeeKeepersDataBase.tblLanguage()
                        {
                            language = DeviceLang.ToLower(),
                            sorcetype = 2,
                            userid = objUser.id,
                            tokenid = tokenId,
                            lastupdateon=DateTime.UtcNow
                        };
                        DB.tblLanguages.AddObject(objLanguage);
                        BeeKeepersDataBase.tblLanguage objLanguage1 = new BeeKeepersDataBase.tblLanguage()
                        {
                            language = DeviceLang.ToLower(),
                            sorcetype = 1,
                            userid = objUser.id,
                            tokenid = tokenId,
                            lastupdateon = DateTime.UtcNow
                        };
                        DB.tblLanguages.AddObject(objLanguage1);
                        DB.SaveChanges();
                        string path = HttpContext.Current.Server.MapPath("..//");
                        Email.SendResistrationMail(this.email, this.firstname, this.lastname, this.usertype.ToString(), this.username, this.password, path.Substring(0, path.Length - 1), DeviceLang);
                        return Convert.ToInt32(objUser.id);
                        //}
                        //throw new Exception("Nickname already exist ");
                    }
                    else
                    {
                        return -2;
                    }

                }
                else
                {
                    return -3;
                }
                
            }
            throw new Exception(new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"));
        }

        public DataResponse IsLoginValid(LoginRequest data)
        {
            DataResponse obj = new DataResponse();
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                if (DB.tblusers.Where(L => L.username == data.userName).Count() == 0)
                    throw new Exception(new SiteLanguages().GetResxNameByValue(data.Lang, "Usernamedoesnotexist"));
                else if (DB.tblusers.Where(L => L.username == data.userName && L.password == data.password).Count() == 0)
                    throw new Exception(new SiteLanguages().GetResxNameByValue(data.Lang, "Passworddoesnotmatch"));
                else
                {
                    var result = (from L in DB.tblusers
                                  where L.username == data.userName && L.password == data.password
                                  select L).Take(1).SingleOrDefault();
                    if (result != null)
                    {
                       if(result.isactive == true)
                        {
                            obj.isactive = true;
                            
                        } 
                       
                        var language = DB.tblLanguages.Where(s => s.userid == result.id && s.sorcetype==2).FirstOrDefault();
                        if (language != null)
                        {
                            language.language = data.Lang.ToLower();
                            language.tokenid = data.tokenId;
                            language.lastupdateon = DateTime.UtcNow;
                            DB.tblLanguages.ApplyCurrentValues(language);
                            DB.SaveChanges();
                        }
                        obj.id = Convert.ToInt32(result.id);
                        obj.userType = Convert.ToInt32(result.usertype);
                        return obj;
                    }
                    else
                        throw new Exception(new SiteLanguages().GetResxNameByValue(data.Lang, "Success"));
                }
            }
        }

        public Int64 ForgotPassword(string email,string lang)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var user=DB.tblusers.Where(s => s.email == email).FirstOrDefault();
                if (user != null)
                {
                    int result = Email.SendForgotPasswordMail(user.firstname+" "+user.lastname, email,user.username, user.password, HttpContext.Current.Request.PhysicalApplicationPath,lang);
                    return result;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Int64 ChangePassword(Int64 userid, string oldpassword, string newpassword)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var user = DB.tblusers.Where(s => s.id == userid && s.password==oldpassword).FirstOrDefault();
                if (user != null)
                {
                    user.password = newpassword;
                    DB.tblusers.ApplyCurrentValues(user);
                    DB.SaveChanges();
                    return 1;
                }
                else
                {
                    return -2;
                }
            }
        }

        public Int64 DeleteUser(string email,string UserName)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var user = DB.tblusers.Where(s => s.email == email && s.username == UserName).FirstOrDefault();
                if (user !=null)
                {
                    user.isactive = false;
                    DB.tblusers.ApplyCurrentValues(user);
                    DB.SaveChanges();
                    return 1;
                }
                else
                {
                    return -2;
                }
            }
        }
        public EditProfile EditProfile(Int64 userid)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                EditProfile profile = new EditProfile();
                var user = DB.tblusers.Where(s => s.id == userid).FirstOrDefault();
                if (user != null)
                {
                    profile.email = user.email;
                    profile.nickName = user.nickname;
                    profile.userName = user.username;
                    profile.userType = user.usertype;
                    profile.firstname = user.firstname;
                    profile.lastname = user.lastname;
                    profile.UpdatedOn = user.updatedon.ToString();
                    return profile;
                }
                else
                {
                    return null;
                }
            }
        }

        public Int64 UpdateProfile(Int64 userid, string username, string nickname, string email, int usertype,string firstname,string lastname)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var user=DB.tblusers.Where(s => s.id == userid).FirstOrDefault();
                if (user != null)
                {
                    if (DB.tblusers.Where(L => L.username == username && L.id!=userid).Count() == 0)
                    {
                        if (DB.tblusers.Where(L => L.email == email && L.id != userid).Count() == 0)
                        {
                            //if (DB.tblusers.Where(L => L.nickname == nickname && L.id != userid).Count() == 0)
                            //{
                                user.usertype = usertype;
                                user.email = email;
                                user.nickname = nickname;
                                user.username = username;
                                user.firstname = firstname;
                                user.lastname = lastname;
                                user.updatedon = DateTime.Now;
                                DB.tblusers.ApplyCurrentValues(user);
                                DB.SaveChanges();
                                return 1;
                            //}
                            //else
                            //{
                            //    return -2;
                            //}
                        }
                        else
                        {
                            return -3;
                        }
                    }
                    else
                    {
                        return -4;
                    }
                }
                else
                {
                    return 0;
                }
            }
        }
        
        public Int64 Create(string username, string emailid)
        {
            tbluser tbluser = new tbluser
            {
                username = this.username,
                password = this.password,
                email = this.email,
                usertype = this.usertype,
                isactive = true,
                addedon = DateTime.Now,
                addedby = this.addedby,
                updatedby = this.updatedby,
                nickname = this.nickname,
                firstname = this.firstname,
                lastname = this.lastname,

            };

            using (BeeKeepersEntities Entities = new BeeKeepersEntities())
            {

                int usernamecount = Entities.tblusers.Where(X => X.username == username).Count();
                int emailcount = Entities.tblusers.Where(X => X.email == emailid).Count();

                if (usernamecount > 0)
                    return -2;
                else if (emailcount > 0)
                    return -3;
                else
                {
                    //Entities.tblusers.AddObject(tbluser);
                    //Entities.SaveChanges();
                    UserRepository.Add(tbluser);
                    return tbluser.id;
                }

            }
        }
        public tbluser DoLogin(string username, string password)
        {
            return UserRepository.DoLogin(username, password);
        }

        //public int  sendForgotPasswordmail(string emailid)
        //{
        //    try
        //    {
        //        tbluser result = UserRepository.GetPasswordByEmailId(emailid);
        //        if (result != null)
        //        {
        //            MailMessage mail = new MailMessage();
        //            mail.To.Add(emailid);
        //            mail.Subject = Resource.Yourlogincredentialsis+" : <br />";
        //            mail.IsBodyHtml = true;
        //            mail.Body = Resource.UserName+" : " + result.username + "<br />"+Resource.Password+" : " + result.password;
        //            mail.From = new MailAddress("donotreply@beeconnected.ca");
        //            Email.Send(mail);
                    
        //            return 1;
        //        }
        //        else
        //            return 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        return 2;
        //    }
           
          
        //}

        public int SendConatctusmail(string name,string emailid,string contactno,string query)
        {
            try
            {
                
                    MailMessage mail = new MailMessage();
                    mail.To.Add("info@beeconnected.ca");
                    mail.Subject = Resource.Contactusqueries;
                    mail.IsBodyHtml = true;
                    mail.Body = Resource.Name+" : " + name + "<br />"+Resource.Email+" : " + emailid + "<br />"+Resource.ContactNumber+" : " + contactno + "<br />"+Resource.Message+" : " + query;
                    mail.From = new MailAddress(emailid);
                    Email.Send(mail);

                    return 1;
                
                
            }
            catch (Exception ex)
            {
                return 2;
            }


        }

        #region ServiceMethods
        public ApiResponse<ScalarResponse> ForgotPasswordJson(ForgotPassword data)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                string strmsg = string.Empty;
                Int64 result = ForgotPassword(data.email, data.Lang);
                if (result == 1)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "YourpasswordhassenttoyourEmaild"), "1");
                }
                else if (result == 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThisEmailIdDoesNotExits"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            
            return objapiresponse;
        }
        //Int64 userid, string oldpassword, string newpassword,string DeviceLang
        public ApiResponse<ScalarResponse> ChangePasswordJson(ChangePassword data)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                string strmsg = string.Empty;
                //EncryptionDecryption hashing = new EncryptionDecryption();
                //string oldPassword = hashing.encryption(data.oldPassword);
                //string newPassword = hashing.encryption(data.newPassword);
                Int64 result = ChangePassword(data.userId, data.oldPassword, data.newPassword);
                if (result == 1)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "Passwordchangedsuccessfully"), "1");
                }
                else if (result == -2)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "Oldpassworddoesnotmatched"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            
            return objapiresponse;
        }
        public ApiResponse<EditProfile> EditProfileJson(ScalarRequest data)
        {
            EditProfile objresponse = new EditProfile();
            ApiResponse<EditProfile> objapiresponse = new ApiResponse<EditProfile>();

            try
            {
                string strmsg = string.Empty;
                objresponse = EditProfile(data.requestValue);
                if (objresponse != null)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "Profiledetailsget"), "1");
                }
                else if (objresponse == null)
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "Profiledetailsnotreceived"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            
            return objapiresponse;
        }
        public ApiResponse<ScalarResponse> UpdateProfileJson(Int64 userid,string username,string nickname,string email,int usertype,string firstname,string lastname,string DeviceLang)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                string strmsg = string.Empty;
                Int64 result = UpdateProfile(userid, username, nickname, email, usertype, firstname, lastname);
                if (result == 1)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Profileupdatedsuccessfully"), "1");
                }
                else if (result == -2)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "NickNamealreadyexist"), "0");
                }
                else if (result == -3)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Emailalreadyexist"), "0");
                }
                else if (result == -4)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Usernamealreadyexist"), "0");
                }
                else if (result == 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ProfileNotUpdated"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public List<GetUserResponse> GetUsers(string userIds)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            var Users = UserRepository.GetUsers(userIds);
            List<GetUserResponse> lstusers = new List<GetUserResponse>();
            foreach (var item in Users)
            {
                GetUserResponse objuser = new GetUserResponse();
                objuser.UserId = item.id;
                objuser.UserName = item.username;
                objuser.UserType = item.usertype;
                lstusers.Add(objuser);
            }
            return lstusers;
        }
        public ApiResponseList<GetUserResponse> GetUsersJson(string userIds, string DeviceLang)
        {
            List<GetUserResponse> objresponse = new List<GetUserResponse>();
            ApiResponseList<GetUserResponse> objapiresponse = new ApiResponseList<GetUserResponse>();
            try
            {
                objresponse = GetUsers(userIds);

                if (objresponse != null && objresponse.Count > 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "UserslistGet"), "1");
                }
                else if (objresponse != null && objresponse.Count == 0)
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "Userdoesnotexits"), "0");
                }
                else
                {
                    objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {

                objapiresponse.AddResponsePacketList(objresponse, new SiteLanguages().GetResxNameByValue(DeviceLang, "ThereisaproblemPleasetryagainlater"), "0");
            }
            return objapiresponse;
        }
        public Int64 ForgotUsername(string email,string lang)
        {
            using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
            {
                var user = DB.tblusers.Where(s => s.email == email).FirstOrDefault();
                if (user != null)
                {
                    int result = Email.SendForgotUsernameMail(user.firstname + " " + user.lastname, email, user.username, user.password, HttpContext.Current.Request.PhysicalApplicationPath, lang);
                    return result;
                }
                else
                {
                    return 0;
                }
            }
        }
        public ApiResponse<ScalarResponse> ForgotUsernameJson(ForgotPassword data)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                string strmsg = string.Empty;
                Int64 result = ForgotUsername(data.email,data.Lang);
                if (result == 1)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "YourusernamehassenttoyourEmaild"), "1");
                }
                else if (result == 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThisEmailIdDoesNotExits"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
            }

            return objapiresponse;
        }

        public ApiResponse<ScalarResponse> DeleteUserJson(DeleteUser data)
        {
            ScalarResponse objresponse = new ScalarResponse();
            ApiResponse<ScalarResponse> objapiresponse = new ApiResponse<ScalarResponse>();
            try
            {
                string strmsg = string.Empty;
                Int64 result = DeleteUser(data.Email, data.UserName);
                if (result == 1)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "UserDeactivatedSuccessfully"),"1");
                }
                else if (result == 0)
                {
                    objresponse.result = result;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThisEmailIdDoesNotExits"), "0");
                }
                else
                {
                    objresponse.result = -1;
                    objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
                }
            }
            catch (Exception ex)
            {
                objresponse.result = -1;
                objapiresponse.AddResponsePacket(objresponse, new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"), "0");
            }

            return objapiresponse;
        }
        #endregion
    }
}
 