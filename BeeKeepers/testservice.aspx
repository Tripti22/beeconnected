﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testservice.aspx.cs" Inherits="BeeKeepers.testservice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 206px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <hr />
            <table>
                <tr>
                    <td>&nbsp;Method Name :
                        
                    <asp:DropDownList  ID="txtMethodName" runat="server" Width="374px" Height="23px">
                        <asp:ListItem Text="SignUp" Value="SignUp"></asp:ListItem>
                        <asp:ListItem Text="Login" Value="Login"></asp:ListItem>
                        <asp:ListItem Text="ForgotPassword" Value="ForgotPassword"></asp:ListItem>
                        <asp:ListItem Text="ForgotUsername" Value="ForgotUsername"></asp:ListItem>
                        <asp:ListItem Text="ChangePassword" Value="ChangePassword"></asp:ListItem>
                        <asp:ListItem Text="EditProfile" Value="EditProfile"></asp:ListItem>
                        <asp:ListItem Text="UpdateProfile" Value="UpdateProfile"></asp:ListItem>
                        <asp:ListItem Text="AddProperty" Value="AddProperty"></asp:ListItem>
                        <asp:ListItem Text="PropertyList" Value="PropertyList"></asp:ListItem>
                        <asp:ListItem Text="EditProperty" Value="EditProperty"></asp:ListItem>
                        <asp:ListItem Text="UpdateProperty" Value="UpdateProperty"></asp:ListItem>
                        <asp:ListItem Text="DeleteProperty" Value="DeleteProperty"></asp:ListItem>
                        <asp:ListItem Text="AddActivity" Value="AddActivity"></asp:ListItem>
                        <asp:ListItem Text="MyActivities" Value="MyActivities"></asp:ListItem>
                        <asp:ListItem Text="EditActivity" Value="EditActivity"></asp:ListItem>
                        <asp:ListItem Text="UpdateActivity" Value="UpdateActivity"></asp:ListItem>
                        <asp:ListItem Text="DeleteActivity" Value="DeleteActivity"></asp:ListItem>
                        <asp:ListItem Text="GetActivities" Value="GetActivities"></asp:ListItem>
                        <asp:ListItem Text="MyActivitiesAround" Value="MyActivitiesAround"></asp:ListItem>
                        <asp:ListItem Text="SendMessage" Value="SendMessage"></asp:ListItem>
                        <asp:ListItem Text="GetMessages" Value="GetMessages"></asp:ListItem>
                        <asp:ListItem Text="getUsersListForInbox" Value="getUsersListForInbox"></asp:ListItem>
                        <asp:ListItem Text="Installation" Value="Installation"></asp:ListItem>
                        <asp:ListItem Text="SetUserInstallation" Value="SetUserInstallation"></asp:ListItem>
                        <asp:ListItem Text="DeleteInstallation" Value="DeleteInstallation"></asp:ListItem>
                        <asp:ListItem Text="SendBroadCastMessage" Value="SendBroadCastMessage"></asp:ListItem>
                        <asp:ListItem Text="NearbyProperty" Value="NearbyProperty"></asp:ListItem>
                        <asp:ListItem Text="GetMessagesNew" Value="GetMessagesNew"></asp:ListItem>
                        <asp:ListItem Text="GetAllSynchData" Value="GetAllSynchData"></asp:ListItem>
                        <asp:ListItem Text="AddLocalData" Value="AddLocalData"></asp:ListItem>
                        <asp:ListItem Text="UpdateLanguageSetting" Value="UpdateLanguageSetting"></asp:ListItem>
                        <asp:ListItem Text="ResetUnreadNotifications" Value="ResetUnreadNotifications"></asp:ListItem>
                        <asp:ListItem Text="UnreadMsgCounter" Value="UnreadMsgCounter"></asp:ListItem>
                        <asp:ListItem Text="GetMessagesById" Value="GetMessagesById"></asp:ListItem>
                        <asp:ListItem Text="GetActivitiesFiltered" Value="GetActivitiesFiltered"></asp:ListItem>
                        <asp:ListItem Text="NearbyPropertyFiltered" Value="NearbyPropertyFiltered"></asp:ListItem>
                        <asp:ListItem Text="GetUsers" Value="GetUsers"></asp:ListItem>
                    </asp:DropDownList>
                    </td>
                    <td class="style1">
                        <asp:RadioButtonList ID="rbtlist" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="XML" Value="xml"></asp:ListItem>
                            <asp:ListItem Text="JSON" Value="json" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="InvokeService" UseSubmitBehavior="true" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>
        <div style="vertical-align: top;">
            <asp:TextBox ID="txtPostData" runat="server" Columns="100" Rows="30" TextMode="MultiLine"></asp:TextBox>
        </div>
    </form>
</body></html>

