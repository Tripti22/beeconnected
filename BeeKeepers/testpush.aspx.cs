﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BeeKeepersBusiness.utility;
using BeeKeepersBusiness;
using System.Resources;
using System.Reflection;

namespace BeeKeepers
{
    public partial class testpush : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (rad1.Checked)
                    AndroidPushNotice.SendNotice(txtid.Text, new SiteLanguages().GetResxNameByValue("en", "Aboutus"), 1, "Message", 15, 0, "");
                else
                    iPhonePush.SendNotice(txtid.Text, new SiteLanguages().GetResxNameByValue("en", "Aboutus"), 1, "Message", 0, 0, "");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }

        }
    }
}