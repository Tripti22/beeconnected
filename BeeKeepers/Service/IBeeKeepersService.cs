﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BeeKeepersBusiness;


namespace BeeKeepers
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBeeKeepersService" in both code and config file together.
    [ServiceContract]
    public interface IBeeKeepersService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SignUp")]
        CommonResponse SignUp(SignUpRequest obj);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Login")]
        CommonResponse Login(LoginRequest obj);

        [OperationContract(Name = "ForgotPassword")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "ForgotPassword")]
        System.ServiceModel.Channels.Message ForgotPassword(ForgotPassword data);
        // CommonResponse ForgotPassword(ForgotPassword data);

        [OperationContract(Name = "ForgotUsername")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "ForgotUsername")]
        System.ServiceModel.Channels.Message ForgotUsername(ForgotPassword data);

        [OperationContract(Name = "ChangePassword")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "ChangePassword")]
        System.ServiceModel.Channels.Message ChangePassword(ChangePassword data);


        [OperationContract(Name = "EditProfile")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "EditProfile")]
        System.ServiceModel.Channels.Message EditProfile(ScalarRequest data);

        [OperationContract(Name = "UpdateProfile")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateProfile")]
        System.ServiceModel.Channels.Message UpdateProfile(UpdateProfile data);

        [OperationContract(Name = "AddProperty")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddProperty")]
        System.ServiceModel.Channels.Message AddProperty(AddProperty data);

        [OperationContract(Name = "PropertyList")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PropertyList")]
        System.ServiceModel.Channels.Message PropertyList(ScalarRequest data);

        [OperationContract(Name = "EditProperty")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "EditProperty")]
        System.ServiceModel.Channels.Message EditProperty(ScalarRequest data);

        [OperationContract(Name = "UpdateProperty")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateProperty")]
        System.ServiceModel.Channels.Message UpdateProperty(PropertyResponse data);

        [OperationContract(Name = "DeleteProperty")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteProperty")]
        System.ServiceModel.Channels.Message DeleteProperty(ScalarRequest data);


        [OperationContract(Name = "AddActivity")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddActivity")]
        System.ServiceModel.Channels.Message AddActivity(ActivityRequest data);

        [OperationContract(Name = "MyActivities")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "MyActivities")]
        System.ServiceModel.Channels.Message MyActivities(MyActivityRequest data);

        [OperationContract(Name = "EditActivity")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "EditActivity")]
        System.ServiceModel.Channels.Message EditActivity(ScalarRequest data);

        [OperationContract(Name = "UpdateActivity")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateActivity")]
        System.ServiceModel.Channels.Message UpdateActivity(ActivityResponse data);

        [OperationContract(Name = "DeleteActivity")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteActivity")]
        System.ServiceModel.Channels.Message DeleteActivity(ScalarRequest data);

        [OperationContract(Name = "GetActivities")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetActivities")]
        System.ServiceModel.Channels.Message GetActivities(ActivityListRequest data);

        [OperationContract(Name = "MyActivitiesAround")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "MyActivitiesAround")]
        System.ServiceModel.Channels.Message MyActivitiesAround(MyActivitiesAroundRequest data);


        [OperationContract(Name = "SendMessage")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendMessage")]
        System.ServiceModel.Channels.Message SendMessage(MessageSendRequest data);

        [OperationContract(Name = "SendBroadCastMessage")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendBroadCastMessage")]
        System.ServiceModel.Channels.Message SendBroadCastMessage(BroadcastMessageRequest data);

        [OperationContract(Name = "GetMessages")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetMessages")]
        System.ServiceModel.Channels.Message GetMessages(MessageGetRequest data);

        [OperationContract(Name = "GetMessagesById")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetMessagesById")]
        System.ServiceModel.Channels.Message GetMessagesById(ScalarRequest data);

        [OperationContract(Name = "GetMessagesNew")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetMessagesNew")]
        System.ServiceModel.Channels.Message GetMessagesNew(MessageGetRequestNew data);


        [OperationContract(Name = "getUsersListForInbox")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getUsersListForInbox")]
        System.ServiceModel.Channels.Message getUsersListForInbox(ScalarRequest data);

        [OperationContract(Name = "Installation")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "Installation")]
        System.ServiceModel.Channels.Message Installation(InstallationRequest data);

        [OperationContract(Name = "SetUserInstallation")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetUserInstallation")]
        System.ServiceModel.Channels.Message SetUserInstallation(SetUserInstallationRequest data);

        [OperationContract(Name = "DeleteInstallation")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteInstallation")]
        System.ServiceModel.Channels.Message DeleteInstallation(ScalarRequest data);

        [OperationContract(Name = "NearbyProperty")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "NearbyProperty")]
        System.ServiceModel.Channels.Message NearbyProperty(NearbyPropertyRequest data);

        [OperationContract(Name = "Get All Data Synch")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllSynchData")]
        System.ServiceModel.Channels.Message GetAllSynchData(GetSynchDataRequest data);

        [OperationContract(Name = "Add local data of device")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "AddLocalData")]
        System.ServiceModel.Channels.Message AddLocalData(LocalData data);

        [OperationContract(Name = "Update Language Setting")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateLanguageSetting")]
        System.ServiceModel.Channels.Message UpdateLanguageSetting(LanguageRequest data);

        [OperationContract(Name = "Reset Unread Notifications")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "ResetUnreadNotifications")]
        System.ServiceModel.Channels.Message ResetUnreadNotifications(ScalarRequest data);

        [OperationContract(Name = "Unread Message Counter")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UnreadMsgCounter")]
        System.ServiceModel.Channels.Message UnreadMsgCounter(ScalarRequest data);

        [OperationContract(Name = "GetActivitiesFiltered")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetActivitiesFiltered")]
        System.ServiceModel.Channels.Message GetActivitiesFiltered(ActivityListFilteredRequest data);

        [OperationContract(Name = "NearbyPropertyFiltered")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "NearbyPropertyFiltered")]
        System.ServiceModel.Channels.Message NearbyPropertyFiltered(NearbyPropertyFilteredRequest data);

        [OperationContract(Name = "Get Users")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUsers")]
        System.ServiceModel.Channels.Message GetUsers(GetUserRequest data);

        [OperationContract(Name = "DeleteUser")]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteUser")]
        System.ServiceModel.Channels.Message DeleteUser(DeleteUser data);
    }
   
}
