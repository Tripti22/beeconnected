﻿using System;
using System.ServiceModel.Activation;
using System.Text;
using BeeKeepersBusiness;
using BeeKeepers.Util;
using System.ServiceModel.Web;

namespace BeeKeepers
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BeeKeepersService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BeeKeepersService.svc or BeeKeepersService.svc.cs at the Solution Explorer and start debugging.
    public class BeeKeepersService : IBeeKeepersService
    {
             

        string ResponseType = "application/json; charset=utf-8";
        Encoding encoding = Encoding.UTF8;

        #region User Services
        public CommonResponse SignUp(SignUpRequest data)
        {
            CommonResponse objCommonResponse = new CommonResponse();
            EncryptionDecryption hashing = new EncryptionDecryption();
            User objUser = new User();
            try
            {

                objUser.username = data.userName;
                objUser.password = hashing.encryption(data.password);
                objUser.email = data.email;
                objUser.usertype = data.userType;
                objUser.firstname= data.firstname;
                objUser.lastname= data.lastname;
                // objUser.addedby = data.addedby;
                objUser.nickname = data.nickName;
                int Id = objUser.SignUp(data.Lang,data.tokenId);
                if (Id > 0)
                {
                    objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "Success"); 
                    objCommonResponse.status = 1;
                    objCommonResponse.data = new DataResponse
                    {
                        id = Id,
                        userType = data.userType
                    };
                    
                }
                else if(Id==-2)
                {
                    objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "Emailalreadyexist"); 
                    objCommonResponse.status = 0;
                }
                else if (Id == -3)
                {
                    objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "Usernamealreadyexist"); 
                    objCommonResponse.status = 0;
                }
                else
                {
                    objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "Userdoesnotregisterproperly");
                    objCommonResponse.status = 0;
                }
            }
            catch (Exception ex)
            {
                objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "ThereisaproblemPleasetryagainlater"); 
                objCommonResponse.status = 0;
            }
            return objCommonResponse;
        }
        public CommonResponse Login(LoginRequest data)
        {
            CommonResponse objCommonResponse = new CommonResponse();
            EncryptionDecryption hashing = new EncryptionDecryption();
            User objUser = new User();
            try
            {
                string password = hashing.encryption(data.password);
                data.password = password;
                DataResponse result = objUser.IsLoginValid(data);
                if(result.isactive == false)
                {
                    objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "InvalidloginUserdeactivated");
                    objCommonResponse.status = 0;
                }
               else if (result.id > 0)
                {
                    objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "Success"); 
                    objCommonResponse.status = 1;
                    objCommonResponse.data = new DataResponse
                    {
                        id = result.id,
                        userType = result.userType,
                        isactive = result.isactive
                        
                    };

                }
                else
                {
                    objCommonResponse.message = new SiteLanguages().GetResxNameByValue(data.Lang, "Invalidusernameorpassword");
                    objCommonResponse.status = 1;
                }
            }
            catch (Exception ex)
            {
                objCommonResponse.message = ex.Message;
                objCommonResponse.status = 0;
            }
            return objCommonResponse;
        }
        public System.ServiceModel.Channels.Message ForgotPassword(ForgotPassword data)
        {
            ApiResponse<ScalarResponse> retval = null;
            User user = new User();
            retval = user.ForgotPasswordJson(data);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message ForgotUsername(ForgotPassword data)
        {
            ApiResponse<ScalarResponse> retval = null;
            User user = new User();
            retval = user.ForgotUsernameJson(data);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message ChangePassword(ChangePassword data)
        {
            ApiResponse<ScalarResponse> retval = null;
            User user = new User();
           
            retval = user.ChangePasswordJson(data);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message EditProfile(ScalarRequest data)
        {
            ApiResponse<EditProfile> retval = null;
            User user = new User();
            retval = user.EditProfileJson(data);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message UpdateProfile(UpdateProfile data)
        {
            ApiResponse<ScalarResponse> retval = null;
            User user = new User();
            retval = user.UpdateProfileJson(data.userId, data.userName, data.nickName, data.email, data.userType, data.firstname, data.lastname, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        #endregion

        #region Add Property
        public System.ServiceModel.Channels.Message AddProperty(AddProperty data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Property property = new Property();
            //retval = property.AddPropertyJson(data.userId,data.latitude,data.longitude,data.addressLine1,data.addressLine2,data.city,data.state,data.zip,data.propertyName);
            retval = property.AddPropertyJson(data.userId, data.latitude, data.longitude, data.propertyName, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message PropertyList(ScalarRequest data)
        {
            ApiResponseList<PropertyResponse> retval = null;
            Property property = new Property();
            retval = property.PropertyListJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message EditProperty(ScalarRequest data)
        {
            ApiResponse<PropertyResponse> retval = null;
            Property property = new Property();
            retval = property.EditPropertyJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message UpdateProperty(PropertyResponse data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Property property = new Property();
            //retval = property.UpdatePropertyJson(data.addressLine1,data.addressLine2,data.latitude,data.longitude,data.propertyId,data.propertyName,data.state,data.userId,data.zip,data.city);
            retval = property.UpdatePropertyJson(data.latitude, data.longitude, data.propertyId, data.propertyName, data.userId, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message DeleteProperty(ScalarRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Property property = new Property();
            //retval = property.UpdatePropertyJson(data.addressLine1,data.addressLine2,data.latitude,data.longitude,data.propertyId,data.propertyName,data.state,data.userId,data.zip,data.city);
            retval = property.DeletePropertyJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        #endregion

        #region Activity
        public System.ServiceModel.Channels.Message AddActivity(ActivityRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Activities activity = new Activities();
            retval = activity.AddActivityJson(data.userId, data.userType, data.note, data.latitude, data.longitude, data.startDate, data.endDate, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message MyActivities(MyActivityRequest data)
        {
            ApiResponseList<ActivityResponse> retval = null;
            Activities activity = new Activities();
            retval = activity.MyActivitiesJson(data.userId, data.dateFrom, data.dateTo, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message EditActivity(ScalarRequest data)
        {
            ApiResponse<ActivityResponse> retval = null;
            Activities activity = new Activities();
            retval = activity.EditActivityJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message UpdateActivity(ActivityResponse data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Activities activity = new Activities();
            retval = activity.UpdateActivityJson(data.userId, data.latitude, data.longitude, data.userType, data.startDate, data.endDate, data.id, data.note, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message DeleteActivity(ScalarRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Activities activity = new Activities();
            retval = activity.DeleteActivityJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message GetActivities(ActivityListRequest data)
        {
            ApiResponseList<ActivityResponse1> retval = null;
            Activities activity = new Activities();
            retval = activity.ActivityListJson(data.userId, data.userType, data.latitude, data.longitude, data.startDate, data.endDate, data.radius, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message MyActivitiesAround(MyActivitiesAroundRequest data)
        {
            ApiResponseList<MyActivitiesAroundResponse> retval = null;
            Activities activity = new Activities();
            retval = activity.MyActivitiesAroundJson(data.userId, data.activityId, data.radius, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        #endregion
        #region Messaging
        public System.ServiceModel.Channels.Message SendMessage(MessageSendRequest data)
        {
            ApiResponse<MessageResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.SendMessageJson(data.fromUserId, data.toUserId, data.message, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message SendBroadCastMessage(BroadcastMessageRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.SendBroadCastMessageJson(data.activityId, data.message, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message GetMessages(MessageGetRequest data)
        {
            ApiResponseList<GetMessagesResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.GetMessagesJson(data.userId, data.otherUserId, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message GetMessagesById(ScalarRequest data)
        {
            ApiResponse<GetMessagesResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.GetMessageByIdJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message GetMessagesNew(MessageGetRequestNew data)
        {
            ApiResponse<GetMessagesResponseNew> retval = null;
            Messages msg = new Messages();
            retval = msg.GetMessagesNewJson(data.userId, data.otherUserId, data.pageNumber, data.pageSize, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message getUsersListForInbox(ScalarRequest data)
        {
            ApiResponseList<GetInboxUserResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.getUsersListForInboxJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message Installation(InstallationRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.InstallationJson(data.DeviceId, data.deviceToken, data.deviceType, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message SetUserInstallation(SetUserInstallationRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.SetUserInstallationJson(data.userId, Convert.ToInt64(data.tokenId), data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message DeleteInstallation(ScalarRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Messages msg = new Messages();
            retval = msg.DeleteInstallationJson(data.requestValue);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }


        public System.ServiceModel.Channels.Message NearbyProperty(NearbyPropertyRequest data)
        {

            ApiResponseList<NearbyPropertyResponse> retval = null;
            Property property = new Property();
            retval = property.NearbyPropertyJson(data.distance, data.latitude, data.longitude, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message GetAllSynchData(GetSynchDataRequest data)
        {
            ApiResponse<SynchData> retval = null;
            SynchDataBusiness obj = new SynchDataBusiness();
            retval = obj.GetAllDataSynchJson(data.UserId, data.Latitude, data.Longitude, data.InstallationId, data.LastSynchDateTime, data.IsDistanceGT50, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message AddLocalData(LocalData data)
        {
            ApiResponse<ScalarResponse> retval = null;
            SynchDataBusiness obj = new SynchDataBusiness();
            retval = obj.AddLocalDataJson(data.LocalActivity, data.LocalProperty, data.LocalMessage, data.LocalDeleted, data.BroadCastMessages, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        #endregion
        public System.ServiceModel.Channels.Message UpdateLanguageSetting(LanguageRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Common common = new Common();
            retval = common.UpdateLanguageSettingJson(data.tokenId, data.Language);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }
        public System.ServiceModel.Channels.Message ResetUnreadNotifications(ScalarRequest data)
        {
            ApiResponse<ScalarResponse> retval = null;
            Common common = new Common();
            retval = common.ResetUnreadNotificationsJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message UnreadMsgCounter(ScalarRequest data)
        {
            ApiResponseList<UnreadCountResponse> retval = null;
            Messages common = new Messages();
            retval = common.UnreadMsgCounterJson(data.requestValue, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message GetActivitiesFiltered(ActivityListFilteredRequest data)
        {
            ApiResponseList<ActivityResponse1> retval = null;
            Activities activity = new Activities();
            retval = activity.ActivityListFilteredJson(data.userId, data.userType, data.latitude, data.longitude, data.startDate, data.endDate, data.Lang, data.ActivityIds);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message NearbyPropertyFiltered(NearbyPropertyFilteredRequest data)
        {

            ApiResponseList<NearbyPropertyResponse> retval = null;
            Property property = new Property();
            retval = property.NearbyPropertyFilteredJson(data.latitude, data.longitude, data.Lang, data.PropertyIds);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message GetUsers(GetUserRequest data)
        {
            ApiResponseList<GetUserResponse> retval = null;
            User common = new User();
            retval = common.GetUsersJson(data.UserIds, data.Lang);
            return WebOperationContext.Current.CreateJsonResponse(retval);
        }

        public System.ServiceModel.Channels.Message DeleteUser(DeleteUser data)
        {
            ApiResponse<ScalarResponse> retval = null;
            User user = new User();
            retval = user.DeleteUserJson(data);
            return WebOperationContext.Current.CreateJsonResponse(retval);

        }

    }
}
