﻿using BeeKeepersBusiness;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BeeKeepers
{
    public partial class testservice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            HttpWebRequest req = null;
            HttpWebResponse res = null;
            try
            { 
                string url = Common.BasePath()+ "service/BeeKeepersService.svc/" + txtMethodName.SelectedValue.ToString();
                //string url = "http://localhost:54828/Service/BeeKeepersService.svc/" + txtMethodName.SelectedValue.ToString();//http://localhost:1743/Website/AutomateServices.svc/
                //string url = "http://beeconnected.ca/Services/service/BeeKeepersService.svc/" + txtMethodName.SelectedValue.ToString();
                req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "POST";
                req.ContentType = "text/" + rbtlist.SelectedValue.ToString() + ";";
                req.Timeout = 10000000;
                StreamWriter writer = new StreamWriter(req.GetRequestStream());
                writer.WriteLine(txtPostData.Text);
                writer.Close();

                res = (HttpWebResponse)req.GetResponse();
                string result;
                using (StreamReader rdr = new StreamReader(res.GetResponseStream()))
                {
                    result = rdr.ReadToEnd();
                }

                Repeater r = new Repeater();
                foreach (RepeaterItem ri in r.Items)
                {
                    if (((CheckBox)ri.FindControl("chkSelect")).Checked)
                    { }
                }


                //return only the xml representing the response details (inner request)
                Response.Write(result);
                //Response.Write(soapResonseXMLDocument.InnerXml);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}