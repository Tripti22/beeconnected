﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeeKeepersDataBase;

namespace BeeKeepersDataBase
{
    public class UserRepository : tbluser
    {
        public static List<sp_GetUsers_Result> GetUsers(string userids)
        {
            var data = Data.Entities;
            var users = data.sp_GetUsers(userids).ToList();
            return users;
        }
        public static List<sp_users_Result> UsersList(int type, string searchkwd)
        {
            var data = Data.Entities;
            var users = data.sp_users().ToList();

            if (type != 0)
            {
                users = users.Where(s => s.usertype == type).ToList();
            }
            if (searchkwd != "")
            {
                users = users.Where(s => s.nickname.ToLower().Contains(searchkwd.ToLower())).ToList();
            }
            return users;
        }
        public static Int64? DeleteUser(Int64 userid)
        {
            var data = Data.Entities;
            return data.sp_deleteUser(userid).FirstOrDefault().result;
        }

        public static void Add(tbluser user)
        {
            var data = Data.Entities;
            data.AddTotblusers(user);
            data.SaveChanges();
        }

        public static tbluser DoLogin(string username, string password)
        {
            BeeKeepersEntities entity = new BeeKeepersEntities();
            var result = entity.tblusers.Where(s => s.username == username && s.password==password).FirstOrDefault();
            return result;
        }

        public static tbluser  GetPasswordByEmailId(string emailid)
        {
            
            
            using (BeeKeepersEntities entities = new BeeKeepersEntities())
            {
                return  entities.tblusers.Where(X => X.email == emailid).SingleOrDefault();
                
               
            }
            

        }

        //public static tbluser Find(string Email, string UserName)
        //{
        //    var data = Data.Entities;
        //    return (from e in data.tblusers
        //            where e.email == Email || e.username== UserName
        //            select e).FirstOrDefault();
        //}



    }
}
