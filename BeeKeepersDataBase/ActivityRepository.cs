﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersDataBase
{
    public class ActivityRepository
    {
        public static Int64? AddActivity(Int64 userId, Int32 userType, string note, string latitude, string longitude, string startDate, string endDate)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_AddActivity(userId, userType, note, latitude, longitude, startDate, endDate).FirstOrDefault().result;
        }
        public static List<sp_getInstallationsForActivityAdded_Result> GetUsersForNotification(Int64 userId, Int32 userType, string latitude, string longitude, string startDate, string endDate)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_getInstallationsForActivityAdded(userId, userType, latitude, longitude, startDate, endDate,5).ToList();
        }
        
        public static List<sp_MyActivities_Result> MyActivityList(Int64 userId, string dateFrom,string dateTo)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_MyActivities(userId, dateFrom,dateTo).ToList();
        }
        public static List<sp_Userprofileactivities_Result> UserProfileActivities(Int64 userId, string dateFrom, string dateTo, Int64 Actid, int radius, string latitude, string longitude)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_Userprofileactivities(userId, dateFrom, dateTo, Actid, radius, latitude, longitude).ToList();
        }

        public static List<sp_ListActivities_Result> ActivityList(Int64 userId, Int64 userType, string latitude, string longitude, string startDate, string endDate, int radius)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_ListActivities(userId,userType,latitude,longitude,radius,startDate,endDate).ToList();
        }
        public static List<sp_ListActivities4Web_Result> ActivityList4web(Int64 userId, Int64 userType, string latitude, string longitude, string startDate, string endDate, int radius)
        {
            try
            {
                BeeKeepersEntities entities = new BeeKeepersEntities();
                DateTime dt_start =DateTime.Now;
                DateTime dt_end = DateTime.Now;
                bool flag_dtstart = false;
                bool flag_dtend = false;
                if (startDate != "")
                {
                    flag_dtstart = true;
                    string[] startdatesplit = startDate.Split('/');
                    dt_start=new DateTime(int.Parse(startdatesplit[2]), int.Parse(startdatesplit[1]), int.Parse(startdatesplit[0]));
                }
                if (endDate != "")
                {
                    flag_dtend = true;
                    string[] enddatesplit = endDate.Split('/');
                    dt_end=new DateTime(int.Parse(enddatesplit[2]), int.Parse(enddatesplit[1]), int.Parse(enddatesplit[0]));
                }


                return entities.sp_ListActivities4Web(userId, userType, latitude, longitude, radius, dt_start, dt_end, flag_dtstart, flag_dtend).ToList();
            }
            catch (Exception ex)
            {
                //errorlog errorlog = new errorlog();
                //errorlog.source = "datetime issue";
                //errorlog.description = startDate;
                //errorlog.occuredcomponent = "";
                //errorlogRepository.Add(errorlog);
                return null;
            }
        }
        public static List<sp_MyActivitiesAround_Result> MyActivitiesAroundList(Int64 userId, Int64 activityId, int radius)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_MyActivitiesAround(userId,activityId,radius).ToList();
        }
        
        public static Int64? UpdateActivity(Int64 userId, string latitude, string longitude, Int64 userType, string startDate, string endDate, Int64 id, string note)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_UpdateActivity(userId,latitude,longitude,userType,startDate,endDate,id,note).FirstOrDefault().result;
        }
        public static Int64? DeleteActivity(Int64 activityId)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_DeleteActivity(activityId).FirstOrDefault().result;
        }

        public static List<SP_Select_Activitylist_Result> ActivityList()
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.SP_Select_Activitylist().ToList();
        }

        
        
    }
}
