﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace BeeKeepersDataBase
{
    public class Data
    {

        private const string ContextKey = "BeeKeepersEntities";

        public static BeeKeepersEntities Entities
        {
            get
            {
                //for Web request
                if (HttpContext.Current != null)
                {
                    var contextItem = HttpContext.Current.Items[ContextKey];
                    if (HttpContext.Current != null && contextItem == null)
                    {
                        HttpContext.Current.Items[ContextKey] = new BeeKeepersEntities();
                    }
                    return HttpContext.Current.Items[ContextKey] as BeeKeepersEntities;
                }
                else // for service request
                {
                    BeeKeepersEntities cabappentities = new BeeKeepersEntities();
                    return cabappentities;
                }

            }
            set
            {
                if (HttpContext.Current != null)
                    HttpContext.Current.Items[ContextKey] = value;
            }
        }

        public static void RemoveContext()
        {
            var contextItem = HttpContext.Current.Items[ContextKey];
            if (
                HttpContext.Current != null
                &&
                contextItem != null
               )
            {
                ((BeeKeepersEntities)contextItem).Dispose();
                HttpContext.Current.Items[ContextKey] = null;
            }

        }
    }
}
