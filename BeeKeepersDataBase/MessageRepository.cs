﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersDataBase
{
    public class MessageRepository
    {
        public static List<sp_getMessageCounter_Result> GetUnreadMSGCounter(long userid)
        {
            try
            {
                using (BeeKeepersEntities entities = new BeeKeepersEntities())
                {
                    return  entities.sp_getMessageCounter(userid).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;

            }

        }
        public static Int64? UpdateMessageStatus(Int64 MessageId, int msgtype)
        {
            try
            {
                using (BeeKeepersEntities entity = new BeeKeepersEntities())
                {
                    Int64? message = entity.sp_UpdateMessageStatus(msgtype, MessageId).FirstOrDefault().result;
                    return message;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        
        public static string GetUnreadAdminCounter(long userid)
        {
            try
            {
                using (BeeKeepersEntities entities = new BeeKeepersEntities())
                {
                    string counter = entities.sp_getAdminMessageCounter(userid).FirstOrDefault().unread_counter.ToString();
                    if (counter == "0")
                    {
                        counter = "";
                    }
                    return counter;
                }
            }
            catch (Exception ex)
            {
                return "";
                     
            }
            
        }
        public static Int64? SendMessage(Int64 fromUserId, Int64 toUserId, string message, string addedon, int status)
        {
            try
            {
                BeeKeepersEntities entities = new BeeKeepersEntities();
                tblmessage objmessage = new tblmessage();
                objmessage.fromuserid = fromUserId;
                objmessage.touserid = toUserId;
                objmessage.body = message;
                objmessage.status = status;
                if (addedon != "" || addedon != "0")
                {
                    if (!string.IsNullOrEmpty(addedon) && addedon != "0")
                    {
                        string[] arrdate = (addedon.Split(' ')[0]).Split('-');
                        string[] arrtime = (addedon.Split(' ')[1]).Split(':');
                        if (addedon.Split(' ')[addedon.Split(' ').Length - 1] == "PM")
                        {
                            objmessage.addedon = Convert.ToDateTime(new DateTime(int.Parse(arrdate[2]), int.Parse(arrdate[0]), int.Parse(arrdate[1]), int.Parse(arrtime[0]), int.Parse(arrtime[1]), int.Parse(arrtime[2])).ToString("dd-MMM-yyyy hh:mm:ss tt")); //Convert.ToDateTime(DateTime.ParseExact(addedon, "MM-dd-yyyy hh:mm:ss tt", null).ToString("dd-MMM-yyyy hh:mm:ss tt"));
                        }
                        else
                        {
                            objmessage.addedon = Convert.ToDateTime(new DateTime(int.Parse(arrdate[2]), int.Parse(arrdate[0]), int.Parse(arrdate[1]), int.Parse(arrtime[0]), int.Parse(arrtime[1]), int.Parse(arrtime[2])).ToString("dd-MMM-yyyy hh:mm:ss tt")); //Convert.ToDateTime(DateTime.ParseExact(addedon, "MM-dd-yyyy hh:mm:ss tt", null).ToString("dd-MMM-yyyy hh:mm:ss tt"));
                        }

                    }
                    else
                        objmessage.addedon = DateTime.Now;
                }
                else
                {
                    objmessage.addedon = DateTime.Now;
                }

                entities.AddTotblmessages(objmessage);
                entities.SaveChanges();
                return objmessage.id;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<sp_SendBroadCastMessage_Result> SendBroadCastMessageUserList(Int64 activityId, string message)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_SendBroadCastMessage(activityId, message).ToList();

        }
        public static List<sp_GetNewAdminMessages_Result> GetNewAdminMessages(Int64 lastmsgid, Int64 userid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_GetNewAdminMessages(lastmsgid, userid).ToList();

        }
        public static List<tbladminmessage> GetAdminMessagesWithoutstatusupdate(Int64 userid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.tbladminmessages.Where(s => s.userid == userid).OrderByDescending(s => s.addedon).ToList();
        }
        public static List<sp_GetNewMessages_Result> GetNewMessages(Int64 lastmsgid, Int64 otherUserId, Int64 userid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_GetNewMessages(lastmsgid, userid, otherUserId).ToList();

        }
        public static List<sp_getMessages_Result> GetMessages(Int64 userid, Int64 otherUserId)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_getMessages(userid, otherUserId).ToList();

        }
        public static sp_getMessageById_Result GetMessageById(Int64 MsgId)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_getMessageById(MsgId).FirstOrDefault();
        }
        public static List<sp_GetAdminMessages_Result> GetAdminMessages(Int64 userid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_GetAdminMessages(userid).ToList();

        }
        public static List<sp_getUsersListForInbox_Result> getUsersListForInbox(Int64 userid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_getUsersListForInbox(userid).ToList();

        }
        public static List<sp_getMessageCounter_Result> getUnreadMsgCounter(Int64 userid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_getMessageCounter(userid).ToList();

        }
        public static Int64 Installation(string deviceid, string devicetoken, int devicetype)
        {
            try
            {
                string strdevicetype = devicetype.ToString();
                BeeKeepersEntities entities = new BeeKeepersEntities();
                tblinstallation objinstallation = null;
                if (!string.IsNullOrEmpty(deviceid))
                {
                    objinstallation = entities.tblinstallations.Where(s => s.deviceid == deviceid).FirstOrDefault();
                    if (objinstallation != null)
                    {
                        objinstallation.devicetoken = devicetoken;
                        objinstallation.devicetype = devicetype.ToString();
                        objinstallation.addedon = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        entities.tblinstallations.ApplyCurrentValues(objinstallation);
                        entities.SaveChanges();
                        return objinstallation.id;
                    }
                    else
                    {
                        objinstallation = entities.tblinstallations.Where(s => s.devicetoken == devicetoken && s.devicetype == strdevicetype).FirstOrDefault();
                        if (objinstallation == null)
                        {
                            objinstallation = new tblinstallation();
                            objinstallation.devicetoken = devicetoken;
                            objinstallation.devicetype = devicetype.ToString();
                            objinstallation.deviceid = deviceid;
                            objinstallation.addedon = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                            entities.AddTotblinstallations(objinstallation);
                            entities.SaveChanges();
                            return objinstallation.id;
                        }
                        else
                        {
                            return objinstallation.id;
                        }
                    }
                }
                else
                {
                    objinstallation = entities.tblinstallations.Where(s => s.devicetoken == devicetoken && s.devicetype == strdevicetype).FirstOrDefault();
                    if (objinstallation == null)
                    {
                        objinstallation = new tblinstallation();
                        objinstallation.devicetoken = devicetoken;
                        objinstallation.devicetype = devicetype.ToString();
                        objinstallation.addedon = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        entities.AddTotblinstallations(objinstallation);
                        entities.SaveChanges();
                        return objinstallation.id;
                    }
                    else
                    {
                        return objinstallation.id;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int64 SetUserInstallation(Int64 userid, Int64 installationid)
        {
            try
            {
                BeeKeepersEntities entities = new BeeKeepersEntities();
                tblinstallation objinstallation = entities.tblinstallations.Where(s => s.id == installationid).FirstOrDefault();
                if (objinstallation != null)
                {
                    objinstallation.userid = userid;
                    entities.tblinstallations.ApplyCurrentValues(objinstallation);
                    entities.SaveChanges();
                    return objinstallation.id;
                }
                else
                {
                    return -2;
                }

            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public static Int64 DeleteInstallation(Int64 installationid)
        {
            try
            {
                BeeKeepersEntities entities = new BeeKeepersEntities();
                tblinstallation objinstallation = entities.tblinstallations.Where(s => s.id == installationid).FirstOrDefault();
                if (objinstallation != null)
                {
                    objinstallation.userid = null;
                    entities.tblinstallations.ApplyCurrentValues(objinstallation);

                    tblLanguage objlang = entities.tblLanguages.Where(s => s.tokenid == installationid).FirstOrDefault();
                    if (objlang != null)
                    {
                        objlang.tokenid = 0;
                        entities.tblLanguages.ApplyCurrentValues(objlang);
                    }
                    entities.SaveChanges();
                    return 1;
                }
                else
                {
                    return -2;
                }

            }
            catch (Exception ex)
            {
                return -1;
            }
        }

    }

}
