﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersDataBase
{
    public class errorlogRepository
    {


        public static void Add(errorlog errorlog)
        {
            var data = Data.Entities;
            data.AddToerrorlogs(errorlog);
            data.SaveChanges();
        }
        public static void Update(errorlog errorlog)
        {
            var data = Data.Entities;
            data.errorlogs.ApplyCurrentValues(errorlog);
            data.SaveChanges();
        }
        public static void Remove(errorlog errorlog)
        {
            var data = Data.Entities;
            data.errorlogs.DeleteObject(errorlog);
            data.SaveChanges();
        }
        public static errorlog Find(int errorlogId)
        {
            var data = Data.Entities;
            return (from e in data.errorlogs
                    where e.Id == errorlogId
                    select e).SingleOrDefault();
        }
    }
}
