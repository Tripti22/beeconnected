﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersDataBase
{
    public class PropertyRepository
    {
        
        public static Int64? AddProperty(Int64 userId, string latitude, string longitude, string propertyName)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_AddProperty(userId, latitude, longitude, propertyName).FirstOrDefault().result;
        }
        public static Int64? UpdateProperty(Int64 userId, string latitude, string longitude, string propertyName,Int64 propertyid)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_UpdateProperty(userId, latitude, longitude, propertyName,propertyid).FirstOrDefault().result;
        }
        public static List<sp_propertylist_Result> Propertylist_admin()
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_propertylist().ToList();
        }
        public static List<sp_getNearbyProperty_Result> NearestProperty(int distance,string latitude,string longitude)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_getNearbyProperty(distance,latitude,longitude).ToList();
        }

        public static List<sp_getPropertiesForActivityAdded_Result> GetUsersForNotification(long userId, int userType, string latitude, string longitude, string startDate, string endDate)
        {
            BeeKeepersEntities entities = new BeeKeepersEntities();
            return entities.sp_getPropertiesForActivityAdded(userId, userType, latitude, longitude, startDate, endDate, 5).ToList();
        }
    }
}
