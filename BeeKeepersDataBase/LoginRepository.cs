﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersDataBase
{
    public class LoginRepository
    {
        public static tbladmin DoLogin(string username, string password)
        {
            BeeKeepersEntities entity = new BeeKeepersEntities();
            return entity.tbladmins.Where(s => s.username == username && s.password == password).FirstOrDefault();
        }
    }
}
