﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeKeepersDataBase
{
    public class PageRepository
    {
        public static List<tblPage> SelectAllPages()
        {
            var data = Data.Entities;
            return data.tblPages.ToList();
        }
        public static Int64? InsertUpdatePage(string pagename, string pagecontent, string formatedname, string frenchcontent)
        {
            var data = Data.Entities;
            return data.sp_InsertUpdatePage(pagename, pagecontent, formatedname,frenchcontent).FirstOrDefault().result;
        }
        public static Int64 DeletePage(string pagename)
        {
            var data = Data.Entities;
            var objpage=data.tblPages.Where(s=>s.pagename==pagename).FirstOrDefault();
            if (objpage != null)
            {
                data.tblPages.DeleteObject(objpage);
                data.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
            

        }
        
    }
}
