﻿using BeeKeepersBusiness;
using BeeKeepersDataBase;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace BeekeepersWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }
        void Session_Start(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("En");
            //This is obviously a new session being created; it can be
            //created at the first hit of a user, or when the user
            //previous session has expired (timeout). We are only interested
            //in the timeout scenario, so we look at the request cookies
            //and if we have a previous session ID cookie, it means this is a
            //new session due to the timing out of the old one.
            //Note: slight problem here: in .Net 2.0 the ASP Session ID
            //cookie name is configurable, but we don't have a way to
            //retrieve that from the web.config - so if you customize
            //the session cookie name in the web.config you'll have to
            //use the same name here.
            string request_cookies = Request.Headers["Cookie"];
            if ((null != request_cookies) &&
                    (request_cookies.IndexOf("ASP.NET_SessionId") >= 0))
            {
                //cookie existed, so this new one is due to timeout.
                //Redirect the user to the login page
                //System.Diagnostics.Debug.WriteLine("Session expired!");
                Response.RedirectToRoute("UserLogin");
            }
        }
        void Session_End(object sender, EventArgs e)
        {
        //    // Code that runs when a session ends.
        //    // Note: The Session_End event is raised only when the sessionstate mode
        //    // is set to InProc in the Web.config file. If session mode is set to StateServer
        //    // or SQLServer, the event is not raised.
        //    try
        //    {
        //        System.Web.Security.FormsAuthentication.SignOut();
        //        System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        //    }
        //    catch (Exception ex)
        //    {
 
        //    }
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        //let us take out the username now                
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;
                        BeeKeepersEntities entity = new BeeKeepersEntities();

                        tbladmin admin = entity.tbladmins.Where(s => s.username == username).FirstOrDefault();
                        if (admin != null)
                        {
                            roles = admin.userrole.ToString();
                        }
                        tbluser user = entity.tblusers.Where(s => s.username == username).FirstOrDefault();
                        if (user != null)
                        {
                            roles = user.usertype.ToString();
                        }
                        //let us extract the roles from our own custom cookie


                        //Let us set the Pricipal with our user specific details
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        FormsAuthentication.SignOut();
                        //somehting went wrong
                    }
                }
            }

        }

    }
}