﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BeeKeepersBusiness;
using BeekeepersWeb.Areas.Manager.Models;

namespace BeekeepersWeb.Areas.Manager.Controllers
{
    public class AdminController : Application_adminController
    {
        //
        // GET: /Manager/Admin/

        public ActionResult Index()
        {
            //return RedirectToAction("Users");
            return View();
        }
        public ActionResult Logout()
        {
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToRoute("Manager_default");
        }
        public ActionResult ChangePassword()
        {

            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel Model)
        {
            string oldpassword = Model.OldPassword;
            string newPassword = Model.NewPassword;
            
            BeeKeepersBusiness.Admin admin = new BeeKeepersBusiness.Admin();
            int status = admin.ChangePassword(UserSession.UserId, oldpassword, newPassword);
            if (status == 1)
            {
                ModelState.Clear();
                ViewBag.status = status.ToString();
                ViewBag.message = BeeKeepersBusiness.Resource.Passwordsuceessfullychanged;
            }
            else
            {
                ViewBag.status = status.ToString();
                ViewBag.message = BeeKeepersBusiness.Resource.Oldpasswordisinvalid;
            }
            return View();
        }
        public void ChangeLanguage(string lang)
        {
            new BeeKeepersBusiness.SiteLanguages().SetLanguage(lang);
            Response.Redirect(HttpContext.Request.UrlReferrer.ToString());
        }
    }
}
