﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;
using BeekeepersWeb.Areas.Manager.Models;
using System.Web.Script.Serialization;
namespace BeekeepersWeb.Areas.Manager.Controllers
{
    public class PageContentsController : Application_adminController
    {
        //
        // GET: /Manager/PageContents/
        PageBusiness pagebusiness = new PageBusiness();
        public ActionResult Index()
        {
            return View(pagebusiness.SelectAllPages());
        }
        public ActionResult AddEditPage()
        {
            return View();
        }
        
        [HttpPost, ValidateInput(false)]
        public ActionResult AddEditPage(FormCollection col)
        {
            ClassPageContent clspagecontent = new ClassPageContent();
            Int64? result = pagebusiness.InsertUpdatePage(col["pagename"].ToString(), col["Editor"].ToString(), col["pageformatedname"].ToString(), col["Editor1"].ToString());
            clspagecontent.pagename = col["pagename"].ToString();
            clspagecontent.Editor = col["Editor"].ToString();
            clspagecontent.Editor1 = col["Editor1"].ToString();
            if (result != null && result == 1)
            {
                ViewBag.message = Resource.Pagecontentaddedupdatedsuccessfully;
                ViewBag.status = "1";
                return View(clspagecontent);
            }
            else
            {
                ViewBag.message = Resource.PagecontentnotaddedupdatedTryagain;
                ViewBag.status = "0";
                return View(clspagecontent);
            }
        }
        
        public ActionResult GetPageContent(string pagename)
        {
            var pagecontents = pagebusiness.SelectAllPages().Where(s => s.pagename == pagename).FirstOrDefault();
            if (pagecontents != null)
            {

                return Json(new JavaScriptSerializer().Serialize(pagecontents));
            }
            else
            {
                return Json(string.Empty);
            }
        }
        public ActionResult Deletepage(string id)
        {
            Int64 result = pagebusiness.DeletePage(id);
            if (result != null && result == 1)
            {
                ViewBag.message = Resource.PageDeletedSuccessfully;
                ViewBag.status = "1";

            }
            else
            {
                ViewBag.message = Resource.pagenotdeletedTryagain;
                ViewBag.status = "0";
            }
            return View("index", pagebusiness.SelectAllPages());
        }
        
    }
}
