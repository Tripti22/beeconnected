﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;
namespace BeekeepersWeb.Areas.Manager.Controllers
{
    public class PropertiesController : Application_adminController
    {
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            User userbusiness = new User();
            ViewBag.users = new SelectList(userbusiness.UsersList_Admin(2, "").OrderBy(s=>s.username).ToList() ,"id","username");
            return View(new Property().Propertlist_admin().OrderBy(s=>s.propertyname).ToList());
        }
        [HttpPost]
        public ActionResult Index(FormCollection col)
        {
            Property property = new Property();

            List<BeeKeepersDataBase.sp_propertylist_Result> lstproperty = property.Propertlist_admin();
            if (col["txtpropertyname"].ToString() != "")
            {
                lstproperty = lstproperty.Where(s => s.propertyname.ToLower().StartsWith(col["txtpropertyname"].ToString().ToLower())).ToList();
            }
            Int64 username=0;
            if (col["username"].ToString() != "0" && col["username"].ToString() != "")
            {
                username = int.Parse(col["username"]);
                lstproperty = lstproperty.Where(s => s.userid== username).ToList();
            }

            ViewBag.propertyname = col["txtpropertyname"].ToString();
            User userbusiness = new User();
            ViewBag.users = new SelectList(userbusiness.UsersList_Admin(2, "").OrderBy(s => s.username).ToList(), "id", "username",username);
            return View(lstproperty.OrderBy(s => s.propertyname).ToList());
        }
        public ActionResult Delete(Int64 id)
        {
            Property property = new Property();
            Int64? result = property.DeleteProperty(id);
            if (result != null && result == 1)
            {
                TempData["message"] = Resource.Propertydeletedsuccessfully;
                TempData["status"] = "1";

            }
            else
            {
                TempData["message"] = Resource.PropertynotaddedTryagain;
                TempData["status"] = "0";
            }
            return RedirectToAction("index");
        }

    }
}
