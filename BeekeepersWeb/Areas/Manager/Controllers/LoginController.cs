﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;
using System.Web.Security;
namespace BeekeepersWeb.Areas.Manager.Controllers
{
    public class LoginController : MyBaseController
    {
        //
        // GET: /Manager/Login/

        public ActionResult Index()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["userid"])) && UserSession.UserRole == "admin_1")
            {
                return RedirectToAction("index", "users");
            }
            else
            {
                return View("Login");
            }
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection col, string returnUrl)
        {
            LoginBusiness loginbusines = new LoginBusiness();
            var user = loginbusines.DoLogin(col["userName"].ToString(), col["password"].ToString());
            if (user != null)
            {
                UserSession.UserId = user.id;
                UserSession.UserRole = "admin_1";
                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToAction("index","users");
                }
                
            }
            else
            {
                ViewBag.messsage = Resource.InvalidLoginCredentials;
                return View();
            }

        }

       


    }
}
