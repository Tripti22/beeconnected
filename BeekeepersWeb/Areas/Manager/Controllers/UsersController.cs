﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;
namespace BeekeepersWeb.Areas.Manager.Controllers
{
    public class UsersController : Application_adminController
    {
        //
        // GET: /Manager/Users/
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            ViewBag.usertype = "0";
            return View(new User().UsersList_Admin(0, ""));
        }
        
        [HttpPost]
        public ActionResult Index(FormCollection col)
        {
            User user = new User();

            List<BeeKeepersDataBase.sp_users_Result> lstusers = user.UsersList_Admin(0, "");
            if (col["usertype"].ToString() != "0")
            {
                int usertype = int.Parse(col["usertype"]);
                lstusers = lstusers.Where(s => s.usertype == usertype).ToList();
            }
            if (col["txtnickname"].ToString() != "")
            {
                lstusers = lstusers.Where(s => s.username.ToLower().StartsWith(col["txtnickname"].ToString().ToLower())).ToList();
            }
            ViewBag.nickname = col["txtnickname"].ToString();
            ViewBag.usertype = col["usertype"].ToString();
            return View(lstusers.OrderByDescending(s => s.addedon).ToList());
        }

        //public ActionResult DeleteUser(Int64 id)
        //{
        //    User user = new User();
        //    Int64? result = user.DeleteUser(id);
        //    if (result != null && result == 1)
        //    {
        //        TempData["message"] = Resource.UserDeletedSuccessfully;
        //        TempData["status"] = "1";

        //    }
        //    else
        //    {
        //        TempData["message"] = Resource.UsernotdeletedTryagain;
        //        TempData["status"] = "0";
        //    }
        //    return RedirectToAction("index", user.UsersList_Admin(0, ""));
        //}

    }
}
