﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;


namespace BeekeepersWeb.Areas.Manager.Controllers
{
    public class ActivityController : Application_adminController
    {
        //
        // GET: /Manager/Activity/
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }

            return View(new Activities().ActivityList().OrderByDescending(s => s.startdate).ToList());
        }
        [HttpPost]
        public ActionResult Index(FormCollection col)
        {
            DateTime startdate=DateTime.Now;
            DateTime enddate=DateTime.Now ;
            Activities activity = new Activities();
            if (col["txtstartdate"] != null && col["txtstartdate"] != "")
                startdate = Convert.ToDateTime(col["txtstartdate"]);

            if (col["txtenddate"] != null && col["txtenddate"] != "")
                enddate = Convert.ToDateTime(col["txtenddate"]);
            List<BeeKeepersDataBase.SP_Select_Activitylist_Result> lstactivity = activity.ActivityList();
            if (col["usertype"].ToString() != "0")
            {
                int usertype = int.Parse(col["usertype"]);
                lstactivity = lstactivity.Where(s => s.usertype == usertype).ToList();
            }
            if (col["txtnickname"].ToString() != "")
            {
                lstactivity = lstactivity.Where(s => s.username.ToLower().StartsWith(col["txtnickname"].ToString().ToLower())).ToList();
            }
            if (col["txtstartdate"] != null && col["txtstartdate"] != "" && col["txtenddate"] != null && col["txtenddate"] != "")
            {
                lstactivity = lstactivity.Where(s => s.startdate >= startdate && s.enddate <= enddate).ToList();
            }
            else if (col["txtstartdate"] != null && col["txtstartdate"] != "")
            {
                lstactivity = lstactivity.Where(s => s.startdate > startdate).ToList();
            }
            else if (col["txtenddate"] != null && col["txtenddate"] != "")
            {
                lstactivity = lstactivity.Where(s => s.enddate < enddate).ToList();
            }

            ViewBag.startdate = col["txtstartdate"];
            ViewBag.enddate = col["txtenddate"];
            ViewBag.nickname = col["txtnickname"].ToString();
            ViewBag.usertype = col["usertype"].ToString();
            return View(lstactivity.OrderByDescending(s => s.startdate).ToList());
        }
        public ActionResult Delete(Int64 id)
        {
            Activities user = new Activities();
            Int64? result = user.DeleteActivity(id);
            if (result != null && result == 1)
            {
                TempData["message"] = BeeKeepersBusiness.Resource.Activitydeletedsuccessfully;
                TempData["status"] = "1";
            }
            else
            {
                TempData["message"] =BeeKeepersBusiness.Resource.ActivitynotdeletedTryagain;
                TempData["status"] = "0";
            }
            return RedirectToAction("index");
        }



    }
}
