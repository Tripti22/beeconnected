﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeekeepersWeb.Areas.Manager.Models;
using BeeKeepersBusiness;
namespace BeekeepersWeb.Areas.Manager.Controllers
{
    public class AdminMessageController : Application_adminController
    {
        //
        // GET: /Manager/AdminMessage/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(AdminMessageModel model)
        {
            Int64? result = new AdminMessages().SendAdminMessage(int.Parse(model.Usertype), model.Message, model.MessageFR);
            if (result != null && result == 1)
            {
                ModelState.Clear();
                ViewBag.message = Resource.Messagesent;
                ViewBag.status = "1";
                return View();
            }
            else
            {
                ViewBag.message = Resource.MessagenotsentTryagain;
                ViewBag.status = "0";
                return View();
            }
        }
    }
}
