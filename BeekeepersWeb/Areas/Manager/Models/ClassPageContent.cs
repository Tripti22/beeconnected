﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BeekeepersWeb.Areas.Manager.Models
{
    public class ClassPageContent
    {
        public Int64 id { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName ="Pleaseselectpagename", ErrorMessage = null)]
        public string pagename { get; set; }
        
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleasefillpagecontentforenglishversion", ErrorMessage = null)]
        [DataType(DataType.MultilineText)]
        public string Editor { get; set; }

        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleasefillpagecontentforfrenchversion", ErrorMessage = null)]
        [DataType(DataType.MultilineText)]
        public string Editor1 { get; set; }
        
    }
}