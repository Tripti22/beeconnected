﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BeekeepersWeb.Areas.Manager.Models
{
    public class AdminMessageModel
    {
        public List<SelectListItem> ListUserType{get;set;}

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "PleaseSelectUserType", ErrorMessage = null)]
        public string Usertype { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseentermessageinenglish", ErrorMessage = null)]
        [MaxLength(1000, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Messagemustbelessthanequal1000char", ErrorMessage = null)]
        public string Message { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseentermessageinfrench", ErrorMessage = null)]
        [MaxLength(1000, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Messagemustbelessthanequal1000char", ErrorMessage = null)]
        public string MessageFR { get; set; }

        
    }
}