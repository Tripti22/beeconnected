﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Areas.Manager.Models
{
    public class Test
    {
        [Required(ErrorMessage = "Nom d'utilisateur Obligatoire")]
        public string username { get; set; }
        [Required(ErrorMessage = "mot de passe requis")]
        public string password { get; set; }
    }
}