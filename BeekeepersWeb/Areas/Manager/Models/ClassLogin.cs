﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeekeepersWeb.Areas.Manager.Models
{
    public class ClassLogin
    {
        public string userName{ get; set; }
        public string password { get; set; }
    }
}