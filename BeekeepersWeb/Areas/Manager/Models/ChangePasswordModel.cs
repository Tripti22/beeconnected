﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Areas.Manager.Models
{
    public class ChangePasswordModel
    {
        [MaxLength(20, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Oldpasswordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName ="Pleaseenteroldpassword")]
        public string OldPassword { get; set; }

        [MaxLength(20, ErrorMessageResourceName = "NewPasswordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName ="Pleaseenternewpassword", ErrorMessage = null)]
        public string NewPassword { get; set; }

        [MaxLength(20, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Confirmpasswordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Newpasswordandconfirmpasswordmustbesame", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource),  ErrorMessageResourceName = "Pleaseenterconfirmpassword", ErrorMessage = null)]
        public string ConfirmPassword { get; set; }


    }
}