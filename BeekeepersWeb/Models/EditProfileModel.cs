﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Models
{
    public class EditProfileModel
    {
        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Firstnamecannotcontainmorethen100characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterfirstname", ErrorMessage = null)]
        public string firstname { get; set; }

        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Lastnamecannotcontainmorethen100characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterlastname", ErrorMessage = null)]
        public string lastname { get; set; }

        public Int32 usertype { get; set; }

        [MaxLength(20, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Usernamecannotcontainmorethen20characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterusername", ErrorMessage = null)]
        public string UserName { get; set; }



        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Emailcannotcontainmorethan100characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenteremail", ErrorMessage = null)]
        [EmailAddress(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterthevalidemailid", ErrorMessage = null)]
        public string Email { get; set; }

    }
}