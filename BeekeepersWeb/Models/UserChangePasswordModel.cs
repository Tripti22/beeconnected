﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Models
{
    public class UserChangePasswordModel
    {
        [MaxLength(16, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Oldpasswordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenteroldpassword", ErrorMessage = null)]
        public string OldPassword { get; set; }

        [MaxLength(16, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "NewPasswordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenternewpassword", ErrorMessage = null)]
        public string NewPassword { get; set; }

        [MaxLength(16, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Confirmpasswordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Newpasswordandconfirmpasswordmustbesame", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterconfirmpassword", ErrorMessage = null)]
        public string ConfirmPassword { get; set; }

    }
}