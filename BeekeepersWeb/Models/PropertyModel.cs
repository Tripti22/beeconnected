﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Models
{
    public class PropertyModel
    {
        public int id { get; set; }

        public Int64 Userid { get; set; }

        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Propertynamecannotcontainmorethan100Characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "PleaseenterPropertyname", ErrorMessage = null)]
        public string PropertyName { get; set; }



        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterlatitude", ErrorMessage = null)]
        public string latitude { get; set; }



        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterlongitude", ErrorMessage = null)]
        public string longitude { get; set; }



       
    }
}