﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Models
{
    public class RegistrationModel
    {

        public Int64 id { get; set; }

        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Firstnamecannotcontainmorethen100characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterfirstname", ErrorMessage = null)]
        public string firstname { get; set; }

        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Lastnamecannotcontainmorethen100characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterlastname", ErrorMessage = null)]
        public string lastname { get; set; }


        public Int32 usertype { get; set; }

        [RegularExpression("^(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "usernameisnotvalid", ErrorMessage = null)]
        [MaxLength(20, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Usernamecannotcontainmorethen20characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterusername", ErrorMessage = null)]
        public string UserName { get; set; }

        [MaxLength(16, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Passwordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterpassword", ErrorMessage = null)]
        [DataType(DataType.Password)]
        public string Pasword { get; set; }

        [MaxLength(16, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Confirmpasswordcannotcontainmorethen16characters", ErrorMessage = null)]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterconfirmpassword", ErrorMessage = null)]
        [Compare("Pasword", ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Passwordandconfirmpasswordmustbesame", ErrorMessage = null)]
        public string ConfirmPasword { get; set; }

        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Emailcannotcontainmorethan100characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenteremail", ErrorMessage = null)]
        [EmailAddress(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterthevalidemailid", ErrorMessage = null)]
        public string Email { get; set; }




    }
}