﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeekeepersWeb.Models
{
    public class NearbyModel
    {
        public int Property { get; set; }

        public int Activity { get; set; }

        public int Search { get; set; }

    }
}