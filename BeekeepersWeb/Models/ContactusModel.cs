﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Models
{
    public class ContactusModel
    {

        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Namecannotcontainmorethan100characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseentername", ErrorMessage = null)]
        public string name { get; set; }

        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Emailcannotcontainmorethan100characters", ErrorMessage = null)]
        [EmailAddress(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterthevalidemailid", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenteremail", ErrorMessage = null)]
        public string email { get; set; }


        [RegularExpression(@"^[0-9]{0,15}$", ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "contactnumbershouldcontainonlynumbers", ErrorMessage = null)]
        [MaxLength(100, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Contactnocannotcontainmorethan15characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseentercontactno", ErrorMessage = null)]
        public string contactno { get; set; }


        [MaxLength(1000, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Querycannotcontainmorethan1000characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterquery", ErrorMessage = null)]
        public string query { get; set; }

    }
}