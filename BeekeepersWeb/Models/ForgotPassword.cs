﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BeekeepersWeb.Models
{
    public class ForgotPassword
    {
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenteremail", ErrorMessage = null)]
        [EmailAddress(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterthevalidemailid", ErrorMessage = null)]
        public string Email { get; set; }
    }
}