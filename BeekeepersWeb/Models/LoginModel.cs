﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Models
{
    public class LoginModel
    {


        [MaxLength(16, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Passwordcannotcontainmorethen16characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterpassword", ErrorMessage = null)]
        [DataType(DataType.Password)]
        public string Pasword { get; set; }

        [MaxLength(20, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Usernamecannotcontainmorethen20characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterusername", ErrorMessage = null)]
        public string UserName { get; set; }

        public bool Remeberme { get; set; }

    }
}