﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BeekeepersWeb.Models
{
    public class ClassSendMessage
    {
        [MaxLength(1000, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Messagecannotcontainmorethen1000characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource),ErrorMessageResourceName = "Pleaseentermessage", ErrorMessage = null)]
        public string txtMessage { get; set; }
    }
}