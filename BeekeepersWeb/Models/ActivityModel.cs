﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersWeb.Models
{
    public class ActivityModel
    {


        public int id { get; set; }
        public int userid { get; set; }
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "PleaseSelectUserType", ErrorMessage = null)]
        public int usertype { get; set; }

        [MaxLength(1000, ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName ="Notecannotcontainmorethen1000characters", ErrorMessage = null)]
        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "PleaseenterNote", ErrorMessage = null)]
        public string note { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterlatitude", ErrorMessage = null)]
        public string latitude { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterlongitude", ErrorMessage = null)]
        public string longitude { get; set; }
        public string geolocation { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "PleaseenterStartDate" , ErrorMessage = null)]
        public string startdate { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "PleaseenterEndDate", ErrorMessage = null)]
        public string enddate { get; set; }

        [Required(ErrorMessageResourceType = typeof(BeeKeepersBusiness.Resource), ErrorMessageResourceName = "Pleaseenterusername", ErrorMessage = null)]
        public DateTime expirydate { get; set; }





    }
}