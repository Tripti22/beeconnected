﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeekeepersWeb.Models;
using BeeKeepersBusiness;
using PagedList;
using BeeKeepersDataBase;
using System.Globalization;
namespace BeekeepersWeb.Controllers
{
    public class UserActivityController : ApplicationController
    {
        //
        // GET: /UserActivity/

        public ActionResult Index(int? page)
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            Activities activities = new Activities();
            var result = activities.GetUserActivityList(UserSession.UserId).OrderByDescending(s => s.startdate).ToList();
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ActivityModel Model)
        {
            try
            {
                Activities activities = new Activities();
                string startDate = DateTime.ParseExact(Model.startdate, "dd-MM-yyyy", null).ToString("dd/MMM/yyyy",new System.Globalization.CultureInfo("en-US"));
                string endDate = DateTime.ParseExact(Model.enddate, "dd-MM-yyyy", null).ToString("dd/MMM/yyyy", new System.Globalization.CultureInfo("en-US"));
                Int64? result = activities.AddActivity(UserSession.UserId, Convert.ToInt32(UserSession.UserRole), Model.note, Model.latitude, Model.longitude, startDate, endDate);
                if (result != null && result > 0)
                {
                    try
                    {
                        new Activities().SendNotificationToUsers(result,UserSession.UserId, Convert.ToInt32(UserSession.UserRole), Model.latitude, Model.longitude, startDate, endDate, Model.note);
                    }
                    catch (Exception ex)
                    {
                        using (errorlogBusiness errorlog = new errorlogBusiness())
                        {
                            errorlog.InsertErrorLog(0, "notification", ex.Message, "");
                        }
                    }
                    ModelState.Clear();
                    TempData["message"] = BeeKeepersBusiness.Resource.Activityaddedsuccessfully;
                    TempData["status"] = "1";
                    return RedirectToAction("index");
                }
                else
                {
                    ModelState.AddModelError("", Resource.ActivitynotaddedTryagain);

                }
                return View();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", Resource.ActivitynotaddedTryagain);
                return View();
            }
        }

        //
        // GET: /UserActivity/Edit/5

        public ActionResult Edit(Int64 id)
        {
            try
            {
                if (TempData["message"] != null)
                {
                    ViewBag.message = TempData["message"];
                    ViewBag.status = TempData["status"];
                }

                Activities activities = new Activities();
                ActivityResponse activityresponse = activities.EditActivity4Web2(id);
                if (activityresponse != null)
                {
                    ActivityModel Model = new ActivityModel();
                    Model.enddate = activityresponse.endDate;
                    Model.startdate = activityresponse.startDate;
                    Model.usertype = Convert.ToInt32(activityresponse.userType);
                    Model.note = activityresponse.note;
                    Model.latitude = activityresponse.latitude;
                    Model.longitude = activityresponse.longitude;
                    return View(Model);
                }
                else
                {
                    TempData["message"] = Resource.Activitydetailsnotget;
                    TempData["status"] = "0";
                    return RedirectToAction("index");
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = Resource.Activitydetailsnotget;
                TempData["status"] = "0";
                return RedirectToAction("index");
            }

        }

        //
        // POST: /UserActivity/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, ActivityModel model)
        {
            try
            {
                Activities activities = new Activities();
                string startDate = DateTime.ParseExact(model.startdate, "dd-MM-yyyy", null).ToString("dd/MMM/yyyy",new System.Globalization.CultureInfo("en-US"));
                string endDate = DateTime.ParseExact(model.enddate, "dd-MM-yyyy", null).ToString("dd/MMM/yyyy",new System.Globalization.CultureInfo("en-US"));
                Int64? result = activities.UpdateActivity(UserSession.UserId, model.latitude, model.longitude, Convert.ToInt32(UserSession.UserRole), startDate, endDate, id, model.note);
                if (result != null && result > 0)
                {
                    TempData["message"] = Resource.Activityupdatedsuccessfully;
                    TempData["status"] = "1";
                    return RedirectToAction("index");
                }
                else
                {
                    TempData["message"] = Resource.ActivitynotupdatedTryagain;
                    TempData["status"] = "0";
                }
                return RedirectToAction("Edit", id);
            }
            catch
            {
                TempData["message"] = Resource.ActivitynotupdatedTryagain;
                TempData["status"] = "0";
                return RedirectToAction("Edit", id);
            }
        }
        public ActionResult Cancel()
        {
            return RedirectToAction("index");
        }
        public ActionResult Delete(int id)
        {
            Activities activity = new Activities();
            Int64? result = activity.DeleteActivity(id);
            if (result != null && result == 1)
            {
                TempData["message"] = Resource.Activitydeletedsuccessfully;
                TempData["status"] = "1";

            }
            else
            {
                TempData["message"] = Resource.ActivitynotdeletedTryagain;
                TempData["status"] = "0";
            }
            return RedirectToAction("index");
        }

        public ActionResult ViewAround(Int64 id)
        {

            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            if (TempData["txtmessage"] != null)
            {
                ViewBag.txtmessage = TempData["txtmessage"];
            }

            ActivityResponse objactivity = new Activities().EditActivity4Web(id);
            ViewBag.userid = objactivity.userId;
            ViewBag.note = objactivity.note;
            ViewBag.acitvityid = objactivity.id;
            ViewBag.latitude = objactivity.latitude;
            ViewBag.longitude = objactivity.longitude;
            ViewBag.startdate = string.Format("{0:dd-MMM-yyyy}", objactivity.startDate);
            ViewBag.enddate = string.Format("{0:dd-MMM-yyyy}", objactivity.endDate); 
            Activities activities = new Activities();
            ViewBag.activityArround = activities.ActivityList4Web(UserSession.UserId, Convert.ToInt64(UserSession.UserRole), objactivity.latitude, objactivity.longitude, objactivity.startDate, objactivity.endDate, 5);
            return View();


        }
        public ActionResult SendBroadcast(FormCollection col)
        {
            Messages messages = new Messages();
            int result = messages.SendBroadcastMessage4web(Convert.ToInt64(col["hdnactivity"]), col["txtmessage"].ToString());
            if (result >= 1)
            {
                TempData["message"] = Resource.Messagesentto+" " + result.ToString() + " "+Resource.users;
                TempData["status"] = "1";
            }
            else if (result == -2)
            {
                TempData["txtmessage"] = col["txtmessage"].ToString();
                TempData["message"] = Resource.Usersnotavailabletosendmessage;
                TempData["status"] = "0";
            }
            else
            {
                TempData["txtmessage"] = col["txtmessage"].ToString();
                TempData["message"] = Resource.Messagenotsent;
                TempData["status"] = "0";
            }
            return RedirectToAction("ViewAround", new { @id = Convert.ToInt64(col["hdnactivity"]) });
        }
    }
}
