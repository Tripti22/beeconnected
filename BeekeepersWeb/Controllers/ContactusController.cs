﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BeekeepersWeb.Controllers
{
    public class ContactusController : MyBaseController
    {
        //
        // GET: /Contactus/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Models.ContactusModel model)
        {
            BeeKeepersBusiness.User user = new BeeKeepersBusiness.User();

            string email = model.email;
            string contactno = model.contactno;
            string query = model.query;
            string name = model.name;
            int status = user.SendConatctusmail(name, email, contactno, query);
            if (status == 1)
            {
                ViewBag.message = BeeKeepersBusiness.Resource.YourquerysenttoBeeconnectedAdmin;
                ViewBag.status = "1";
            }
            else
            {
                ViewBag.message = BeeKeepersBusiness.Resource.Unabletoprocess;
                ViewBag.status = "0";
            }
            ModelState.Clear();
            return View();

        }

    }
}
