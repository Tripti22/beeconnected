﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersDataBase;
using BeeKeepersBusiness;
using BeekeepersWeb.Models;
using BeeKeepersBusiness.utility;
namespace BeekeepersWeb.Controllers
{

    public class UserProfileController : ApplicationController
    {
        delegate void EmailDelegate(List<UserEmail> lstuseremail);
        public ActionResult Index(Int64 id)
        {
            Int64 actid =0;
            if (Request["actid"] != null)
            {
                actid = Convert.ToInt64(Request["actid"]);
            }
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            ViewBag.type = "current";
            BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities();
            var user = DB.tblusers.Where(s => s.id == id).FirstOrDefault();
            ViewBag.username = user.username;
            ViewBag.userrole = user.usertype;
            ViewBag.activityid = actid;
            ViewBag.userid = user.id;
            ViewBag.activities = ActivityRepository.UserProfileActivities(user.id, DateTime.Now.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), DateTime.Now.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), actid, 5, "", "").ToList();
            return View();
        }
        public ActionResult Search(Int64 id, string latitude, string longitude)
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            ViewBag.type = "current";
            BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities();
            var user = DB.tblusers.Where(s => s.id == id).FirstOrDefault();
            ViewBag.userid = user.id;
            ViewBag.username = user.username;
            ViewBag.userrole = user.usertype;
            ViewBag.userid = user.id;

            ViewBag.activityid = 0;
            ViewBag.latitude = latitude;
            ViewBag.longitude = longitude;
            ViewBag.activities = ActivityRepository.UserProfileActivities(user.id, DateTime.Now.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), DateTime.Now.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), 0, 5, latitude, longitude).ToList();
            return View("index");
        }
        public ActionResult GetActivities(string type, Int64 id,Int64 actid,string latitude, string longitude)
        {
            ViewBag.type = type;
            BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities();
            if (type == "future")
            {
                var user = DB.tblusers.Where(s => s.id == id).FirstOrDefault();
                ViewBag.username = user.username;
                var result = ActivityRepository.UserProfileActivities(id, DateTime.Now.AddDays(1).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), "", actid, 5, latitude, longitude).ToList();
                return PartialView("PVUserProfileActivities", result);
            }
            else if (type == "past")
            {
                var user = DB.tblusers.Where(s => s.id == id).FirstOrDefault();
                ViewBag.username = user.username;
                var result = ActivityRepository.UserProfileActivities(id, "", DateTime.Now.AddDays(-1).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), actid, 5, latitude, longitude).ToList();
                return PartialView("PVUserProfileActivities", result);
            }
            else
            {
                var user = DB.tblusers.Where(s => s.id == id).FirstOrDefault();
                ViewBag.username = user.username;
                var result = ActivityRepository.UserProfileActivities(id, DateTime.Now.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), DateTime.Now.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US")), actid, 5, latitude, longitude).ToList();
                return PartialView("PVUserProfileActivities", result);
            }

            //return PartialView("PVUserProfileActivities", null);
        }
        public ActionResult myactivitiesaround(Int64 id)
        {
            ViewBag.acitvityid = id;
            ActivityResponse objactivity = new Activities().EditActivity(id);
            ViewBag.latitude = objactivity.latitude;
            ViewBag.longitude = objactivity.longitude;

            ViewBag.startdate = Convert.ToDateTime(objactivity.startDate).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            ViewBag.enddate = Convert.ToDateTime(objactivity.endDate).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            ViewBag.usertype = objactivity.userType;
            var result = ActivityRepository.MyActivitiesAroundList(UserSession.UserId, id, 5);
            return View(result);
        }
        public ActionResult Edit()
        {

            EditProfileModel model = new EditProfileModel();
            BeeKeepersBusiness.User user = new User();
            EditProfile EditProfileResult = user.EditProfile(UserSession.UserId);
            model.lastname = EditProfileResult.lastname;
            model.firstname = EditProfileResult.firstname;
            model.usertype = EditProfileResult.userType;
            model.UserName = EditProfileResult.userName;
            model.Email = EditProfileResult.email;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditProfileModel model)
        {

            BeeKeepersBusiness.User user = new User();
            Int64 status = user.UpdateProfile(UserSession.UserId, UserSession.UserName, "", model.Email, Convert.ToInt32(UserSession.UserRole), model.firstname, model.lastname);
            model.usertype = int.Parse(UserSession.UserRole);
            if (status > 0)
            {
                ViewBag.message = BeeKeepersBusiness.Resource.Profileupdatedsuccessfully;
                ViewBag.status = "1";
            }
            else
            {
                ViewBag.message = BeeKeepersBusiness.Resource.ProfilenotupdatedTryagain;
                ViewBag.status = "0";
            }
            return View(model);

        }
        [HttpPost]
        public ActionResult SendMessage(Int64 id,FormCollection col)
        {
            Int64 actid = 0;
            if (Request["actid"] != null)
            {
                actid = Convert.ToInt64(Request["actid"]);
            }
            using (Messages messages = new Messages())
            {
                if (id != null)
                {
                    Int64? result = messages.SendMessage(UserSession.UserId, id, col["txtMessage"].ToString(), "",1);
                    if (result != null && result > 0)
                    {
                        var installation = Data.Entities.tblinstallations.Where(s => s.userid == id).ToList();
                        var fromuserinfo = Data.Entities.tblusers.Where(s => s.id == UserSession.UserId).FirstOrDefault();
                        var touserinfo = Data.Entities.tblusers.Where(s => s.id == id).FirstOrDefault();

                        string langEmail = new Common().GetLanguage4Emails(id);
                        List<UserEmail> lstuseremail = new List<UserEmail>();
                        UserEmail objuseremail = new UserEmail();
                        objuseremail.fromUserName = fromuserinfo.username;
                        objuseremail.toUserName = touserinfo.username;
                        objuseremail.toEmailId = touserinfo.email;
                        objuseremail.UserLanguage = langEmail;
                        objuseremail.message = fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(langEmail, "sentyouamessage");
                        lstuseremail.Add(objuseremail);

                        int? badgecount = new Common().GetNotificationCounter(id);
                        if (installation != null)
                        {
                            foreach (var item in installation)
                            {
                                string langnoti = new Common().GetLanguage4Notification(item.userid);
                                if (item.devicetype == "1")
                                {
                                    iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(langnoti, "sentyouamessage"), badgecount, "Message", UserSession.UserId, result, "");
                                }
                                else if (item.devicetype == "2")
                                {
                                    AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(langnoti, "sentyouamessage"), badgecount, "Message", UserSession.UserId,result, "");
                                }
                            }
                            EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Message);
                            AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                            IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail, cb, emailDelegate);

                        }
                        TempData["message"] = Resource.Messagesentto + " " + objuseremail.toUserName;
                        TempData["status"] = "1";
                    }
                    else
                    {
                        TempData["message"] = Resource.Messagenotsent;
                        TempData["status"] = "0";
                    }
                }
                else
                {
                    TempData["message"] = Resource.Messagenotsent;
                    TempData["status"] = "0";
                }
            }
            return RedirectToAction("index", new { id = id, actid = actid });
        }
        public static void MyAsyncCallback(IAsyncResult ar)
        {

        }
    }
}
