﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;
using BeeKeepersDataBase;
using GoogleMaps.LocationServices;

namespace BeekeepersWeb.Controllers
{
    public class NearbyController : ApplicationController
    {
        //
        // GET: /Nearby/

        public ActionResult Index()
        {
            ViewBag.lstactivity = new BeeKeepersBusiness.Activities().GetUserActivityList(UserSession.UserId).OrderByDescending(s => s.startdate).ToList();
            ViewBag.lstproperty = new BeeKeepersBusiness.Property().PropertyList(UserSession.UserId).OrderBy(s => s.propertyName).ToList();
            return View();
        }

        public PartialViewResult ActivityList(Int64 id, string startdate, string enddate)
        {
            if (startdate != null && startdate != "")
            {
                startdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            }
            if (enddate != null && enddate != "")
            {
                enddate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            }
            ActivityResponse result = new Activities().EditActivity(id);
            ViewBag.latitude = result.latitude;
            ViewBag.longitude = result.longitude;
            ViewBag.actid = id;
            List<sp_ListActivities_Result> aroundactivity = ActivityRepository.ActivityList(UserSession.UserId, Convert.ToInt64(UserSession.UserRole), result.latitude, result.longitude, startdate, enddate, 5);
            int count = aroundactivity.Count;
            if (UserSession.UserRole == "1")
            {
                var Propertyresult = BeeKeepersDataBase.PropertyRepository.NearestProperty(5, result.latitude, result.longitude);
                foreach (var item in Propertyresult)
                {
                    count = count + 1;
                    //MyActivitiesAroundResponse n =new MyActivitiesAroundResponse();

                    sp_ListActivities_Result n = new sp_ListActivities_Result();
                    n.latitude = item.latitude;
                    n.longitude = item.longitude;
                    n.usertype = 2;
                    n.note = item.propertyname;
                    n.username = item.username;
                    n.userid = item.userid;
                    aroundactivity.Add(n);
                }
            }

            return PartialView(aroundactivity);
        }

        public PartialViewResult PropertyList(Int64 id, string startdate, string enddate)
        {
            PropertyResponse propertiesresult = new Property().EditProperty(id);
            ViewBag.Platitude = propertiesresult.latitude;
            ViewBag.Plongitude = propertiesresult.longitude;
            ViewBag.pid = id;
            if (startdate != null && startdate != "")
            {
                startdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            }
            if (enddate != null && enddate != "")
            {
                enddate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            }
            List<sp_ListActivities_Result> lstPropertyresult = ActivityRepository.ActivityList(UserSession.UserId, Convert.ToInt64(UserSession.UserRole), propertiesresult.latitude, propertiesresult.longitude, startdate, enddate, 5);

            return PartialView(lstPropertyresult);
        }


        public PartialViewResult Search(string startdate, string enddate, string searchtext)
        {
            AddressData addresses = new AddressData();
            addresses.Address = searchtext;
            addresses.Zip = null;
            var gls = new GoogleLocationService();
            try
            {
                var latlong = gls.GetLatLongFromAddress(addresses);
                string Latitude = Convert.ToString(latlong.Latitude).Replace(",",".");
                string Longitude = Convert.ToString(latlong.Longitude).Replace(",", "."); ;

                ViewBag.Platitude = Latitude;
                ViewBag.Plongitude = Longitude;
                if (startdate != null && startdate != "")
                {
                    startdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", null).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
                }
                if (enddate != null && enddate != "")
                {
                    enddate = DateTime.ParseExact(enddate, "dd-MM-yyyy", null).ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
                }
                var id = UserSession.UserId;
                var name = UserSession.UserName;
                var role = UserSession.UserRole;
                List<sp_ListActivities_Result> lstPropertyresult = ActivityRepository.ActivityList(UserSession.UserId, Convert.ToInt64(UserSession.UserRole), Latitude, Longitude, startdate, enddate, 5);
                return PartialView(lstPropertyresult);
            }
            catch (Exception ex)
            {
                return PartialView("defaultmap");
            }
            
        }


    }
}
