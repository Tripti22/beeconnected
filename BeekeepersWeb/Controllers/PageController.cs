﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;
namespace BeekeepersWeb.Controllers
{
    public class PageController : MyBaseController
    {
        public ActionResult Aboutus()
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("aboutus");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
                {
                    if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }
        public ActionResult Resource()
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("resource");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
                {
                    if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }

        public ActionResult Beekeepers()
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("beekeepers");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent)!="")
                {
                    if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }
        public ActionResult Farmers()
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("farmers");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
                {
                    if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }
        public ActionResult Contractors()
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("contractors");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
                {
                    if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }
        public ActionResult Privacy()
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("privacy");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
                {
                    if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }
        public ActionResult Terms()
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("terms");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
                {
                    if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }

    }
}
