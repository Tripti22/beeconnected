﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeekeepersWeb.Models;
using BeeKeepersBusiness;
using BeekeepersWeb.Util;
using System.Web.Security;

namespace BeekeepersWeb.Controllers
{
    public class UserLoginController : MyBaseController
    {
        public ActionResult Index()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["userid"])) && UserSession.UserRole == "admin_1")
            {
                return RedirectToAction("index", "admin");
            }
            else
            {
                return View("Login");
            }
        }


        public ActionResult RedirectToLogin()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("index", "Home");
        }

        public ActionResult Login()
        {
            LoginModel model = new LoginModel();
            if (Request.Cookies["UserName"] != null)

                model.UserName = Request.Cookies["UserName"].Value;

            if (Request.Cookies["Password"] != null)

                model.Pasword = Request.Cookies["Password"].Value;
            if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                model.Remeberme = true;

            return View(model);
        }


        [HttpPost]
        public ActionResult Login(LoginModel Model, string returnUrl)
        {
            try
            {
                EncryptionDecryption hashing = new EncryptionDecryption();
                string password = hashing.encryption(Model.Pasword);
                BeeKeepersBusiness.User loginbusiness = new BeeKeepersBusiness.User();
                var result = loginbusiness.DoLogin(Model.UserName, password);
                if (result != null)
                {
                    if(result.isactive == false)
                    {
                        ModelState.Remove("Pasword");
                        ModelState.AddModelError("Error", BeeKeepersBusiness.Resource.InvalidloginUserdeactivated);
                        return View("Login");
                    }
                    using (BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities())
                    {
                        var language = DB.tblLanguages.Where(s => s.userid == result.id && s.sorcetype == 1).FirstOrDefault();
                        if (language != null)
                        {
                            language.language = (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")?"fr":"en";
                            language.tokenid = 0;
                            language.lastupdateon = DateTime.UtcNow;
                            DB.tblLanguages.ApplyCurrentValues(language);
                            DB.SaveChanges();
                        }
                    }
                    
                    //FormsAuthentication.SetAuthCookie(Model.UserName.ToString(), false);
                    UserSession.UserName = result.username;
                    UserSession.UserId = result.id;
                    UserSession.UserRole = result.usertype.ToString();

                    if (Model.Remeberme)
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UserName"].Value = result.username;
                        Response.Cookies["Password"].Value = result.password;
                    }
                    else
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

                    }
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("index", "Dashboard");
                    }



                }
                else
                {
                    ModelState.Remove("Pasword");
                    ModelState.AddModelError("Error", BeeKeepersBusiness.Resource.Invalidusernameorpassword);
                    return View("Login");
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("Error", BeeKeepersBusiness.Resource.Error);
                return View();
            }
        }
        
        

        //
        // GET: /Login/Edit/5

        public ActionResult ForgetPassword()
        {
            return View();
        }

        //
        // POST: /Login/Edit/5

        [HttpPost]
        public ActionResult ForgetPassword(BeekeepersWeb.Models.ForgotPassword model)
        {
            try
            {
                BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities();
                var objuser = DB.tblusers.Where(s => s.email == model.Email).FirstOrDefault();
                if (objuser != null)
                {
                    int status = Email.SendForgotPasswordMail(objuser.firstname + " " + objuser.lastname, objuser.email, objuser.username, objuser.password, Request.PhysicalApplicationPath, Request.Cookies.Get("culture") != null ? Request.Cookies.Get("culture").Value.ToString() : "en");
                    if (status == 1)
                    {
                        ModelState.Clear();
                        ViewBag.message = BeeKeepersBusiness.Resource.YourpasswordhassenttoyourEmaild;
                        ViewBag.status = "1";
                    }
                    else
                    {
                        ModelState.AddModelError("", BeeKeepersBusiness.Resource.Unabletoprocess);
                    }
                }
                else
                {
                    ModelState.AddModelError("", BeeKeepersBusiness.Resource.ThisEmailIdDoesNotExits);
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(UserChangePasswordModel model)
        {
            BeeKeepersBusiness.User user = new User();
            EncryptionDecryption hashing = new EncryptionDecryption();
            string oldPassword = hashing.encryption(model.OldPassword);
            string confirmPassword = hashing.encryption(model.ConfirmPassword);
            long status = user.ChangePassword(UserSession.UserId, oldPassword, confirmPassword);
            if (status == 1)
            {
                ViewBag.message = BeeKeepersBusiness.Resource.Yourpasswordsucessfullychanged;
                ViewBag.status = "1";
            }
            else if (status == -2)
            {
                ViewBag.message = BeeKeepersBusiness.Resource.Oldpasswordisnotcorrect;
                ViewBag.status = "0";
            }
            else
            {
                ViewBag.message = BeeKeepersBusiness.Resource.PasswordnotchangedPleasetryagain;
                ViewBag.status = "0";
            }
            return View();
        }

        public ActionResult ForgetUsername()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetUsername(BeekeepersWeb.Models.ForgotPassword model)
        {
            try
            {
                BeeKeepersDataBase.BeeKeepersEntities DB = new BeeKeepersDataBase.BeeKeepersEntities();
                var objuser = DB.tblusers.Where(s => s.email == model.Email).FirstOrDefault();
                if (objuser != null)
                {
                    int status = Email.SendForgotUsernameMail(objuser.firstname + " " + objuser.lastname, objuser.email, objuser.username, "", Request.PhysicalApplicationPath, Request.Cookies.Get("culture") != null ? Request.Cookies.Get("culture").Value.ToString() : "en");
                    if (status == 1)
                    {
                        ModelState.Clear();
                        ViewBag.message = BeeKeepersBusiness.Resource.YourusernamehassenttoyourEmaild;
                        ViewBag.status = "1";
                    }
                    else if (status == 0)
                    {
                        ModelState.AddModelError("",BeeKeepersBusiness.Resource.Unabletoprocess);
                    }
                    else if (status == 2)
                    {
                        ModelState.AddModelError("", BeeKeepersBusiness.Resource.Unabletoprocess);
                    }
                }
                else
                {
                    ModelState.AddModelError("", BeeKeepersBusiness.Resource.Userdoesnotexits);
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
    }
}
