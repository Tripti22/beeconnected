﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersDataBase;
using BeeKeepersBusiness;
using BeekeepersWeb.Models;
using BeekeepersWeb.Util;
using System.Web.Security;

namespace BeekeepersWeb.Controllers
{
    public class RegistrationController : MyBaseController
    {
        public ActionResult Index()
        {
            return View("Create");
        }
        public ActionResult Create()
        {
            PageBusiness pagebus = new PageBusiness();
            var objpage = pagebus.SelectPageText("terms");
            if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
            {
                if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")
                {
                    ViewBag.termstext = objpage.frenchContent;
                }
                else
                {
                    ViewBag.termstext = objpage.pagecontent;
                }
            }
            else
            {
                ViewBag.termstext = BeeKeepersBusiness.Resource.ComingSoon;
            }
            //objpage = pagebus.SelectPageText("resource");
            //if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
            //{
            //    ViewBag.restext = objpage.pagecontent;
            //}
            //else
            //{
            //    ViewBag.restext = "À Venir ...";
            //}
            return View();
        }
        [HttpPost]
        public ActionResult Create(RegistrationModel Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EncryptionDecryption hashing = new EncryptionDecryption();
                    BeeKeepersBusiness.User user = new BeeKeepersBusiness.User();
                    user.username = Model.UserName;
                    user.password = hashing.encryption(Model.Pasword);
                    user.lastname = Model.lastname;
                    user.usertype = Model.usertype;
                    user.firstname = Model.firstname;
                    user.email = Model.Email;

                    Int64 status = user.Create(Model.UserName, Model.Email);
                    if (status == -2)
                    {
                        ModelState.AddModelError("username", BeeKeepersBusiness.Resource.Usernamealreadyexist);
                    }
                    else if (status == -3)
                    {
                        ModelState.AddModelError("Emailid", BeeKeepersBusiness.Resource.Emailalreadyexist);
                    }
                    else
                    {
                        string usertype ="";
                        switch (Model.usertype)
                        {
                            case 1: usertype = "Beekeeper";break;
                            case 2: usertype = "Farmer"; break;
                            case 3: usertype = "Contractor"; break;
                        }

                        Email.SendResistrationMail(Model.Email, Model.firstname, Model.lastname, usertype, Model.UserName, Model.Pasword, Request.PhysicalApplicationPath, (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr") ? "FR" : "EN");
                        BeeKeepersBusiness.User loginbusiness = new BeeKeepersBusiness.User();
                        var result = loginbusiness.DoLogin(Model.UserName, Model.Pasword);
                        if (result != null)
                        {
                            using (BeeKeepersEntities DB = new BeeKeepersEntities())
                            {
                                BeeKeepersDataBase.tblLanguage objLanguage = new BeeKeepersDataBase.tblLanguage()
                                {
                                    language = (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr")?"fr":"en",
                                    sorcetype = 1,
                                    userid = result.id,
                                    tokenid = 0,
                                    lastupdateon = DateTime.UtcNow
                                };
                                DB.tblLanguages.AddObject(objLanguage);
                                BeeKeepersDataBase.tblLanguage objLanguage1 = new BeeKeepersDataBase.tblLanguage()
                                {
                                    language = (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value == "fr") ? "fr" : "en",
                                    sorcetype = 2,
                                    userid = result.id,
                                    tokenid = 0,
                                    lastupdateon = DateTime.UtcNow
                                };
                                DB.tblLanguages.AddObject(objLanguage1);
                                DB.SaveChanges();
                            }
                            FormsAuthentication.SetAuthCookie(Model.UserName.ToString(), false);
                            UserSession.UserName = result.username;
                            UserSession.UserId = result.id;
                            UserSession.UserRole = result.usertype.ToString();

                            return RedirectToAction ("index", "Dashboard");
                        }
                    }
                }

                return View("Create");

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", BeeKeepersBusiness.Resource.Error);
                return View();

            }
        }

        
        public ActionResult Disclaimer(string key)
        {
            using (PageBusiness pagebus = new PageBusiness())
            {
                var objpage = pagebus.SelectPageText("terms");
                if (objpage != null && objpage.pagecontent != null && Convert.ToString(objpage.pagecontent) != "")
                {

                    if (key != null && key == "fr")
                    {
                        ViewBag.pagetext = objpage.frenchContent;
                    }
                    else
                    {
                        ViewBag.pagetext = objpage.pagecontent;
                    }
                }
                else
                {
                    if(key!=null)
                        new SiteLanguages().SetLanguage(key.ToUpper());
                    else
                        new SiteLanguages().SetLanguage("EN");
                    ViewBag.pagetext = BeeKeepersBusiness.Resource.ComingSoon;
                }
            }
            return View();
        }



    }
}
