﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeekeepersWeb.Models;
using BeeKeepersBusiness;

namespace BeekeepersWeb.Controllers
{
    public class HomeController : MyBaseController
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        public void ChangeLanguage(string lang)
        {
            if (UserSession.UserId != 0)
            {
                new Common().UpdateLanguageSetting4Web(UserSession.UserId, lang, 2);
            }
            
            new BeeKeepersBusiness.SiteLanguages().SetLanguage(lang);
            Response.Redirect(HttpContext.Request.UrlReferrer.ToString());
        }
        
    }
}
