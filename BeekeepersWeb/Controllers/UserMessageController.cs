﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using BeeKeepersBusiness;
using BeeKeepersDataBase;
using BeeKeepersBusiness.utility;
using System.Threading;
using System.Net;
using System.IO;

namespace BeekeepersWeb.Controllers
{
    public class UserMessageController : ApplicationController
    {
        delegate void EmailDelegate(List<UserEmail> lstuseremail);
        public ActionResult Index()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult UserList()
        {
            if (UserSession.UserId != 0)
            {
                Messages msg = new Messages();
                //ViewBag.AdminCounter = MessageRepository.GetUnreadAdminCounter(UserSession.UserId);
                //var result = MessageRepository.getUsersListForInbox(UserSession.UserId);
                var result = msg.getUsersListForInbox(UserSession.UserId);
                return PartialView(result);
            }
            else
            {
                return Json(null);
            }
        }
        public ActionResult GetNewMessages(string LastMsgId, string otheruserid)
        {
            try
            {
                if (otheruserid == "0")
                {
                    ViewBag.touserid = otheruserid;
                    var result = MessageRepository.GetNewAdminMessages(Convert.ToInt64(LastMsgId), UserSession.UserId).OrderBy(s => s.addedon).ToList();
                    return Json(result);
                }
                else
                {
                    ViewBag.touserid = otheruserid;
                    var result = MessageRepository.GetNewMessages(Convert.ToInt64(LastMsgId), Convert.ToInt64(otheruserid), UserSession.UserId).OrderBy(s => s.addedon).ToList();
                    return Json(result);
                }

                
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }
        public ActionResult Unreadcounter()
        {
            HttpWebRequest req = null;
            HttpWebResponse res = null;
            string url = "http://wds2.projectstatus.co.uk/democroplife/Services/service/BeeKeepersService.svc/UnreadMsgCounter";
            req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "POST";
            req.ContentType = "text/json;";
            req.Timeout = 10000000;
            StreamWriter writer = new StreamWriter(req.GetRequestStream());
            writer.WriteLine("{\"Lang\":\"en\",\"requestValue\":"+UserSession.UserId.ToString()+"}");
            writer.Close();

            res = (HttpWebResponse)req.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(res.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }
            return Json(result);
        }
        public ActionResult Show()
        {
            if (UserSession.UserId != 0)
            {
                return PartialView();
            }
            else
            {
                return Json(null);
            }
        }

        public ActionResult GetMessages(int id)
        {

            return View("Index");
        }
        //public string GetUnreadAdminCounter()
        //{
        //    return MessageRepository.GetUnreadAdminCounter(UserSession.UserId);
        //}
        public ActionResult PVUserMessage(Int64 id)
        {
            ViewBag.ServerDateTime = DateTime.Now.ToString();
            if (UserSession.UserId != 0)
            {
                if (id != 0)
                {
                    ViewBag.touserid = id;
                    var result = MessageRepository.GetMessages(UserSession.UserId, id).OrderBy(s => s.addedon).ToList();
                    return PartialView(result);
                }
                else
                {
                    ViewBag.touserid = 0;
                    var result = MessageRepository.GetAdminMessages(UserSession.UserId).OrderBy(s => s.addedon).ToList();
                    return PartialView("PVAdminMessage", result);
                }
            }
            else
            {
                return Json(null);
            }

        }
        [HttpPost]
        public ActionResult getmessages(Int64 id)
        {
            ViewBag.ServerDateTime = DateTime.Now.ToString();
            var result = MessageRepository.GetMessages(UserSession.UserId, id).OrderBy(s => s.addedon).ToList();
            return PartialView("PVUserMessage", result);
        }
        [HttpPost]
        public ActionResult SendMessage(FormCollection col)
        {
            ViewBag.ServerDateTime = DateTime.Now.ToString();
            List<sp_getMessages_Result> lstmesseges = new List<sp_getMessages_Result>(); 
            if (Convert.ToString(col["hdntouserid"]) != null && Convert.ToString(col["hdntouserid"]) != "")
            {
                Int64 touserid = Convert.ToInt64(col["hdntouserid"]);
                ViewBag.touserid = Convert.ToString(touserid);
                using (Messages messages = new Messages())
                {
                    Int64? result = messages.SendMessage(UserSession.UserId, Convert.ToInt64(col["hdntouserid"]), col["txtMessage"].ToString(), "",1);
                    if (result != null && result > 0)
                    {

                        var installation = Data.Entities.tblinstallations.Where(s => s.userid == touserid).ToList();
                        var fromuserinfo = Data.Entities.tblusers.Where(s => s.id == UserSession.UserId).FirstOrDefault();
                        var touserinfo = Data.Entities.tblusers.Where(s => s.id == touserid).FirstOrDefault();

                        string langEmail = new Common().GetLanguage4Emails(touserid);
                        List<UserEmail> lstuseremail = new List<UserEmail>();
                        UserEmail objuseremail = new UserEmail();
                        objuseremail.fromUserName = fromuserinfo.username;
                        objuseremail.toUserName = touserinfo.username;
                        objuseremail.toEmailId = touserinfo.email;
                        objuseremail.UserLanguage = langEmail;
                        objuseremail.message = fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(langEmail, "sentyouamessage");
                        lstuseremail.Add(objuseremail);

                        int? badgecount = new Common().GetNotificationCounter(touserid);
                        if (installation != null)
                        {
                            foreach (var item in installation)
                            {
                                string langnoti = new Common().GetLanguage4Notification(item.userid);
                                if (item.devicetype == "1")
                                {
                                    iPhonePush.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(langnoti, "sentyouamessage"), badgecount, "Message", UserSession.UserId, result, "");
                                }
                                else if (item.devicetype == "2")
                                {
                                    AndroidPushNotice.SendNotice(item.devicetoken, fromuserinfo.username + " " + new SiteLanguages().GetResxNameByValue(langnoti, "sentyouamessage"), badgecount, "Message", UserSession.UserId, result, "");
                                }
                            }
                            EmailDelegate emailDelegate = new EmailDelegate(UserEmail.SendNotificationMails4Message);
                            AsyncCallback cb = new AsyncCallback(MyAsyncCallback);
                            IAsyncResult ar = emailDelegate.BeginInvoke(lstuseremail, cb, emailDelegate);

                        }

                        ViewBag.message = Resource.Messagesent;
                        ViewBag.status = "1";
                    }
                    else
                    {
                        ViewBag.message = Resource.MessagenotsentTryagain;
                        ViewBag.status = "0";
                        ModelState.AddModelError("", "");
                    }
                }
                lstmesseges = MessageRepository.GetMessages(UserSession.UserId, Convert.ToInt64(col["hdntouserid"])).OrderBy(s => s.addedon).ToList();
            }
            else
            {
                ViewBag.message = Resource.Pleaseselectuser;
                ViewBag.status = "0";
                lstmesseges = new List<sp_getMessages_Result>(); 
            }
            return PartialView("PVUserMessage", lstmesseges);
        }

        public JsonResult CountUnreadMsg()
        {
            List<MsgCounter> lstmsgCounter = (from a in MessageRepository.GetUnreadMSGCounter(UserSession.UserId).AsEnumerable() select new MsgCounter { UnreadCounter=a.unreadcount,UserId=0}).ToList();
            return Json(null);
        }
        public static void MyAsyncCallback(IAsyncResult ar)
        {

        }

    }
}
