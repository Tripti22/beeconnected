﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeeKeepersBusiness;
using BeeKeepersDataBase;
using PagedList;

namespace BeekeepersWeb.Controllers
{
    public class PropertyController : ApplicationController
    {
        public ActionResult Index(int? page)
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            var result = new Property().PropertyList_front(UserSession.UserId).ToList();
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(BeekeepersWeb.Models.PropertyModel Model)
        {
            try
            {
                Property property = new Property();
                Int64? result = property.AddProperty(UserSession.UserId, Model.latitude, Model.longitude, Model.PropertyName);
                if (result != null && result > 0)
                {
                    ModelState.Clear();
                    TempData["message"] = BeeKeepersBusiness.Resource.Propertyaddedsuccessfully;
                    TempData["status"] = "1";
                    return RedirectToAction("index");
                }
                else
                {
                    ModelState.AddModelError("", BeeKeepersBusiness.Resource.PropertynotaddedTryagain);

                }
                return View();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", BeeKeepersBusiness.Resource.PropertynotaddedTryagain);
                return View();
            }
            
        }

        public ActionResult Edit(int id)
        {
            try
            {
                if (TempData["message"] != null)
                {
                    ViewBag.message = TempData["message"];
                    ViewBag.status = TempData["status"];
                }
                PropertyResponse EditProperty = new Property().EditProperty(id);
                if (EditProperty != null)
                {
                    BeekeepersWeb.Models.PropertyModel model = new Models.PropertyModel();
                    model.PropertyName = EditProperty.propertyName;
                    model.latitude = EditProperty.latitude;
                    model.longitude = EditProperty.longitude;
                    return View(model);
                }
                else
                {
                    TempData["message"] =BeeKeepersBusiness.Resource.Propertydetailsnotget;
                    TempData["status"] = "0";
                    return RedirectToAction("index");
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = BeeKeepersBusiness.Resource.Propertydetailsnotget;
                TempData["status"] = "0";
                return RedirectToAction("index");
            }
            

        }
        public ActionResult Cancel()
        {
            return RedirectToAction("index");
        }
        [HttpPost]
        public ActionResult Edit(BeekeepersWeb.Models.PropertyModel model)
        {
            try
            {
                Int64? status = new Property().UpdateProperty(UserSession.UserId, model.latitude, model.longitude, model.PropertyName, Convert.ToInt64(model.id));
                if (status != null && status > 0)
                {
                    TempData["message"] = BeeKeepersBusiness.Resource.Propertyupdatedsuccessfully;
                    TempData["status"] = "1";
                    return RedirectToAction("index");
                }
                else
                {
                    TempData["message"] =BeeKeepersBusiness.Resource.PropertynotupdatedTryagain;
                    TempData["status"] = "0";
                }
                return RedirectToAction("Edit", model.id);
            }
            catch (Exception ex)
            {
                TempData["message"] = BeeKeepersBusiness.Resource.PropertynotupdatedTryagain;
                TempData["status"] = "0";
                return RedirectToAction("Edit", model.id);
            }
        }

        public ActionResult Delete(int id)
        {
            Int64? result = new Property().DeleteProperty(id);
            if (result != null && result == 1)
            {
                TempData["message"] =BeeKeepersBusiness.Resource.Propertydeletedsuccessfully;
                TempData["status"] = "1";

            }
            else
            {
                TempData["message"] = BeeKeepersBusiness.Resource.PropertynotdeletedTryagain;
                TempData["status"] = "0";
            }
            return RedirectToAction("index");
        }
        public ActionResult ViewAround(Int64 id)
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"];
                ViewBag.status = TempData["status"];
            }
            if (TempData["txtmessage"] != null)
            {
                ViewBag.txtmessage = TempData["txtmessage"];
            }
            PropertyResponse objproperty = new Property().EditProperty(id);
            ViewBag.userid = objproperty.userId;
            ViewBag.Propertyid = objproperty.propertyId;
            ViewBag.propertyName = objproperty.propertyName;
            ViewBag.latitude = objproperty.latitude;
            ViewBag.longitude = objproperty.longitude;
            Activities activities = new Activities();
            string startdate = string.Format("{0:dd/MM/yyyy}", DateTime.Now.ToString("dd/MM/yyyy"));
            var activityArround = activities.ActivityList4Web(UserSession.UserId, Convert.ToInt64(UserSession.UserRole), objproperty.latitude, objproperty.longitude, startdate, "", 5);
            return View(activityArround);
        }
        
    }
}
