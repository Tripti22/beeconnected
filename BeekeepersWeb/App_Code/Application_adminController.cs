﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BeekeepersWeb
{
    public class Application_adminController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var actionDescriptor = filterContext.ActionDescriptor;
            string ActionName = actionDescriptor.ActionName;
            string ControllerName = actionDescriptor.ControllerDescriptor.ControllerName;

            if (!string.IsNullOrEmpty(UserSession.UserId.ToString()) && !string.IsNullOrEmpty(UserSession.UserRole))
            {
                if (UserSession.UserRole != "admin_1")
                {
                    filterContext.Result = RedirectToAction("login", "login");
                }
            }
            else
            {
                filterContext.Result = RedirectToAction("login", "login");
            }
        }
        protected override void HandleUnknownAction(string actionName)
        {
            this.View(actionName).ExecuteResult(ControllerContext);
        }
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = null;
            HttpCookie langCookie = Request.Cookies["culture"];
            if (langCookie != null)
            {
                lang = langCookie.Value;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userLang = userLanguage != null ? userLanguage[0] : "";
                if (userLang != "")
                {
                    lang = userLang;
                }
                else
                {
                    lang = BeeKeepersBusiness.SiteLanguages.GetDefaultLanguage();
                }
            }

            new BeeKeepersBusiness.SiteLanguages().SetLanguage(lang);

            return base.BeginExecuteCore(callback, state);
        }
    }
}