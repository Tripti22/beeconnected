﻿using System;
using System.Web;
using System.Web.Mvc;
namespace BeekeepersWeb
{
    public class MyBaseController : Controller
    {
        // Here I have created this for execute each time any controller (inherit this) load 
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = null;
            HttpCookie langCookie =Request.Cookies["culture"];
            if (langCookie != null)
            {
                lang = langCookie.Value;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userLang = userLanguage != null ? userLanguage[0] : "";
                if (userLang != "")
                {
                    lang = userLang;
                }
                else
                {
                    lang = BeeKeepersBusiness.SiteLanguages.GetDefaultLanguage();
                }
            }

            new BeeKeepersBusiness.SiteLanguages().SetLanguage(lang);

            return base.BeginExecuteCore(callback, state);
        }
    }
}