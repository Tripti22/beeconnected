﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeekeepersWeb
{
    public class UserSession
    {
        public UserSession()
        {

        }

        public static long UserId
        {
            get { return Convert.ToInt32(HttpContext.Current.Session["UserId"]); }
            set { HttpContext.Current.Session["UserId"] = value; }
        }
        public static string UserRole
        {
            get { return Convert.ToString(HttpContext.Current.Session["AccountType"]); }
            set { HttpContext.Current.Session["AccountType"] = value; }
        }
        public static string UserName
        {
            get { return Convert.ToString(HttpContext.Current.Session["UserName"]); }
            set { HttpContext.Current.Session["UserName"] = value; }
        }
        public static String BasePath()
        {
            if (HttpContext.Current.Request.Url.ToString().Contains("https") == false)
                return String.Format("http://{0}{1}", HttpContext.Current.Request.ServerVariables["HTTP_HOST"], (HttpContext.Current.Request.ApplicationPath.Equals("/") ? String.Empty : HttpContext.Current.Request.ApplicationPath)) + "/";
            else
                return String.Format("https://{0}{1}", HttpContext.Current.Request.ServerVariables["HTTP_HOST"], (HttpContext.Current.Request.ApplicationPath.Equals("/") ? String.Empty : HttpContext.Current.Request.ApplicationPath)) + "/";
        }
       
    }
}